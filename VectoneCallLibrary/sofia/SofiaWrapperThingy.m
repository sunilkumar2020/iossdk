//
//  SofiaWrapperThingy.m
//  webRTC_testbed
//
//  Created by FlorinAdrianOdagiu on 03/12/2015.
//  Copyright © 2015 FlorinAdrianOdagiu. All rights reserved.
//

#import "SofiaWrapperThingy.h"
#import "SoundManager.h"
#import "DataSingleton.h"
#import "APPClient.h"
#import "Reachability.h"
#import <WebRTC/RTCRtpTransceiver.h>
#import "DNSResolverService.h"
#import <UIKit/UIKit.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <resolv.h>
#include <dns.h>
#include <sofia-sip/su_log.h>
#include <sofia-sip/sip_status.h>
#include <sofia-sip/nua.h>
#include <sofia-sip/nua_tag.h>
#include <sofia-sip/su_tag.h>
#include <sofia-sip/su_tagarg.h>
#include <sofia-sip/sresolv.h>
#include <sofia-sip/soa_tag.h>
#include <sofia-sip/stun_tag.h>
#include <sofia-sip/tport_tag.h>
#include <sofia-sip/tport.h>
#include <sofia-sip/sip_protos.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>

struct sofia_call_details callList[3];

SofiaWrapperThingy *temp;

AppClient *clientip;
extern char addrBuf[100];
extern char addrBuf1[200];
char *domainname;
bool isRegister = false;
bool is180 = false;
bool isCallFromPushKitNotification = false;

@implementation SofiaWrapperThingy

#pragma mark
#pragma mark Plain C stuff from Chennai (long section following)

#pragma mark - Account stuff
static struct webrtc_account wac1;
char *message1;
char flag2;
int answer;
BOOL iptype;
callStatusInfo *info = NULL;

nua_handle_t *auth_handle_temp=NULL;
char auth_bc[100]="";

typedef void (*fun_ptr)(void);//typedef void (*func2)(void);
fun_ptr pt;
char* file1="/tmp/file1.txt";
char* ms1=NULL;
@synthesize flagu = _flagu;

#pragma mark
#pragma mark static function declarations
static ua_sig_t * ua_sig_create_session(webrtc_account_t *account);

#pragma mark
#pragma mark Create a new SIP operation with nua_handle().
ua_sig_oper_t * ua_sig_oper_create(ua_sig_t *cli, sip_method_t method, char const *name, const char *address, tag_type_t tag, tag_value_t value, ...);
static ua_sig_oper_t * ua_sig_oper_create2(ua_sig_t *cli, sip_method_t method, char const *name, nua_handle_t *nh, sip_from_t const *from);

#pragma mark
#pragma mark  SIP callbacks activated for incoming requests
void ua_sig_i_accept_cb(ua_sig_t *ua_sig);
void ua_sig_i_cancel_cb(ua_sig_t *ua_sig);

static void ua_sig_i_subscription(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_i_notify(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_i_invite(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

void ua_sig_i_message(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_i_state(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_i_active(nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_i_terminated(int status, char const *phrase, nua_t *nua, ua_sig_t *cli, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

#pragma mark
#pragma mark SIP callbacks activated for outgoing requests
static void ua_sig_r_invite(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

void ua_sig_r_message(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_get_params(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_notifier(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_shutdown(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_register(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_unregister(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_subscribe(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

static void ua_sig_r_authorize(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]);

void oper_set_auth(ua_sig_t *ssip, ua_sig_oper_t *op, sip_t const *sip, tagi_t *tags);

void addNewCall(nua_handle_t *current_handle, const char *call_id);

struct sofia_call_details *find_call(const char *call_id);

#pragma  mark
ua_sig_t *ua_sig = NULL;
ua_sig_oper_t *op = NULL;

nua_handle_t *temp_handle = NULL;
sip_t const *sip_reinvite;
nua_t *n1=NULL;
char *to_str = NULL;
dispatch_queue_t incomingSerialQ;
dispatch_queue_t outgoingSerialQ;

-(instancetype)initWithDelegate:(id)delegate{
    if(self = [super init]){
        _delegate = delegate;
    }
    return self;
}

struct in_addr *ipv4=NULL;
void ipv6_to_ipv4(const struct in6_addr *ipv6){
    #ifdef LINUX
        ipv4->s_addr = ntohl(ipv6->s6_addr32[3]);
    #else
        ipv4->s_addr = ntohl(ipv6->__u6_addr.__u6_addr32[3]);
    #endif
}

#pragma mark I group
char from[100];
char to[100];
NSString *ch;
char to_str1[100];

char * trimextra(char * str){
    while(isspace(*str) ||  *str == '\"') str++;
    //printf("**** strlen of str is %d\n",strlen(str));
    str[ strlen(str) - 2 ] = '\0';
    return str;
}

void ua_sig_callback(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    NSLog(@"\nFunction 'ua_sig_callback' Called: %d Status = %d", event, status);
    
    if(info == NULL){
        info = [callStatusInfo new];
    }
    info.status = status;
    
    if(sip != NULL){
        @try{
            if(sip->sip_call_id->i_id != NULL){
                info.call_id = [NSString stringWithCString:sip->sip_call_id->i_id encoding:NSUTF8StringEncoding];
                NSLog(@"\nFunction 'ua_sig_callback' info.call_id: %@", info.call_id);
            }
            if(sip->sip_from->a_display != NULL){
                info.from = [NSString stringWithCString:sip->sip_from->a_display encoding:NSUTF8StringEncoding];
                NSLog(@"\nFunction 'ua_sig_callback' info.from: %@", info.from);
            }
            if(sip->sip_to->a_display != NULL){
                info.to = [NSString stringWithCString:sip->sip_to->a_display encoding:NSUTF8StringEncoding];
                NSLog(@"\nFunction 'ua_sig_callback' info.to: %@", info.to);
            }
        }@catch(NSException *exception){
            NSLog(@"%@", exception.reason);
        }
    }
    
    switch(event){
        case nua_i_error:
            NSLog(@"Sofia Error : %d",status);
            break;
            
        case nua_i_register:
            NSLog(@"My Contact : %s",sip->sip_contact->m_comment);
            break;
            
        case nua_r_register:
            ua_sig_r_register(event, status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_unregister:
            if(![DataSingleton sharedInstance].ispersonInCall)
                ua_sig_r_unregister(event, status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_shutdown:
            ua_sig_r_shutdown(event, status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_i_outbound:
            NSLog(@"hitting outbound event");
            break;
            
        case nua_r_message:
            ua_sig_r_message(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_i_message:
            ua_sig_i_message(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_get_params:
            ua_sig_r_get_params(event, status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_notifier:
            ua_sig_r_notifier(event, status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_subscribe:
            ua_sig_r_subscribe(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_authorize:
            ua_sig_r_authorize(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_i_subscription:
            ua_sig_i_subscription(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_i_notify:
            ua_sig_i_notify(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_i_invite: /**< Incoming call INVITE */
            NSLog(@"recieving call:%d\n",status);
            ua_sig_i_invite(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_i_terminated:
            [[NSNotificationCenter defaultCenter]postNotificationName:@"CloseCall" object:nil];
            [SofiaWrapperThingy sofiaReceivedCallEnd:info];// sanjay  did for video call crashing dated 22-9-16
            ua_sig_i_terminated(status, phrase, nua, ua_sig, nh, op, sip, tags);
            break;
            
        case nua_r_invite: /**< Answer to outgoing INVITE */
            ua_sig_r_invite(status, phrase, nua, ua_sig, nh, op, sip, tags);
            NSLog(@"receiving invite for outgoing:%d\n",status);
            break;
            
        case nua_i_active:
            ua_sig_i_active(nua, ua_sig, nh, op, sip, tags);
            [temp setEventState:info];
            NSLog(@"Sunil nua_i_active %d",info.status);
            break;
            
        case nua_i_state:
            ua_sig_i_state(status, phrase, nua, ua_sig, nh, op, sip, tags);
            NSLog(@" nua_i_state getting cancel state 2 :%d",status);
            [temp setEventState:info];
            NSLog(@"Sunil nua_i_state %d",info.status);
            break;
            
        case nua_i_bye:
            nua_respond(nh,SIP_200_OK,TAG_END());
            ua_sig_i_bye(nua,phrase,nh,op,sip,tags);
            [SofiaWrapperThingy sofiaReceivedCallEnd:info];
            NSLog(@" nua_i_bye getting cancel state 1 :%d",status);
            break;
            
        case nua_r_bye:
            ua_sig_r_bye(status,phrase,nua,nh,op,sip,tags);
            break;
            
        case nua_i_cancel:
            [[NSNotificationCenter defaultCenter]postNotificationName:@"CloseCall" object:nil];  // close incoming call UI
            [SofiaWrapperThingy sofiaReceivedCallEnd:info];// sanjay  did for video call crashing dated 22-9-16
            ua_sig_i_cancel(status,phrase,nua,nh,op,sip,tags);
            NSLog(@" nua_i_cancelgetting cancel state 2 :%d",status);
            break;
            
        case nua_r_cancel:
            ua_sig_r_cancel(status,phrase,nua,nh,op,sip,tags);
            NSLog(@"reaching cancel state:%d",status);
            break;
            
        case nua_i_network_changed:
            printf("network address changed");
            break;
            
        case nua_i_ack:
            break;
            
        default:
            break;
    }
    return;
    
//    if(status == 200){
//        /** Events */
//            typedef enum nua_event_e {
//                /* Event used by stack internally */
//                nua_i_none = -1,                                                            -1
//
//                /* Indications */
//                nua_i_error,            /**< Error indication */                               0
//
//                nua_i_invite,            /**< Incoming call INVITE */                           1
//                nua_i_cancel,            /**< Incoming INVITE has been cancelled */
//                nua_i_ack,            /**< Final response to INVITE has been ACKed */        2
//                nua_i_fork,            /**< Outgoing call has been forked */                  3
//                nua_i_active,            /**< A call has been activated */                      4
//                nua_i_terminated,        /**< A call has been terminated */                     5
//                nua_i_state,                /**< Call state has changed */                     6
//
//                nua_i_outbound,        /**< Status from outbound processing */                7
//
//                nua_i_bye,            /**< Incoming BYE call hangup */                       8
//                nua_i_options,        /**< Incoming OPTIONS */                               9
//                nua_i_refer,            /**< Incoming REFER call transfer */                  10
//                nua_i_publish,        /**< Incoming PUBLISH */                              11
//                nua_i_prack,            /**< Incoming PRACK */                                12
//                nua_i_info,            /**< Incoming session INFO */                         13
//                nua_i_update,            /**< Incoming session UPDATE */                       14
//                nua_i_message,        /**< Incoming MESSAGE */                              15
//                nua_i_chat,            /**< Incoming chat MESSAGE  */                        16
//                nua_i_subscribe,        /**< Incoming SUBSCRIBE  */                           17
//                nua_i_subscription,        /**< Incoming subscription to be authorized */    18
//                nua_i_notify,            /**< Incoming event NOTIFY */                         19
//                nua_i_method,            /**< Incoming, unknown method */                      20
//
//                nua_i_media_error,        /**< Offer-answer error indication */             21
//
//                /* Responses */
//                nua_r_set_params,        /**< Answer to nua_set_params() or                    22
//                                         * nua_get_hparams(). */
//                nua_r_get_params,        /**< Answer to nua_get_params() or                    23
//                                         * nua_get_hparams(). */
//                nua_r_shutdown,        /**< Answer to nua_shutdown() */                      24
//                nua_r_notifier,        /**< Answer to nua_notifier() */                      25
//                nua_r_terminate,        /**< Answer to nua_terminate() */                     26
//                nua_r_authorize,        /**< Answer to nua_authorize()  */                    27
//
//                /* SIP responses */
//                nua_r_register,        /**< Answer to outgoing REGISTER */                   28
//                nua_r_unregister,        /**< Answer to outgoing un-REGISTER */                29
//                nua_r_invite,                /**< Answer to outgoing INVITE */                 30
//                nua_r_cancel,            /**< Answer to outgoing CANCEL */                     31
//                nua_r_bye,            /**< Answer to outgoing BYE */                        32
//                nua_r_options,        /**< Answer to outgoing OPTIONS */                    33
//                nua_r_refer,            /**< Answer to outgoing REFER */                      34
//                nua_r_publish,        /**< Answer to outgoing PUBLISH */                    35
//                nua_r_unpublish,        /**< Answer to outgoing un-PUBLISH */                 36
//                nua_r_info,                /**< Answer to outgoing INFO */                   37
//                nua_r_prack,            /**< Answer to outgoing PRACK */                      38ƒ
//                nua_r_update,                /**< Answer to outgoing UPDATE */                 39
//                nua_r_message,        /**< Answer to outgoing MESSAGE */                    40
//                nua_r_chat,            /**< Answer to outgoing chat message */               41
//                nua_r_subscribe,        /**< Answer to outgoing SUBSCRIBE */                  42
//                nua_r_unsubscribe,        /**< Answer to outgoing un-SUBSCRIBE */           43
//                nua_r_notify,            /**< Answer to outgoing NOTIFY */                     44
//                nua_r_method,            /**< Answer to unknown outgoing method */             45
//
//                nua_r_authenticate,        /**< Answer to nua_authenticate() */              46
//
//                /* Internal events: nua hides them from application */
//                nua_r_redirect,                                                             47
//                nua_r_destroy,                                                              48
//                nua_r_respond,                                                              49
//                nua_r_nit_respond,                                                          50
//                nua_r_ack,            /*#< Answer to ACK */                                 51
//
//                /* NOTE: Post 1.12 release events come here (below) to keep ABI
//                 compatibility! */
//                nua_i_network_changed,        /**< Local IP(v6) address has changed.        52
//                                               @NEW_1_12_2 */
//                nua_i_register        /**< Incoming REGISTER. @NEW_1_12_4. */               53
//            } nua_event_t;
//    }
}

void ua_sig_r_cancel(int status, char const *phrase, nua_t *nua, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    assert(op);
    assert(op->op_handle==nh);
    printf("cancel response status:%d",status);
    
    if(status < 200){
        return;
    }
}

void ua_sig_i_cancel(int status, char const *phrase, nua_t *nua, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    if(op){
        assert(op);
        assert(op->op_handle==nh);
        printf("Incoming cancel status:%d",status);
        if(status < 200){
            return;
        }
    }else{
        return;
    }
}

#pragma  mark
int ua_sig_login(char *iuid_str){
    char const *user = NULL;
#if 0
    const char *registrar = NULL;
    const char *proxy = NULL;
    const char *domain = NULL;
    int reg_port;
    int proxy_port;
    int have_presence_server;
    
    char *upd = NULL;
    su_home_t *home = NULL;
#endif
    webrtc_account_t *account = &wac1;
    NSLog(@"*** Account name is %s ****\n",account->user);
    NSLog(@"*** Account password is %s ****\n",account->password);
    
    @try{
        while(1){
            user = account->user;
            ua_sig = ua_sig_create_session(account);
            NSLog(@"SIP stack is created First");
            if(ua_sig != NULL){
                NSLog(@"*** REGISTER ... *****\n");
                ua_sig->s_connection_state = UA_REGISTERING;
                
                /* Create new operation, REGISTER. */
                op = ua_sig_oper_create(ua_sig, SIP_METHOD_REGISTER, user, TAG_END());
                
                /* Send REGISTER to registrar with our account and domain as "From:" field. Response is hooked by callback. */
                const char  *deviceType = [[NSString stringWithFormat:@"X-device:IOS,%@",[DataSingleton sharedInstance].appCode] cStringUsingEncoding:NSUTF8StringEncoding];
                
                NSString *getCallID = [NSString stringWithFormat:@"%@", [DataSingleton sharedInstance].callID];
                const char *callID = [getCallID cStringUsingEncoding:NSUTF8StringEncoding];
                printf("Get Call ID -- %s",callID);

                assert(ua_sig_login);
                
                if(op->op_handle != NULL){
                    if((getCallID == NULL) || ([getCallID isEqualToString:@""])){
                        nua_register(op->op_handle,
                                     NUTAG_M_DISPLAY(account->cli),
                                     SIPTAG_EXPIRES_STR("3600"),
                                     SIPTAG_UNKNOWN_STR(iuid_str),
                                     SIPTAG_UNKNOWN_STR(deviceType),
                                     SIPTAG_USER_AGENT_STR(setUserAgent()),
                                     NUTAG_OUTBOUND("no-options-keepalive"),
                                     NUTAG_OUTBOUND("no-validate"),
                                     NUTAG_KEEPALIVE(0),
                                     //SIPTAG_CALL_ID_STR(callID),
                                     //NUTAG_M_PARAMS("transport=TCP"),
                                     //NUTAG_REGISTRAR(ua_sig->s_registrar),
                                     TAG_END());
                    }else{
                        nua_register(op->op_handle,
                                     NUTAG_M_DISPLAY(account->cli),
                                     SIPTAG_EXPIRES_STR("3600"),
                                     SIPTAG_UNKNOWN_STR(iuid_str),
                                     SIPTAG_UNKNOWN_STR(deviceType),
                                     SIPTAG_USER_AGENT_STR(setUserAgent()),
                                     NUTAG_OUTBOUND("no-options-keepalive"),
                                     NUTAG_OUTBOUND("no-validate"),
                                     NUTAG_KEEPALIVE(0),
                                     SIPTAG_CALL_ID_STR(callID),
                                     //NUTAG_M_PARAMS("transport=TCP"),
                                     //NUTAG_REGISTRAR(ua_sig->s_registrar),
                                     TAG_END());
                    }
                    NSLog(@">>SUNIL >>>> START");
                    su_root_run(ua_sig->s_root);//Thread Blocking function
                }
            }
            releaseBlock();//Destroy allocated resources
            NSLog(@">>SUNIL >>>> STOP");
            break;
        }
    }@catch(NSException *exception){
        NSLog(@"%@", exception.reason);
    }
    return 0;
}

+(void)deInitResources:(BOOL)isCallFromPushKit{
    isCallFromPushKitNotification = isCallFromPushKit;
    @try{
        if((ua_sig != NULL) && (ua_sig->s_root != NULL)){
            su_root_break(ua_sig->s_root);
        }
    }@catch(NSException *exception){
        NSLog(@"<<<<<<<<<< Deinit Exception >>>>>>>");
    }
}

void releaseBlock(){
    NSLog(@">>SUNIL >>>> releaseBlock ");
    /*if((ua_sig != NULL) && (ua_sig->s_root != NULL)){
        su_root_break(ua_sig->s_root);
        su_root_destroy(ua_sig->s_root), ua_sig->s_root = NULL;
        su_home_deinit(ua_sig->s_home);
        su_deinit();
    }*/
    
    /*if(ua_sig != NULL){
        if(ua_sig->s_nua != NULL){
            nua_shutdown(ua_sig->s_nua);
        }
        if(ua_sig->s_root != NULL){
            su_root_destroy(ua_sig->s_root);
        }
    }
    su_deinit();*/
    
    //Callback to application releasecompleted()
    NSDictionary *callFrom = @{@"isCallFromPushKit":[NSNumber numberWithBool:isCallFromPushKitNotification]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OperationQueueStart" object:nil userInfo:callFrom];
}

void autoAccept(const char* iuid){
    NSString*_iuid=[NSString stringWithCString:iuid encoding:NSUTF8StringEncoding];
    NSString *answerSDP;

    if([[DataSingleton sharedInstance].client.iu_id isEqualToString:_iuid]){
        answerSDP = [NSString stringWithString:[DataSingleton sharedInstance].client.outgoingPayload];
        answerSDP = [[DataSingleton sharedInstance].client optimizePayload:answerSDP];
       
    }else if([[DataSingleton sharedInstance].client_L2.iu_id isEqualToString:_iuid]){
        answerSDP = [NSString stringWithString:[DataSingleton sharedInstance].client_L2.outgoingPayload];
        answerSDP = [[DataSingleton sharedInstance].client_L2 optimizePayload:answerSDP];
        
    }else{
        answerSDP = @"";
    }
    
    if([answerSDP containsString:@"a=candidate"]){
        answerSDP = [answerSDP stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
        answerSDP = [answerSDP stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
        answerSDP = [answerSDP stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:active"];
        
        char const *l_sdp = [answerSDP UTF8String];
        if(op->op_handle){
            nua_respond(op->op_handle,SIP_200_OK,TAG_IF(l_sdp,SOATAG_USER_SDP_STR(l_sdp)),SIPTAG_USER_AGENT_STR(setUserAgent()),TAG_END());
            op->op_callstate = opc_complete;
            [DataSingleton sharedInstance].isIncomingCallAccept = NO;
        }
    }
}

bool isValidIpAddress(char *ipAddress){
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    printf("ip address:%s",ipAddress);
    return result != 0;
}

+(uint32_t)convertIpAddress:(NSString *)ipAddress{
    struct sockaddr_in sin;
    inet_aton([ipAddress UTF8String], &sin.sin_addr);
    return ntohl(sin.sin_addr.s_addr);
}

static ua_sig_t *ua_sig_create_session(webrtc_account_t *account){
    /* Initialize Sofia-SIP library and create event loop */
    ua_sig_t *sample = (ua_sig_t *)malloc(sizeof(ua_sig_t));
    if(sample == NULL){
        NSLog(@"Unable to create ua signalling instance \n");
        exit(1);
    }
    memset(sample,0,sizeof(ua_sig_t));
    
    strcpy(sample->s_domain,account->domain);
    strcpy(sample->s_proxy,account->proxy);
    strcpy(sample->s_registrar,account->domain);
    
    su_init ();
    sample->s_root = su_root_create(NULL);
    
    if(sample->s_root == NULL){
        printf("s_root is null");
    }
    
    /* Create a user agent instance. Caller and callee should bind to different
     * address to avoid conflicts. The stack will call the 'event_callback()'
     * callback when events such as succesful registration to network,
     * an incoming call, etc, occur.
     */
    
    char *cap="v=0\r\n"
    "m=audio 0 RTP/AVP 0\r\n"
    "a=rtpmap:0 PCMU/8000\r\n";
    
    char *from1 = (char *)malloc(100*sizeof(char));
    if([DataSingleton sharedInstance].isMiddleEast == YES){
        sprintf(from1,"sips:%s@%s",account->cli,account->domain);
    }else{
        sprintf(from1,"sip:%s@%s",account->cli,account->domain);
    }
    
    printf("registering from:%s\n",from1);
    printf("ua sig create session");
    NSString *v6address1 = [AppClient getPrimaryIPAddress];
    
    if([v6address1 length]>0){
    char* addre1=(char*)[v6address1 UTF8String];
    [DataSingleton sharedInstance].old_IPAddress=v6address1;

    char *sip_url= (char *)malloc(100*sizeof(char));
    //NSLog(@"printing ipv6 address:%@\n",v6address1);
#if 1
    //This code is written by Servanan, presently we are getting wrong ip address so that comenting for now by murli dated 8-9-16
    BOOL flagem = [SofiaWrapperThingy ipAddress:v6address1 isBetweenIpAddress:@"169.254.0.0" andIpAddress:@"169.254.255.255"];
    NSLog(@"printing bool flagem:%d\n",flagem);
    [NSThread detachNewThreadSelector:@selector(waitForRegistration) toTarget:temp withObject:nil];
    if(isValidIpAddress(addre1) && (!flagem))
#endif
    {
        sprintf(sip_url,"sip:0.0.0.0:*;transport=TCP");
        //void (*ua_sig_callback)= &ua_sig_callback;
        //void (*nua_callback_f)() = &ua_sig_callback;
        sample->s_nua = nua_create(sample->s_root,
                                   ua_sig_callback,
                                   NULL,
                                   NUTAG_URL(sip_url),
                                   SIPTAG_FROM_STR(from1),
                                   //NUTAG_PROXY(wac1.proxy),
                                   //NTATAG_SIP_T1(360),
                                   TAG_IF(cap,SOATAG_USER_SDP_STR(cap)),
                                   SOATAG_AF(SOA_AF_IP4_ONLY),
                                   NUTAG_M_USERNAME(account->user),
                                   NUTAG_MEDIA_ENABLE(1),
                                   NUTAG_DETECT_NETWORK_UPDATES (NUA_NW_DETECT_TRY_FULL),
                                   TAG_END());
        printf("ipv4 address");
    }
    else
    {
#if 1
            //murli if ipv6 will go with ipv6 than we need to take this code for real time this will work  dated 2-10-16
            //void (*nua_callback_f)() = &ua_sig_callback;
            sample->s_nua = nua_create(sample->s_root,
                                       ua_sig_callback,
                                       NULL,
                                       NUTAG_URL("sip:[::]:*;transport=TCP"),
                                       SIPTAG_FROM_STR(from1),
                                       NUTAG_PROXY(wac1.proxy),
                                       NTATAG_SIP_T1X64(3600),
                                       //NUTAG_MEDIA_ADDRESS(local),
                                       TAG_IF(cap,SOATAG_USER_SDP_STR(cap)),
                                       SOATAG_AF(SOA_AF_IP6_ONLY),
                                       NUTAG_MEDIA_ENABLE(1),
                                       NUTAG_M_USERNAME(account->user),
                                       NUTAG_DETECT_NETWORK_UPDATES (NUA_NW_DETECT_TRY_FULL),
                                       TAG_END());
      
#endif
        printf("ipv6 address");
        NSLog(@"it is an valid ipv6 address:%@\n",v6address1);
    }
        
    if(sample->s_nua ){
        nua_set_params(sample->s_nua,
                       //STUNTAG_DOMAIN("turn:stun02.mundio.com:3478"),
                       NUTAG_ENABLEMESSAGE(1),
                       NUTAG_ENABLEINVITE(1),
                       NUTAG_ALLOW("REFER"),
                       NUTAG_ALLOW("REGISTER"),
                       NUTAG_OUTBOUND("notify use-rport options-keepalive"),
                       NUTAG_AUTOALERT(1),
                       NUTAG_AUTOACK(1),
                       TAG_NULL());
        NSLog(@"enabling auto alert to no");
    }else{
        printf("s_nua is null");
    }
        NSLog(@" ****** ua_sig_session is created *******\n");
    }
    return sample;
}

void ua_sig_i_invite(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]) {
    NSLog(@"SDK receiving incoming INVITE ua_sig_i_invite: %s \n ", __FUNCTION__);
    if(op){
        op->op_callstate |= opc_recv;
    }else if((op = ua_sig_oper_create2(ua_sig, SIP_METHOD_INVITE, nh, sip->sip_from))){
        op->op_callstate = opc_recv;
    }
    if(op){
        NUTAG_OFFER_RECV_REF(status);
        NSLog(@"status is not 183:priting status:%c",status);
        if(op->op_callstate == opc_recv){
            op->op_callstate = opc_ringing;
            printf("printing callstate inside 180: %d \n %c",op->op_callstate,flag2);
        }
    }
    addNewCall(nh, sip->sip_call_id->i_id);
    
    if([DataSingleton sharedInstance].isIncomingCallDecline == YES){
        [SofiaWrapperThingy sofiaRejectCall];
        [DataSingleton sharedInstance].isIncomingCallDecline = NO;
    }else{
        if(!incomingSerialQ){
            incomingSerialQ = dispatch_queue_create("SDP_SERIAL_QUEUE", DISPATCH_QUEUE_SERIAL);
        }
        dispatch_async(incomingSerialQ,^{
            NSString *isAudio = [NSString stringWithCString:sip->sip_payload->pl_data encoding:NSUTF8StringEncoding];
            if([isAudio  localizedCaseInsensitiveContainsString:@"BUNDLE audio video"]|| [isAudio  localizedCaseInsensitiveContainsString:@"BUNDLE 0 1"]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *sipCall_ID = [NSString stringWithCString:sip->sip_call_id->i_id encoding:NSUTF8StringEncoding];
                    if([[DataSingleton sharedInstance].sipCall_ID isEqualToString:sipCall_ID]){
                        /*reInviteHandle=nh;
                        NSLog(@"REINVITE murli sip account id same %@ /n",sipCall_ID);
                        if([DataSingleton sharedInstance].client.isInitiator){
                            [DataSingleton sharedInstance].outgoingPayload = [[DataSingleton sharedInstance].outgoingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:active"];
                        }else{
                            [DataSingleton sharedInstance].outgoingPayload = [[DataSingleton sharedInstance].outgoingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:passive"];
                        }
                        char const *l_sdp = [[DataSingleton sharedInstance].outgoingPayload cStringUsingEncoding:NSUTF8StringEncoding];
                        [DataSingleton sharedInstance].client.isAudioOnly = NO;
                        [DataSingleton sharedInstance].client.isInitiator = NO;
                        [[DataSingleton sharedInstance].client createReInviteAnswer:YES];
                        nua_respond(nh, SIP_200_OK,TAG_IF(l_sdp,SOATAG_USER_SDP_STR(l_sdp)),TAG_END());

                        [SofiaWrapperThingy callStatus:@"Connected"];
                        [DataSingleton sharedInstance].isAutoReInvite=NO;
                        
                        //implemente for ReInvite
                        [SofiaWrapperThingy acceptcall];*/
                        
                        op->op_callstate = opc_complete;
                    }else{
                        //nua_respond(nh, SIP_180_RINGING,TAG_END());
                        if(sip->sip_payload->pl_data != NULL){
                            const char* iuid = sofia_get_iuid(sip);
                            NSString *payload = [NSString stringWithCString:sip->sip_payload->pl_data encoding:NSUTF8StringEncoding];
                            //payload = [payload stringByReplacingOccurrencesOfString:@"BUNDLE 0 1" withString:@"BUNDLE audio video"];
                            [[DataSingleton sharedInstance] addClient:sipCall_ID iuid:[NSString stringWithCString:iuid encoding:NSUTF8StringEncoding] incomPayload:payload isAudio:NO];
                            NSDictionary* userInfo = @{@"Number":[NSString stringWithCString:sip->sip_from->a_display encoding:NSUTF8StringEncoding],@"Type":@"Video", @"call_id":sipCall_ID,@"iuid":[NSString stringWithCString:iuid encoding:NSUTF8StringEncoding]};
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"pushIn" object:nil userInfo:userInfo];
                        }
                    }
              });
          }else if([isAudio  localizedCaseInsensitiveContainsString:@"BUNDLE audio"]|| [isAudio  localizedCaseInsensitiveContainsString:@"BUNDLE 0"]){
              dispatch_async(dispatch_get_main_queue(), ^{
                  NSString *sipCall_ID = [NSString stringWithCString:sip->sip_call_id->i_id encoding:NSUTF8StringEncoding];
                  if([[DataSingleton sharedInstance].client.call_id isEqualToString:sipCall_ID]){
                      [SofiaWrapperThingy callStatus:@"Connected"];
                      NSLog(@"REINVITE murli sip account id same %@ /n",sipCall_ID);
                      
                      //implement for ReInvite
                      if([DataSingleton sharedInstance].client.isInitiator){
                          [DataSingleton sharedInstance].client.outgoingPayload = [[DataSingleton sharedInstance].client.outgoingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:active"];
                      }else{
                          [DataSingleton sharedInstance].client.outgoingPayload = [[DataSingleton sharedInstance].client.outgoingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:passive"];
                      }
                      
                      char const *l_sdp = [[DataSingleton sharedInstance].client.outgoingPayload cStringUsingEncoding:NSUTF8StringEncoding];
                      [DataSingleton sharedInstance].client.isAudioOnly = YES;
                      [[DataSingleton sharedInstance].client createReInviteAnswer:NO];
                      //[SofiaWrapperThingy acceptcall];
                      [SofiaWrapperThingy callStatus:@"Connected"];
                      nua_respond(nh,SIP_200_OK,TAG_IF(l_sdp,SOATAG_USER_SDP_STR(l_sdp)),TAG_END());
                      op->op_callstate = opc_complete;
                      
                  }else if([[DataSingleton sharedInstance].client_L2.call_id isEqualToString:sipCall_ID]){
                      [SofiaWrapperThingy callStatus:@"Connected"];
                      NSLog(@"REINVITE murli sip account id same %@ /n",sipCall_ID);
                      //implement for ReInvite
                      if([DataSingleton sharedInstance].client_L2.isInitiator){
                          [DataSingleton sharedInstance].client_L2.outgoingPayload = [[DataSingleton sharedInstance].client_L2.outgoingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:active"];
                      }else{
                          [DataSingleton sharedInstance].client_L2.outgoingPayload = [[DataSingleton sharedInstance].client_L2.outgoingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:passive"];
                      }
                      char const *l_sdp = [[DataSingleton sharedInstance].client_L2.outgoingPayload cStringUsingEncoding:NSUTF8StringEncoding];
                      [DataSingleton sharedInstance].client_L2.isAudioOnly = YES;
                      [[DataSingleton sharedInstance].client_L2 createReInviteAnswer:NO];
                      //[SofiaWrapperThingy acceptcall];
                      [SofiaWrapperThingy callStatus:@"Connected"];
                      nua_respond(nh,SIP_200_OK,TAG_IF(l_sdp,SOATAG_USER_SDP_STR(l_sdp)),TAG_END());
                      
                      op->op_callstate = opc_complete;
                  }else{
                      //nua_respond(nh, SIP_180_RINGING,TAG_END());
                      if(sip->sip_payload->pl_data != NULL){
                          const char* iuid = sofia_get_iuid(sip);
                          [[DataSingleton sharedInstance] addClient:sipCall_ID iuid:[NSString stringWithCString:iuid encoding:NSUTF8StringEncoding] incomPayload:[NSString stringWithCString:sip->sip_payload->pl_data encoding:NSUTF8StringEncoding] isAudio:YES];
                          NSDictionary *userInfo = @{@"Number":[NSString stringWithCString:sip->sip_from->a_display encoding:NSUTF8StringEncoding],@"Type":@"Audio", @"call_id":sipCall_ID,@"iuid":[NSString stringWithCString:iuid encoding:NSUTF8StringEncoding]};
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"pushIn" object:nil userInfo:userInfo];
                      }
                  }
              });
          }
          sip_payload_t const *payload = sip->sip_payload;
          fwrite(payload->pl_data, payload->pl_len, 1, stdout);
      });
    }
    return;
}

const char *sofia_get_iuid(sip_t const *sip){
    sip_unknown_t *unknown_header = sip->sip_unknown;
    while(unknown_header){
        if(0 == strcmp(unknown_header->un_name, "iuid")){
            return unknown_header->un_value;
        }
        unknown_header = unknown_header->un_next;
    }
    return "";
}

void sofia_hold(int hold){
    /*tagi_t tags;
    int offer_sent=0;
    int ss_state=0;
    
    tl_gets(&tags,
    NUTAG_CALLSTATE_REF(ss_state),
    NUTAG_OFFER_RECV_REF(offer_recv),
    NUTAG_ANSWER_RECV_REF(answer_recv),
    NUTAG_OFFER_SENT_REF(offer_sent),
    NUTAG_ANSWER_SENT_REF(answer_sent),
    NUTAG_CALLSTATE_REF(ss_state),
    NUTAG_OFFER_SENT_REF(offer_sent),
    SOATAG_LOCAL_SDP_STR_REF(l_sdp),
    TAG_END());
    
    [DataSingleton sharedInstance].client.outgoingPayload = [[DataSingleton sharedInstance].client.outgoingPayload stringByReplacingOccurrencesOfString:@"setup:active" withString:@"setup:actpass"];
    [DataSingleton sharedInstance].client.outgoingPayload = [[DataSingleton sharedInstance].client.outgoingPayload stringByReplacingOccurrencesOfString:@"setup:passive" withString:@"setup:actpass"];
    char const *l_sdp1 = [[DataSingleton sharedInstance].client.outgoingPayload  UTF8String];
    if(!l_sdp1){
        printf("sdp NULL--inside sofia_hold");
    }else{
        printf("sdp NOT NULL -- inside sofia_unhold");
    }*/
    
    if(op){
        //nua_invite(op->op_handle,SOATAG_HOLD(hold), TAG_END());
        nua_invite(op->op_handle,NUTAG_HOLD(hold),TAG_END());
        op->op_callstate = opc_sent2;
    }else{
        printf("no call put on hold\n");
    }
}

void ua_sig_i_message(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    NSLog(@"*** Inside ua_sig_i_message ****\n");
    message1 = (char*)malloc((200*sizeof(char)));
    if(sip->sip_subject){
        printf("Subject: %s\n", sip->sip_subject->g_value);
    }
    if(sip->sip_payload){
        message1 = (char*)sip->sip_payload->pl_data;
    }
    printf("message inside function ua_sig_i_message:%s", message1);
    
    char sender[100];
    strcpy(sender,sip->sip_from->a_display);
    
    //NSString *sender1 = [[NSString alloc] initWithUTF8String:sender];
    //if(sip->sip_payload)
    //strncpy(message1,(char*)sip->sip_payload,sip->sip_payload->pl_len);
    
    if(strncmp(sip->sip_content_type->c_type, "text/plain", 16) == 0){
        NSLog(@"Message received is text/plain type\n");
        
    }else if(strncmp(sip->sip_content_type->c_type,"application/im-iscomposing+xml", 30) == 0){
        NSLog(@"Message received is xml type\n");
        
    }else if(strncmp(sip->sip_content_type->c_type, "text/html", 16) == 0){
        NSLog(@"Message received is test/html type\n");
        if(sip!=NULL && sip->sip_payload != NULL){
            NSLog(@"and the message is... %s\n",sip->sip_payload->pl_data);
        }
    }else{
        NSLog(@"Message received is some unknown type\n");
    }
}

char *printmessage(){
    return message1;
}

void ua_sig_i_subscription(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    #if 0
        nea_sub_t *subscriber = NULL;
        tl_gets(tags, NEATAG_SUB_REF(subscriber), TAG_END());
        //invoke a dialog where the user can authorize subscriber for the presence
        nua_authorize(nh, NEATAG_SUB(subscriber), NUTAG_SUBSTATE(nua_substate_active), TAG_END());
        return;
    #endif
}

void ua_sig_i_notify(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    if(sip){
        sip_from_t const *from = sip->sip_from;
        sip_event_t const *event = sip->sip_event;
        sip_content_type_t const *content_type = sip->sip_content_type;
        sip_payload_t const *payload = sip->sip_payload;
        if(op){
            NSLog(@"%@: NOTIFY from %s",from, op->op_ident);
        }
        if(event){
            printf("\tEvent: %s", event->o_type);
        }
        if(content_type){
            printf("\tContent type: %s", content_type->c_type);
        }
        
        fputs("\n",stdout);
        if(payload){
            fwrite(payload->pl_data, payload->pl_len, 1, stdout);
            if(payload->pl_len < 1 || (payload->pl_data[payload->pl_len - 1] != '\n' || payload->pl_data[payload->pl_len - 1] != '\r')){
                fputs("\n\n", stdout);
            }else{
                fputs("\n", stdout);
            }
        }
    }
    
    #if 0
        nua_respond(nh, SIP_404_NOT_FOUND, TAG_END());
    #endif
    
    return;
}

bool isH264Codec(NSString * str_sdp){
    BOOL isTrue = FALSE;
    NSString *codecnumber;
    NSArray *arr = [str_sdp componentsSeparatedByString:@"\n"];
    int count = -1;
    for(NSString *str in arr){
        if([str hasPrefix:@"m=video"]){
            NSArray *iscodec = [str componentsSeparatedByString:@" "];
            for(NSString *str in iscodec){
                count++;
                if([str isEqualToString:@"TCP/TLS/RTP/SAVPF"]){
                    codecnumber = [iscodec objectAtIndex:(count+1)];
                    break;
                }
            }
        }else if([str hasPrefix:@"a=rtpmap:"]){
            NSArray *temp = [str componentsSeparatedByString:@" "];
            BOOL isloopbreak = false;
            for(NSString *strstr in temp){
                if([strstr isEqualToString:@"H264/90000"]){
                    isTrue = YES;
                    NSLog(@"Its H264 codec \n");
                    isloopbreak = YES;
                    break;
                    
                }else if([strstr isEqualToString:@"VP8/90000\r"]){
                    isTrue = NO;
                    isloopbreak = YES;
                    NSLog(@"Its VP8  codec\n");
                    break;
                }
            }
            if(isloopbreak){
                break;
            }
        }
    }
    return isTrue;
}

const char *cli_active(int mode){
    switch(mode){
        case nua_active_inactive:
            return "inactive";
        case nua_active_sendonly:
            return "sendonly";
        case nua_active_recvonly:
            return "recvonly";
        case nua_active_sendrecv:
            return "sendrecv";
        default:
            return "none";
    }
}

void ua_sig_i_state(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    #if 0
        NSLog(@"state-----------------status is %d",status);
        char const *l_sdp = NULL, *r_sdp = NULL;
        int audio = nua_active_inactive, video = nua_active_inactive, chat = nua_active_inactive;
        int offer_recv = 0, answer_recv = 0, offer_sent = 0, answer_sent = 0;
        int ss_state = nua_callstate_init;
        if(!op){
            printf("Error: op is NULL for ssc_i_state");
            return;
        }
    
        tl_gets(tags,
                /*NUTAG_CALLSTATE_REF(ss_state),
                NUTAG_OFFER_RECV_REF(offer_recv),
                NUTAG_ANSWER_RECV_REF(answer_recv),
                NUTAG_OFFER_SENT_REF(offer_sent),
                NUTAG_ANSWER_SENT_REF(answer_sent),*/
                SOATAG_LOCAL_SDP_STR_REF(l_sdp),
                SOATAG_REMOTE_SDP_STR_REF(r_sdp),
                TAG_END());
    
        switch((enum nua_callstate)ss_state){
            case nua_callstate_calling:
                NSLog(@"it is hitting calling state:%d",status);
                break;
                
            case nua_callstate_received:
                NSLog(@"it is getting callstate recieved:%d",status);
                break;
                
            case nua_callstate_proceeding:
                NSLog(@"callstate proceeding:%d",status);
                break;
                    
            case nua_callstate_early:
                NSLog(@"callstate early with 200 response:%d",status);
                break;
                    
            case nua_callstate_completing:
                NSLog(@"callstate completing:%d",status);
                break;
                
            case nua_callstate_ready:
                tl_gets(tags,
                    NUTAG_ACTIVE_AUDIO_REF(audio),
                    NUTAG_ACTIVE_VIDEO_REF(video),
                    NUTAG_ACTIVE_CHAT_REF(chat),
                    TAG_END());
                    NSLog(@"callstate ready:%d",status);
                if(op->op_callstate == opc_ringing){
                    //nua_respond(nh, SIP_200_OK,TAG_IF(l_sdp,SOATAG_USER_SDP_STR(l_sdp)),TAG_END());
                    op->op_callstate = opc_complete;
                }
                break;
        }
        return;
    #else
        memset(to_str1,0,100);
        NSLog(@"state-----------------status is %d",status);
        char const *l_sdp = NULL, *r_sdp = NULL;
        int audio = nua_active_inactive, video = nua_active_inactive, chat = nua_active_inactive;
        int offer_recv = 0, answer_recv = 0, offer_sent = 0, answer_sent = 0;
        int ss_state = nua_callstate_init;
        if(!op){
            printf("Error: op is NULL for ssc_i_state");
            return;
        }
        tl_gets(tags,
                NUTAG_CALLSTATE_REF(ss_state),
                NUTAG_OFFER_RECV_REF(offer_recv),
                NUTAG_ANSWER_RECV_REF(answer_recv),
                NUTAG_OFFER_SENT_REF(offer_sent),
                NUTAG_ANSWER_SENT_REF(answer_sent),
                SOATAG_LOCAL_SDP_STR_REF(l_sdp),
                SOATAG_REMOTE_SDP_STR_REF(r_sdp),
                TAG_END());
    
        switch((enum nua_callstate)ss_state){
            case nua_callstate_init:
                printf("nua_callstate_init");
                break;
                
            case nua_callstate_authenticating:
                printf("nua_callstate_authenticating");
                break;
                
            case nua_callstate_calling:
                NSLog(@"it is hitting calling state:%d",status);
                break;
                
            case nua_callstate_proceeding:
                printf("nua_callstate_proceeding");
                break;
                
            case nua_callstate_completing:
                printf("nua_callstate_completing");
                break;
                
            case nua_callstate_received:
                printf("nua_callstate_received");
                break;
                
            case nua_callstate_early:
                printf("nua_callstate_early");
                break;
                
            case nua_callstate_completed:
                printf("nua_callstate_completed");
                break;
                
            case nua_callstate_ready:
                tl_gets(tags,
                        NUTAG_ACTIVE_AUDIO_REF(audio),
                        NUTAG_ACTIVE_VIDEO_REF(video),
                        NUTAG_ACTIVE_CHAT_REF(chat),
                        TAG_END());
                NSLog(@"callstate ready:%d",status);
                printf("inside nua_callstate : video:%s and audio:%s\n",cli_active(video),cli_active(audio));
                break;
                
            case nua_callstate_terminating:
                printf("BYE Sent");
                [DataSingleton sharedInstance].sipCall_ID = nil;
                break;
                
            case nua_callstate_terminated:
                [DataSingleton sharedInstance].sipCall_ID = nil;
                printf("BYE complete");
                break;
            
        }
    
        if(r_sdp){
            printf("*******got remote_sdp\n %s\n******",r_sdp);
            //[DataSingleton sharedInstance].isAutoReInvite = NO;
        }
        if(op){
            printf("************** op is available  ************\n");
            if(op->op_callstate == opc_ringing){
                //NSLog(@"incoming call with cli: %s",NUTAG_M_DISPLAY(wac1.cli));
                setanswer(1);
                printf("inside 180 video:%s and audio:%s\n",cli_active(video),cli_active(audio));
                printf("from is %s,to is %s",from,to);
                ch = [[NSString alloc] initWithUTF8String:from];
                if(temp){
                    printf("inside 180 video:%s and audio:%s\n",cli_active(video),cli_active(audio));
                    printf("temp is not null");
                }else{
                    printf("temp is null");
                }
                NSLog(@"reached setState when it is not null");
            }else if(op->op_callstate == opc_resp_complete){
                printf("**** For the sent-Invite got 200_OK response  ***\n");
            }
        }
        
    #endif
}

void setanswer(int i){
    answer=i;
}

int checkvalue(){
    if(answer == 1){
        printf("check value");
    }
    return answer;
}

void ua_sig_i_active(nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    assert(op);
    NSLog(@"*** Inside ua_sig_i_active, callstate is active ****\n");
    op->op_callstate = opc_active;
}

void ua_sig_i_terminated(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    printf("inside termination");
    if(op){
        if(op->op_callstate == opc_ringing){
            ua_sig_oper_destroy(ua_sig, op);
        }
        op->op_callstate = 0;
    }
}

#pragma mark R group
static void ua_sig_r_register(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    NSLog(@"*** Inside ua_sig_r_register *****\n");
    long defaultExpiry = 3600;
    [temp sendRegState:status isExpired:false expiry:defaultExpiry];
    NSLog(@" register -----------------  status is %d", status);

    char *currentIpPort = NULL;
    if(status == 200){
        isRegister = YES;
        
        currentIpPort = (char *)malloc(200*sizeof(char));
        char *host = (char *)malloc(100*sizeof(char));
        
        if(sip->sip_via->v_received)
            sprintf(host,"%s",sip->sip_via->v_received);
        else
            sprintf(host,"%s",sip->sip_via->v_host);

        char *port= (char *)malloc(100*sizeof(char));
        if(sip->sip_via->v_rport)
            sprintf(port,"%s",sip->sip_via->v_rport);
        else
            sprintf(port,"%s",sip->sip_via->v_port);
        
        
        sprintf(currentIpPort,"%s<sip:%s@%s:%s;transport=TCP>;expires=3600",wac1.cli,wac1.user,host,port);
        
        NSLog(@"Sofia_Debug :>>>>>>>>>>>>> sip >>>>>>  save register 200 currentIpPort  %s:", currentIpPort);
        NSLog(@"Sofia_Debug :>>>>>>>>>>>>> sip >>>>>>  save register 200 sip->sip_expires  %p:", sip->sip_expires);

        unsigned long delta = defaultExpiry;
        
        if(sip->sip_expires != NULL){
            delta = sip->sip_expires->ex_delta;//Delta seconds.
        }
        
        //static int update_bindings(struct proxy_tr *t)
        //static int validate_contacts(struct proxy_tr *t)
        NSLog(@"save register 200 sip->sip_expires delta %lu :",delta);
        
        //http://sofia-sip.sourceforge.net/refdocs/url/structurl__t.htmlx
        //http://sofia-sip.sourceforge.net/refdocs/sip/structsip__contact__s.html
        
        sip_contact_t  *contacts = sip->sip_contact;
        if(contacts){
            sip_contact_t *m;
            bool isExpireMatch = false;
            
            for(m = contacts; m; m = m->m_next){
                url_t *s_addr = m->m_url;
                
                if(strcmp(s_addr->url_host,host) == 0){
                    NSLog(@"Sofia_Debug :>>>>>>>>>>>>> sip >>>>>> save  m_display matched contact %s >> %s:  %s  =  %s",m->m_expires, m->m_display,s_addr->url_host,host);
                    isExpireMatch = true;
                    //NSString *url_host = [NSString stringWithUTF8String:s_addr->url_host];
                    long expiryTime = [[NSString stringWithUTF8String:m->m_expires] intValue];
                    [temp sendRegState:status isExpired:isExpireMatch expiry:expiryTime];
                    break;
                }else{
                    NSLog(@"Sofia_Debug :>>>>>>>>>>>>> sip >>>>>> save  m_display contact not match  default duration %s >> %s:  %s  =  %s",m->m_expires, m->m_display,s_addr->url_host,host);
                }
            }
            
            if(!isExpireMatch){
                NSLog(@"Sofia_Debug :>>>>>>>>>>>>> sip >>>>>> save  m_display contact not match %s >> %s ",host,"3600");
                //NSString *url_host = [NSString stringWithUTF8String:host];
                [temp sendRegState:status isExpired:isExpireMatch expiry:defaultExpiry];
            }
        }
    }else if((status == 401) || (status == 407)){
        char *host = (char *)malloc(100*sizeof(char));
        
        if(sip->sip_via->v_received)
            sprintf(host,"%s",sip->sip_via->v_received);
        else
            sprintf(host,"%s",sip->sip_via->v_host);
        
        char* port= (char *)malloc(100*sizeof(char));
        if(sip->sip_via->v_rport)
            sprintf(port,"%s",sip->sip_via->v_rport);
        else
            sprintf(port,"%s",sip->sip_via->v_port);
                
        if((status == 401) || (status == 407))
        [DataSingleton sharedInstance].sipContactIpAndPort=[NSString stringWithFormat:@"%s<sip:%s@%s:%s;transport=TCP>;expires=3600",wac1.cli,wac1.user,host,port];
        
        auth_MD5(nh, sip);
        
    }else if(status == 408){
        [temp sendRegState:status isExpired:false expiry:defaultExpiry];
        printf("timeout occurs");
        
    }else if(status == 503){
        [temp sendRegState:status isExpired:false expiry:defaultExpiry];
    }
}

static void auth_MD5(nua_handle_t *nh,sip_t const *sip){
    sip_www_authenticate_t const *wa = sip->sip_www_authenticate;
    sip_proxy_authenticate_t const *pa = sip->sip_proxy_authenticate;
    const char *method = NULL;
    const char *realm = NULL;
    char auth[100]="";
    
    if(wa){
        realm = msg_params_find(wa->au_params, "realm=");
        method = wa->au_scheme;
        
    }else if(pa){
        realm = msg_params_find(pa->au_params, "realm=");
        method = pa->au_scheme;
    }
    
    if(realm == NULL){
        return;
    }
    
    sprintf(auth,"%s:%s:%s:%s",method, realm,  wac1.user ,wac1.password);
    sprintf(auth_bc,"%s:%s:%s:%s",method, realm,  wac1.user ,wac1.password);
               
    auth_handle_temp = nh;
    nua_authenticate(nh, NUTAG_AUTH(auth), TAG_END());
}

void ua_sig_r_unregister(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    //if (ua_sig->s_have_registrar == 0 || status < 200 || status >= 400)
    //if (status < 200 || status >= 400)
    //return;
    printf("entering unregister sequence\n");
    
    /*static int attempt=0;
     if(attempt >= 3){
     su_root_break(ua_sig->s_root);
     printf("entering attempt number:%d\n",attempt);
     attempt++;
     }*/
    //ua_sig->s_connection_state = UA_SHUTDOWN;
    //nua_shutdown(ua_sig->s_nua);
    return;
}

void ua_sig_r_invite(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    if(info == NULL){
        info = [callStatusInfo new];
    }
    info.status = status;
    
    if(sip != NULL){
        if(sip->sip_call_id->i_id != NULL){
            info.call_id = [NSString stringWithCString:sip->sip_call_id->i_id encoding:NSUTF8StringEncoding];
        }
        if(sip->sip_from->a_display != NULL){
            info.from = [NSString stringWithCString:sip->sip_from->a_display encoding:NSUTF8StringEncoding];
        }
        if(sip->sip_to->a_display != NULL){
            info.to = [NSString stringWithCString:sip->sip_to->a_display encoding:NSUTF8StringEncoding];
        }
    }
    
    if(status == 100){
        [temp setEventState:info];
        [DataSingleton sharedInstance].client.call_id = [NSString stringWithCString:sip->sip_call_id->i_id encoding:NSUTF8StringEncoding];
        addNewCall(nh, sip->sip_call_id->i_id);
        
    }else if(status == 180){
        [temp setEventState:info];
        if([DataSingleton sharedInstance].client.dIsPSTN == YES || [[DataSingleton sharedInstance].appCode isEqualToString:@"URAPP"]) {
            if (sip->sip_payload !=nil)
            {
                [DataSingleton sharedInstance].client.incomingPayload = [NSString stringWithCString:sip->sip_payload->pl_data encoding:NSUTF8StringEncoding];
                
                is180=true;
                [[DataSingleton sharedInstance].client creatingAnserForIncomingCall];
            }
        }
        NSLog(@"replied for 180 with sdp for pre--media enabling from ua_sig_r_invite");
        
    }else if(status == 183){
        [temp setEventState:info];
        op->op_callstate = nua_callstate_early;
        
        if([DataSingleton sharedInstance].client.dIsPSTN == YES || [[DataSingleton sharedInstance].appCode isEqualToString:@"URAPP"]){
            if(sip->sip_payload !=nil){
                [DataSingleton sharedInstance].client.incomingPayload = [NSString stringWithCString:sip->sip_payload->pl_data encoding:NSUTF8StringEncoding];
                is180=true;
                [[DataSingleton sharedInstance].client creatingAnserForIncomingCall];
            }
        }
    }else if(status == 200){
        if(sip->sip_payload !=nil){
            [DataSingleton sharedInstance].client.incomingPayload = [NSString stringWithCString:sip->sip_payload->pl_data encoding:NSUTF8StringEncoding];
            [DataSingleton sharedInstance].ispersonInCall = YES;
            [SofiaWrapperThingy callStatus:@"Connected"];//suresh connected call Mobile-A Party
            [temp setEventState:info];
            if(is180 != true){
                [[DataSingleton sharedInstance].client creatingAnserForIncomingCall];   //pintu 24-9-16
            }
            NSLog(@"entering signaling message type answer ");
        }
        op->op_callstate = opc_resp_complete;
        return;
        
    }else if((status == 401) || (status == 407)){
        op->op_callstate &= ~opc_sent;
        auth_MD5(nh, sip);
        
    }else if(status == 408){
        [temp setEventState:info];
        [SofiaWrapperThingy callStatus:@"Failed"];
        ua_sig_oper_destroy(ua_sig, op);
        
    }else if(status == 503){
        [temp setEventState:info];
        ua_sig_oper_destroy(ua_sig, op);
        [SofiaWrapperThingy callStatus:@"Failed"];
        
    }else if(status == 404){
        [SofiaWrapperThingy callStatus:@"User Busy"];
        [temp setEventState:info];
        ua_sig_oper_destroy(ua_sig, op);
        
    }else if(status == 486){
         [SofiaWrapperThingy callStatus:@"User Busy"];
         [temp setEventState:info];
         ua_sig_oper_destroy(ua_sig, op);
        
    }else if(status == 487){
        [SofiaWrapperThingy callStatus:@"Failed"];
        [temp setEventState:info];
        ua_sig_oper_destroy(ua_sig, op);
        
    }else if(status == 480){
      [temp setEventState:info];
      [SofiaWrapperThingy callStatus:@"Failed"];
        
    }else if(status == 481){
        [temp setEventState:info];
        [SofiaWrapperThingy callStatus:@"Failed"];
        
    }else if((status == 600) || (status == 603)){
      [temp setEventState:info];
      [SofiaWrapperThingy callStatus:@"Failed"];
        ua_sig_oper_destroy(ua_sig, op);
    }
}

void ua_sig_r_message(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    /*if(status < 200) {
        return;
    }else if (status == 200) {
        return;
    }else if(status == 408){
        [temp SetEventState:status];
        [SofiaWrapperThingy callStatus:@"Failed"];
    }else if(status == 503){
        [temp SetEventState:status];
        [SofiaWrapperThingy callStatus:@"Failed"];
    }else if(status == 404){
        [temp SetEventState:status];
        [SofiaWrapperThingy callStatus:@"Failed"];
    }*/
}

-(void)setEventState:(callStatusInfo*)info{
    NSLog(@"call function SetInEventState");
    [_delegate SofiaWrapperThingySIPEventstate:info];
}

-(void)sendRegState:(int)state isExpired:(bool)expired expiry:(long)expiry;{
    [_delegate SofiaWrapperThingyRegisterStatus:state isExpired:expired expiry:expiry];
}

static void ua_sig_r_get_params(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    //GaimAccount *account = ua_sig->s_account;
    const sip_contact_t *contact = NULL;
    ua_sig_oper_t *pr_op = NULL;
    char const *user = NULL;
    char const *pr_event = "presence";
    
    //If the "Enable local presence server" is checked in prpl options
    //if (ua_sig->s_have_presence_server)
    //At this point username is formalized (in ua_sig_login),no regexp needed
    
    tl_gets(tags,NTATAG_CONTACT_REF(contact),TAG_END());
    
    //This creates a presence server == hook for incoming subscriptions
    pr_op = ua_sig_oper_create(ua_sig, SIP_METHOD_SUBSCRIBE, user, TAG_END());
        
    nua_notifier(pr_op->op_handle,
                 NUTAG_URL(contact->m_url),
                 SIPTAG_EXPIRES_STR("3600"),
                 SIPTAG_FROM_STR(ua_sig->s_from),
                 SIPTAG_EVENT_STR(pr_event),
                 SIPTAG_CONTENT_TYPE_STR("application/pidf-partial+xml"),
                 NUTAG_SUBSTATE(nua_substate_pending),
                 TAG_END());
    return;
}

static void ua_sig_r_notifier(nua_event_t event, int status,char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]) {
    nua_respond(nh,SIP_200_OK,TAG_END());
    printf("responding with notification ack");
    return;
}

void ua_sig_r_shutdown(nua_event_t event, int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]) {
    //wait until the stack is finished
    if(status < 200 && status >= 400){
       return;
    }
    if(ua_sig->s_root != NULL){
        //nua_destroy(ua_sig->s_nua), ua_sig->s_nua = NULL;
        su_root_break(ua_sig->s_root);
        //g_source_destroy(ua_sig->s_gs), ua_sig->s_gs = NULL;
        su_root_destroy(ua_sig->s_root), ua_sig->s_root = NULL;
    }
    su_home_deinit(ua_sig->s_home);
    su_deinit();
}

void ack(nua_handle_t *nh){
    if(nh){
        nua_ack(nh, TAG_END());
    }
}

void ua_sig_r_bye(int status, char const *phrase, nua_t *nua, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]) {
    [DataSingleton sharedInstance].sipCall_ID = nil;
    assert(op);
    assert(op->op_handle==nh);
    printf("bye status:%d",status);
    if(status < 200){
        return;
    }
}

void ua_sig_i_bye(nua_t *nua, char const *phrase, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]) {
    [DataSingleton sharedInstance].sipCall_ID = nil;
    
    assert(op);
    assert(op->op_handle==nh);
    if(info == NULL){
        info = [callStatusInfo new];
    }
    info.status = 201;
    
    if(sip != NULL){
        if(sip->sip_call_id->i_id != NULL){
            info.call_id = [NSString stringWithCString:sip->sip_call_id->i_id encoding:NSUTF8StringEncoding];
        }
        if(sip->sip_from->a_display != NULL){
            info.from = [NSString stringWithCString:sip->sip_from->a_display encoding:NSUTF8StringEncoding];
        }
        if(sip->sip_to->a_display != NULL){
            info.to = [NSString stringWithCString:sip->sip_to->a_display encoding:NSUTF8StringEncoding];
        }
    }
    fputs(" INCOMING BYE received",stdout);
}

void ua_sig_r_subscribe(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]) {
    if(status >= 200 && status < 300){
        return;
        
    }else if (status == 401 || status == 407){
        //   char const *scheme = NULL;
        //    char const *realm = NULL;
        char *auth = NULL;
        
        /* Laxman
         if (sip && sip->sip_www_authenticate) {
         scheme = (char const *) sip->sip_www_authenticate->au_scheme;
         realm = (char const *) *sip->sip_www_authenticate->au_params;
         }
         else if (sip && sip->sip_proxy_authenticate) {
         scheme = (char const *) sip->sip_proxy_authenticate->au_scheme;
         realm = (char const *) *sip->sip_proxy_authenticate->au_params;
         }
         */
        //oper_set_auth(ua_sig, op, sip, tags);
        nua_authenticate(op->op_handle, NUTAG_AUTH(auth), TAG_END());
    }else{
        NSLog(@"Subscription failed\n");
        ua_sig_oper_destroy(ua_sig, op);
        nua_handle_destroy(nh); /* XXX -- do not try to do anything for some time */
    }
    return;
}

static void ua_sig_r_authorize(int status, char const *phrase, nua_t *nua, ua_sig_t *ua_sig, nua_handle_t *nh, ua_sig_oper_t *op, sip_t const *sip, tagi_t tags[]){
    return;
}

#pragma mark
void ua_sig_subscribe_button_cb(const char* caller){
    if(ua_sig->s_nua){
        op = ua_sig_oper_create(ua_sig, SIP_METHOD_SUBSCRIBE,caller, TAG_END());
    }
        
    if(op->op_handle){
        nua_subscribe(op->op_handle,
                      SIPTAG_EXPIRES_STR("3600"),
                      SIPTAG_EVENT_STR("presence"),
                      SIPTAG_ACCEPT_STR("application/pidf+xml"),
                      TAG_END());
        printf("subscription happens for user:%s\n",caller);
    }
}

void ua_sig_invite_button_cb(const char *callee,bool isAudioOnly,bool isPSTN){
    is180=false;
    NSLog(@"Sofia: sending away INVITE");
    [DataSingleton sharedInstance].client.outgoingPayload = [[DataSingleton sharedInstance].client.outgoingPayload stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
    
    NSString *outgoingOptPayload = [[DataSingleton sharedInstance].client optimizePayload:[DataSingleton sharedInstance].client.outgoingPayload];
    char const *l_sdp = [outgoingOptPayload UTF8String];
    //char const *l_sdp = [[DataSingleton sharedInstance].outgoingPayload  UTF8String];
    
    if(ua_sig == NULL){
        webrtc_account_t *account = &wac1;
        ua_sig = ua_sig_create_session(account);
    }
    
    if(ua_sig != NULL){
        printf("********ua_sig not null ********");
        printf("********ua_sig inside button cb ");
        
        if(ua_sig->s_nua != NULL){
            printf("********ua_sig->s_nua not null ********:");
        }else{
            printf("********ua_sig->s_nua is null ********");
        }
       
        if(ua_sig->s_nua!=NULL){
            op = ua_sig_oper_create(ua_sig, SIP_METHOD_INVITE,callee, TAG_END());
            
        }else if(!ua_sig->s_nua && ua_sig != NULL){
            op = ua_sig_oper_create(ua_sig, SIP_METHOD_INVITE,callee, TAG_END());
        }
    }else{
        printf("********ua_sig is null ********");
        [SofiaWrapperThingy sofiaEndCall];
        return;
    }
    
    NSString *ws_call_type;
    if(isAudioOnly){
        ws_call_type = @"X-webcall:audio";
    }else{
        ws_call_type = @"X-webcall:video";
    }
    const char  *call_type = [ws_call_type cStringUsingEncoding:NSUTF8StringEncoding];
    
    /*NSString *is_PSTN;
    if(isPSTN){
        is_PSTN =@"X-Call: pstn";
    }else{
        is_PSTN = @"X-Call: app";
    }
    const char  *isPSTNCall = [is_PSTN cStringUsingEncoding:NSUTF8StringEncoding];*/

    char * to_str2 = (char *)malloc(200*sizeof(char));
    
    NSLog(@"dynamic domain is: %@",[DataSingleton sharedInstance].dynamicdomainString);
    NSString *ddomainis=[DataSingleton sharedInstance].dynamicdomainString;

    if([DataSingleton sharedInstance].isMiddleEast == YES){
        ddomainis = [ddomainis stringByAppendingString:@":443;transport=tls"];
    }
    char* domain = (char*) [ddomainis cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    if ([ddomainis isEqual: [NSNull null]]) {
        ddomainis = @"6367.UR.mundio.com";
        domain = (char*) [ddomainis cStringUsingEncoding:[NSString defaultCStringEncoding]];
    }
    NSLog(@"ddomainis : %@", ddomainis);
    
    sprintf(to_str2,"sip:%s@%s:5060",callee,domain);

    NSString *device_type =[NSString stringWithFormat:@"X-device:IOS,%@",[DataSingleton sharedInstance].appCode];
    const char  *deviceType = [device_type cStringUsingEncoding:NSUTF8StringEncoding];
    if(op){
        if(([DataSingleton sharedInstance].sipContactIpAndPort != NULL) & ([[DataSingleton sharedInstance].sipContactIpAndPort length]>0)){
            const char  *contact= [[DataSingleton sharedInstance].sipContactIpAndPort cStringUsingEncoding:NSUTF8StringEncoding];
            nua_invite(op->op_handle,
                       //NUTAG_MEDIA_ENABLE(1),
                       //SIPTAG_UNKNOWN_STR(isPSTNCall),
                       //NUTAG_AUTOANSWER(0),
                       //STUNTAG_DOMAIN("turn:stun02.mundio.com:3478"),
                       SIPTAG_TO_STR(to_str2),
                       SIPTAG_UNKNOWN_STR(call_type),
                       SIPTAG_UNKNOWN_STR(deviceType),
                       SIPTAG_CONTACT_STR(contact),
                       SOATAG_USER_SDP_STR(l_sdp),
                       SIPTAG_USER_AGENT_STR(setUserAgent()),
                       TAG_END());
        }else{
            nua_invite(op->op_handle,
                       //NUTAG_MEDIA_ENABLE(1),
                       //SIPTAG_UNKNOWN_STR(isPSTNCall),
                       //NUTAG_AUTOANSWER(0),
                       //STUNTAG_DOMAIN("turn:stun02.mundio.com:3478"),
                       SIPTAG_TO_STR(to_str2),
                       SIPTAG_UNKNOWN_STR(call_type),
                       SIPTAG_UNKNOWN_STR(deviceType),
                       SOATAG_USER_SDP_STR(l_sdp),
                       SIPTAG_USER_AGENT_STR(setUserAgent()),
                       TAG_END());
        }
        op->op_callstate |= opc_sent;
    }else{
        NSLog(@"it is returning without doing nothing");
    }
    return;
}

const char *setUserAgent(){
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    const char *useragent =[[NSString stringWithFormat:@"%@,v-%@,%@,IOS",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"],version,[DataSingleton sharedInstance].appCode] cStringUsingEncoding:NSUTF8StringEncoding];
    return useragent;
}

//This is using when mobile is in lock and application is in background
void ua_sig_options_button_cb(char *callee) {
    printf(" ***** Inside option button callback ****\n");
    op = ua_sig_oper_create(ua_sig, SIP_METHOD_OPTIONS,callee, TAG_END());
    if(!op){
        return;
    }
    //get the ports and list of media
    nua_options(op->op_handle, TAG_END());
}

//Create a new SIP operation with nua_handle().
ua_sig_oper_t * ua_sig_oper_create(ua_sig_t *cli, sip_method_t method, char const *name, const char *address, tag_type_t tag, tag_value_t value, ...){
    int have_url = 1;
    NSLog(@"**** Inside ua_sig_oper_create, name is %s  and address is %s ******\n",name,address);
    char *sip_uri = (char *)malloc(200*sizeof(char));
    char *to_str = (char *)malloc(200*sizeof(char));
    char *from_str = (char *)malloc(200*sizeof(char));
    
    sip_uri = form_sip_uri((char*)address);
    
    if((method == sip_method_register) || (method == sip_method_subscribe)){
        to_str= form_to_str1(wac1.cli);
        
    }else{
        to_str= form_to_str((char*)address);
    }
    from_str = form_from_str();
    
    ta_list ta;
    op = (ua_sig_oper_t *) malloc(sizeof(ua_sig_oper_t));
    NSLog(@"MM URI %s %s",to_str,sip_uri);

    if(strcmp(name,"REGISTER") == 0){
        have_url = 0;
    }
    
    ta_start(ta, tag, value);
    if(cli->s_nua!=NULL){
        n1=cli->s_nua;
        op->op_handle = nua_handle(cli->s_nua,
                                   op,
                                   TAG_IF(have_url,NUTAG_URL(sip_uri)),
                                   SIPTAG_TO_STR(to_str),
                                   SIPTAG_FROM_STR(from_str),
                                   //SIPTAG_CONTACT_STR("sip:503@10.32.2.32:5062"),
                                   ta_tags(ta));
        NSLog(@"continue inside null nua loop %s",name);
    }
    
    if(cli->s_nua == NULL){
        if(n1 != NULL){
            op->op_handle = nua_handle(n1,
                                       op,
                                       TAG_IF(have_url,NUTAG_URL(sip_uri)),
                                       SIPTAG_TO_STR(to_str),
                                       SIPTAG_FROM_STR(from_str),
                                       //SIPTAG_CONTACT_STR("sip:503@10.32.2.32:5062"),
                                       ta_tags(ta));
        }
        NSLog(@"continue inside null nua loop");
    }
    
    ta_end(ta);
    free(sip_uri);
    free(to_str);
    free(from_str);
    
    if(op != NULL ){
        NSLog(@"******** op is not null ***********");
        if(op->op_handle != NULL){
            NSLog(@"******** op->op_handle is not null ***********");
        }
    }
    return op;
}

ua_sig_oper_t * ua_sig_oper_create2(ua_sig_t *cli,sip_method_t method,char const *name,nua_handle_t *nh,sip_from_t const *from){
    if((op = (ua_sig_oper_t *) malloc(sizeof(ua_sig_oper_t)))){
        nua_handle_bind(op->op_handle = nh, op);
    }
    return op;
}

void ua_sig_reject_button_cb(void){
    [DataSingleton sharedInstance].sipCall_ID = nil;
    if(op){
        nua_cancel(op->op_handle,TAG_END());
        op->op_callstate=0;
        NSLog(@"nuha reinvite is rejecting nua handle made to null");
    }
}

void ua_sig_bye_button_cb(void){
    [DataSingleton sharedInstance].sipCall_ID = nil;
    if(op){
        nua_bye(op->op_handle, TAG_END());
        op->op_callstate = 0;
        NSLog(@"ending nua handle call made to null");
    }
    
    if(temp_handle != NULL){
        nua_bye(temp_handle, TAG_END());
    }
    return;
}

void ua_sig_msg_cb(const char *callee,const char *msg){
    op = ua_sig_oper_create(ua_sig, SIP_METHOD_MESSAGE, callee, TAG_END());
    nua_message(op->op_handle,
                SIPTAG_CONTENT_TYPE_STR("text/plain"),
                TAG_IF(msg,SIPTAG_PAYLOAD_STR(msg)),
                TAG_END());
    return;
}

void ua_sig_unreg_cb(void){
    char* to_str = (char *)malloc(200*sizeof(char));
    sprintf(to_str,"<sip:%s@%s:%s>",wac1.user,wac1.domain,wac1.s_port);
    NSLog(@"unregister sequence call back");
    
    if(op){
        NSLog(@"ua_sig_unreg_cb----coming inside of unregister\n");
        nua_unregister(op->op_handle,
                       SIPTAG_TO_STR(to_str),
                       //SIPTAG_CONTACT_STR("*"),
                       //SIPTAG_EXPIRES_STR("0"),
                       TAG_NULL());
    }
    return;
}

char * form_to_str(char *user){
    char* to_str = (char *)malloc(200*sizeof(char));
    if([DataSingleton sharedInstance].isMiddleEast == YES){
        sprintf(to_str,"sips:%s@%s:%s;transport=tls",user,wac1.domain,wac1.s_port);
        
    }else{
        sprintf(to_str,"sip:%s@%s:%s;transport=TCP",user,wac1.domain,wac1.s_port);
    }
    
    NSLog(@"\n form_to_str %s \n",to_str);
    return to_str;
}

char * form_to_str1(char *user){
    char* to_str = (char *)malloc(200*sizeof(char));
    if([DataSingleton sharedInstance].isMiddleEast == YES){
        sprintf(to_str,"sips:%s@%s:%s;transport=tls",user,wac1.domain,wac1.s_port);
    }else{
        sprintf(to_str,"sip:%s@%s:%s;transport=TCP",user,wac1.domain,wac1.s_port);
    }
    NSLog(@"\n form_to_str1 %s \n",to_str);
    return to_str;
}

char * form_from_str(void){
    char* from_str = (char *)malloc(200*sizeof(char));
    
    char *tempFromUser = (char*) [[DataSingleton sharedInstance].mMobileNumber cStringUsingEncoding:[NSString defaultCStringEncoding]];
    if(![DataSingleton sharedInstance].dIsVectoneNumber){
        sprintf(from_str,"%s <sip:%s@%s:%s>",tempFromUser,wac1.user,wac1.domain,wac1.s_port);
    }else{
        sprintf(from_str,"%s <sip:%s@%s:%s>",wac1.cli,wac1.user,wac1.domain,wac1.s_port);
    }
    return from_str;
}

char * form_sip_uri(char *user){
    char* sip_uri = (char *)malloc(200*sizeof(char));
    if([DataSingleton sharedInstance].isMiddleEast == YES){
        sprintf(sip_uri,"sips:%s@%s:%s",user,wac1.domain,wac1.s_port);
        
    }else{
        sprintf(sip_uri,"sip:%s@%s:%s",user,wac1.domain,wac1.s_port);
    }
    NSLog(@"\n form_sip_uri %s \n",sip_uri);
    return sip_uri;
}

void ualog(char *p){
    FILE *fp;
    char timebuff[50];
    char textbuff[256];
    time_t curtime;
    struct tm *tym_info;
    
    time(&curtime);
    tym_info = (struct tm *)localtime(&curtime);
    strftime(timebuff,sizeof(timebuff),"%H:%M:%S %x",tym_info);
    fp = fopen("/root/log.txt","a");
    sprintf(textbuff,"%s %s:%d %s \n",timebuff,__FILE__,__LINE__,p);
    
    if(fp == NULL){
        NSLog(@"*** Unable to open the file ****\n");
    }else{
        fwrite(textbuff,strlen(textbuff),1,fp);
    }
    fclose(fp);
}

//Set operation to be authenticated
void oper_set_auth(ua_sig_t *ssip, ua_sig_oper_t *op, sip_t const *sip, tagi_t *tags){
    if(sip != NULL){
        sip_www_authenticate_t const *wa = sip->sip_www_authenticate;
        sip_proxy_authenticate_t const *pa = sip->sip_proxy_authenticate;
        tl_gets(tags,
                SIPTAG_WWW_AUTHENTICATE_REF(wa),
                SIPTAG_PROXY_AUTHENTICATE_REF(pa),
                TAG_NULL());
    }
    op->op_authstate = 1;
}

//Delete operation and attached handles and identities
void ua_sig_oper_destroy(ua_sig_t *cli, ua_sig_oper_t *op){
    NSLog(@"****** Inside ua_sig_oper_destroy ***************\n");
}

void addNewCall(nua_handle_t *current_handle, const char *call_id){
    struct sofia_call_details *record = find_call(call_id);
    if(record == NULL){
        int n =  sizeof callList / sizeof callList[0];
        
        for(int i = 0; i < n; i++){
            if(callList[i].call_id == NULL){
                callList[i].current_handle = current_handle;
                callList[i].call_id = call_id;
                callList[i].index = i;
                return;
           }
        }
    }else{
        record->current_handle = current_handle;
    }
}

struct sofia_call_details *find_call(const char *call_id){
    int n =  sizeof callList / sizeof callList[0];
    
    for(int i = 0; i < n; i++){
       if(callList[i].call_id != NULL){
        if(strcmp(callList[i].call_id,call_id) == 0)
            return &(callList[i]);
       }
    }
    return NULL; /* Not found */
}

void deleteCallList(){
    int n =  sizeof callList / sizeof callList[0];
    for(int i = 0; i < n; i++){
        callList[i].call_id = NULL;
        callList[i].current_handle = NULL;
        callList[i].index = 0;
    }
    [DataSingleton sharedInstance].ispersonInCall = NO;
}

#pragma mark
#pragma mark ObjectiveC stuff wrapping stuff (end of long section from waaaaay above)

+(NSString*)printmessage{
    NSString *matter=[NSString stringWithUTF8String:printmessage()];
    NSLog(@"printing content:%@\n",matter);
    return matter;
}

+(void)reInvite{
    char* to_str = (char *)malloc(200*sizeof(char));
    to_str = form_to_str("919739084997");
    //sip_replaces_t *replaces = nua_handle_make_replaces(op->op_handle, NULL, 0);
    /*nua_refer(reInviteHandle,
              SIPTAG_REFER_TO_STR(to_str),
              SIPTAG_REPLACES(replaces),
              TAG_END());*/
}

+(void)sofiaMergeCalls {
    if(([DataSingleton sharedInstance].client != nil) && ([DataSingleton sharedInstance].client_L2 != nil)){
        for(RTCRtpTransceiver *transceiver in [DataSingleton sharedInstance].client.peerConnection.transceivers){
            [[DataSingleton sharedInstance].client_L2.peerConnection addTrack:transceiver.receiver.track streamIds:@[@"ARDAMS"]];
          }
        
          for(RTCRtpTransceiver *transceiver in [DataSingleton sharedInstance].client_L2.peerConnection.transceivers){
              [[DataSingleton sharedInstance].client.peerConnection addTrack:transceiver.receiver.track streamIds:@[@"ARDAMS"]];
          }
    }
}

+(void)sofiaLineSwitch:(NSString*)iuid {
    if([[DataSingleton sharedInstance].client.iu_id isEqualToString:iuid]){
        [[DataSingleton sharedInstance].client setLocalholdUnhold:NO];
        const char* callid = [[DataSingleton sharedInstance].client.call_id cStringUsingEncoding:NSUTF8StringEncoding];
        holdUnhlod(find_call(callid),false);
        
    }else if([[DataSingleton sharedInstance].client_L2.iu_id isEqualToString:iuid]){
        [[DataSingleton sharedInstance].client_L2 setLocalholdUnhold:NO];
        const char* callid = [[DataSingleton sharedInstance].client_L2.call_id cStringUsingEncoding:NSUTF8StringEncoding];
        holdUnhlod(find_call(callid),false);
    }
}

void holdUnhlod(struct sofia_call_details *record,bool hold){
    if(record != NULL){
        if(record->current_handle != NULL){
            nua_invite(record->current_handle,NUTAG_HOLD(hold),TAG_END());
        }
    }
}

+(void)sofiaReRegistion {
    ua_sig_reg_cb();
}

void ua_sig_reg_cb(void)
{
    NSLog(@"Sofia_Debug :>>>>>>>>>>>>>> sip >>>>> ****** calling nua_shutdown ****\n");

    if(ua_sig->s_nua != NULL)
        nua_shutdown(ua_sig->s_nua);
}


+(void)sofiaRegister:(NSString*)username password:(NSString*)pass mobile:(NSString*)number server:(NSString*)server isMiddleEast:(BOOL)ismiddleEast tlsServerIP:(NSString*)tlsserverIP iuid:(NSString*)iuid callId:(NSString*)callId {
    @try{
        const char *utf8String = [username cStringUsingEncoding:NSUTF8StringEncoding];
        const char *c1 = [pass cStringUsingEncoding:NSUTF8StringEncoding];
        const char *mobno = [number cStringUsingEncoding:NSUTF8StringEncoding];
        const char *tcpServer = [server cStringUsingEncoding:NSUTF8StringEncoding];
        const char *tlsServer = [tlsserverIP cStringUsingEncoding:NSUTF8StringEncoding];
        
        char *iuid_str = (char *)malloc(200*sizeof(char));
        @try{
            if([iuid length]>0){
                sprintf(iuid_str,"iuid:%s", [iuid cStringUsingEncoding:NSUTF8StringEncoding]);
            }else{
                iuid_str = "iuid:";
            }
            NSLog(@"iuid_str:%s\n",iuid_str);
        }@catch(NSException *exception){
            NSLog(@"uId have blank string\n");
        }
        
        @try{
            if(utf8String){
                strcpy(wac1.user, utf8String);
            }
            NSLog(@"print user:%s\n",wac1.user);
        }@catch(NSException *exception){
            NSLog(@"wac1.user part throwing error\n");
        }
        
        @try{
            if(c1){
                strcpy(wac1.password,c1);
            }
            NSLog(@"print password:%s\n",wac1.password);
        }@catch(NSException *exception){
            NSLog(@"Password part throwing exception\n");
        }
        
        @try{
            if(mobno){
                strcpy(wac1.cli,mobno);
            }
            NSLog(@"printing cli:%s\n",wac1.cli);
        }@catch(NSException *exception){
            NSLog(@"wac1.cli dont have value\n");
        }
        
        domainname = (char *)malloc(200*sizeof(char));
        if(ismiddleEast == YES){
            if(tlsServer){
                strcpy(wac1.domain,tlsServer);
                NSLog(@"print domain:%s",tlsServer);
                strcpy(wac1.proxy,tlsServer);
                strcpy(domainname,tlsServer);
            }else{
                strcpy(wac1.proxy," ");
                NSLog(@"tlsServer is null");
            }
            strcpy(wac1.s_port,"443");
            strcpy(wac1.o_port,"443");
            [DataSingleton sharedInstance].dynamicdomainString=tlsserverIP;
        }else{
            if(tcpServer){
                strcpy(wac1.domain,tcpServer);
                NSLog(@"print domain:%s",tcpServer);
                strcpy(wac1.proxy,tcpServer);
                strcpy(domainname, tcpServer);
                
            }else{
                strcpy(wac1.proxy," ");
                NSLog(@"tcpServer is null");
            }
            strcpy(wac1.s_port,"5060");
            strcpy(wac1.o_port,"5060");
            [DataSingleton sharedInstance].dynamicdomainString = server;
        }

        printf("user:%s\t password:%s\t cli:%s\t domain:%s\t proxy:%s\t s_port:%s\t o_port:%s\t",wac1.user,wac1.password,wac1.cli,wac1.domain,wac1.proxy,wac1.s_port,wac1.o_port);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if(strlen(wac1.srvip) == 0){
                DNSResolverService *dnsObj = [[DNSResolverService alloc]init];
                [dnsObj getIPByHost:server completionHandler:^(NSArray *ips){
                    if(ips != NULL){
                        if([ips count]>0){
                            const char *ip=[[ips objectAtIndex:0] cStringUsingEncoding:NSUTF8StringEncoding];
                            strcpy(wac1.srvip,ip);
                            strcpy(wac1.proxy,ip);
                        }else{
                            if(ismiddleEast == YES){
                                strcpy(wac1.proxy,tlsServer);
                            }else{
                                strcpy(wac1.proxy,tcpServer);
                            }
                        }
                    }
                    ua_sig_login(iuid_str);
                 }];
            }else{
                ua_sig_login(iuid_str);
            }
        });
    }@catch(NSException *exception){
        NSLog(@"sofiaRegister exception = %@",exception.reason);
    }
}

void getPublicIP(){
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    snprintf(ifr.ifr_name, IFNAMSIZ, "eth0");
    ioctl(fd, SIOCGIFADDR, &ifr);
    /* and more importantly */
    printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

    close(fd);
}

+(void)sofiaConference:(NSString *)toNumber{
    NSLog(@"*********** Call Conference ***************");
    const char* call_addon_info = [[NSString stringWithFormat:@"add_on_call:%@,%@",toNumber,[DataSingleton sharedInstance].mMobileNumber] cStringUsingEncoding:NSUTF8StringEncoding];
    if(op){
        nua_info(op->op_handle,
                 SIPTAG_CONTENT_TYPE_STR("application/transfer-info"),
                 SIPTAG_PAYLOAD_STR(call_addon_info),
                 TAG_END());
    }
}

+(void)callBackOption{
    ua_sig_options_button_cb(nil);
}

+(void)sofiaApplicationCallback:(void*)funcptr{
    //func1(funcptr);
}

+(void)sofiaCall:(NSString *)destination callType:(BOOL)isAudioOnly isPSTN:(BOOL)ispst{
    NSLog(@"\n");
    NSLog(@"**** Going to make call ****\n");
    
    const char  *to = [destination cStringUsingEncoding:NSUTF8StringEncoding];
    printf("printing call string :%s\n",to);
    ua_sig_invite_button_cb(to,isAudioOnly,ispst);
}

//call type means park and flip unflip
+(void)sofiaCallParkAndFlipUnflip:(NSString * )handle callType:(NSString*)callType{
    NSLog(@" *********** Call Refer *************** X-transfertype:%@",callType);
    const char* call_type_info = [[NSString stringWithFormat:@"%@=%@",callType,handle] cStringUsingEncoding:NSUTF8StringEncoding];
    NSLog(@" Call refer id : %s",call_type_info);
    
    if(op){
        nua_info(op->op_handle,
                 SIPTAG_CONTENT_TYPE_STR("application/transfer-info"),
                 SIPTAG_PAYLOAD_STR(call_type_info),
                 TAG_END());
    }
    NSLog(@" *********** Call Refer ***************");
}

+(void)switchToNetworkCall:(NSString *)handle switchToNetWork:(NSString*)switchToNetWork {
    NSLog(@" *********** Call Refer *************** switchToNetWork:%@",switchToNetWork);
    
    const char* switchToNetWorkExt = [[NSString stringWithFormat:@"sip:%@@%s?",handle,domainname] cStringUsingEncoding:NSUTF8StringEncoding];
    const char* switchToNetWork_CallT_ype = [[NSString stringWithFormat:@"X-transfertype:%@",switchToNetWork] cStringUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Call switchToNetWorkExt id : %s",switchToNetWorkExt);
    if(op){
        nua_refer(op->op_handle,
                  SIPTAG_REFER_TO_STR(switchToNetWorkExt),
                  SIPTAG_UNKNOWN_STR(switchToNetWork_CallT_ype),
                  TAG_END());
    }
    NSLog(@" *********** Call Refer ***************");
}

+(void)sofiaDTMF:(NSString *)digits {
    digits = [digits stringByReplacingOccurrencesOfString:@"*"
    withString:@"10"];
    
    digits = [digits stringByReplacingOccurrencesOfString:@"#"
    withString:@"11"];

    NSLog(@"*********** DTMF ***************");
    const char* info = [[NSString stringWithFormat:@"Signal= %@ \nDuration= 160",digits] cStringUsingEncoding:NSUTF8StringEncoding];

    if(op){
        nua_info(op->op_handle,
                 SIPTAG_CONTENT_TYPE_STR("application/dtmf-info"),
                 SIPTAG_PAYLOAD_STR(info),
                 TAG_END());
    }
}

+(void)sofiaUserStatus:(BOOL)isActive {
    NSLog(@"*********** Send Status ***************");
    const char* info;

    char* to_str = (char *)malloc(200*sizeof(char));
    sprintf(to_str,"<sip:%s@%s:%s>",wac1.user,wac1.domain,wac1.s_port);

    char* from_str = (char *)malloc(200*sizeof(char));
    sprintf(from_str,"<sip:%s@%s:%s>",wac1.user,wac1.domain,wac1.s_port);
    
    char* request_line = (char *)malloc(200*sizeof(char));
    sprintf(request_line,"INFO sip:%s@%s:%s SIP/2.0",wac1.user,wac1.domain,wac1.s_port);
    
    if(isActive == YES){
        info = [[NSString stringWithFormat:@"AppState = 1"] cStringUsingEncoding:NSUTF8StringEncoding];
    }else{
        info = [[NSString stringWithFormat:@"AppState = 0"] cStringUsingEncoding:NSUTF8StringEncoding];
    }
}

+(void)sofiaReferCall:(NSString * )handle referCallType:(NSString*)referCallType {
    NSLog(@" *********** Call Refer *************** X-transfertype:%@",referCallType);
    
    const char* referToExt = [[NSString stringWithFormat:@"sip:%@@%s?",handle,domainname] cStringUsingEncoding:NSUTF8StringEncoding];
    const char* refer_CallT_ype = [[NSString stringWithFormat:@"X-transfertype:%@",referCallType] cStringUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"Call refer id : %s",referToExt);
    
    if(op){
        nua_refer(op->op_handle,
                  SIPTAG_REFER_TO_STR(referToExt),
                  SIPTAG_UNKNOWN_STR(refer_CallT_ype),
                  TAG_END());
    }
    NSLog(@" *********** Call Refer ***************");
}

+(void)sofiasubscribe:(NSString*)subscriber {
    const char *from=[subscriber cStringUsingEncoding:NSUTF8StringEncoding];
    printf("subscriber details:%s\n",from);
    ua_sig_subscribe_button_cb(from);
}

+(void)sofiahold{
    //NSLog(@"**** going to hold the call ****\n");
    //sofia_hold(1);
}

+(void)sofiahold:(BOOL)option iuid:(NSString *)iu_id {
    if([[DataSingleton sharedInstance].client_L2.iu_id isEqualToString:iu_id]) {
        const char* callid = [[DataSingleton sharedInstance].client_L2.call_id cStringUsingEncoding:NSUTF8StringEncoding];
        [[DataSingleton sharedInstance].client_L2 setLocalholdUnhold:option];
        if(option == YES){
            holdUnhlod(find_call(callid),true);
        }else{
            holdUnhlod(find_call(callid),false);
        }
    }else if([[DataSingleton sharedInstance].client.iu_id isEqualToString:iu_id]){
        const char* callid = [[DataSingleton sharedInstance].client.call_id cStringUsingEncoding:NSUTF8StringEncoding];
        [[DataSingleton sharedInstance].client setLocalholdUnhold:option];
        if(option == YES){
            holdUnhlod(find_call(callid),true);
        }else{
            holdUnhlod(find_call(callid),false);
        }
    }
}

+(void)sofiaAcceptCall:(NSString*)iuid {
    [DataSingleton sharedInstance].isIncomingCallAccept = YES;
    if([DataSingleton sharedInstance].ispersonInCall){
        if([DataSingleton sharedInstance].client_L2 != nil){
            [[DataSingleton sharedInstance].client_L2 configureInComingCall];
        }
    }else{
        if([DataSingleton sharedInstance].client != nil){
            [[DataSingleton sharedInstance].client configureInComingCall];
        }
    }
}

+(void)autoAccept:(NSString*)iuid {
    const char* _iuid = [iuid cStringUsingEncoding:NSUTF8StringEncoding];
    autoAccept(_iuid);
    [DataSingleton sharedInstance].ispersonInCall = YES;
}

+(void)sofiamessage:(NSString*)destination message:(NSString*)msg {
    NSLog(@"\n going to send message");
    const char* to = [destination cStringUsingEncoding:NSUTF8StringEncoding];
    const char* msg1=[msg cStringUsingEncoding:NSUTF8StringEncoding];
    printf("it is to:%s and msg1:%s\n",to,msg1);
    ua_sig_msg_cb(to,msg1);
}

+(void)sofiamessage:(NSString*)destination {
    NSLog(@"\n going to send message");
    const char* to = [destination cStringUsingEncoding:NSUTF8StringEncoding];
    ua_sig_msg_cb(to,"hello world");
}

+(void)sofiaEndCall {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
    });
    NSLog(@"***** Going to end the call ****\n");
    [DataSingleton sharedInstance].sipCall_ID = nil;
    [DataSingleton sharedInstance].isIncomingCallAccept = NO;
    [DataSingleton sharedInstance].isIncomingCallDecline = NO;
    ua_sig_bye_button_cb();
    deleteCallList();
    dispatch_async(dispatch_get_main_queue(), ^{
        [[DataSingleton sharedInstance].client disconnect];
        [[DataSingleton sharedInstance] removeClient];
    });
}

//Added method to gracefully end the call from B side
+(void)sofiaReceivedCallEnd:(callStatusInfo*)info {
    [DataSingleton sharedInstance].ispersonInCall = NO;
    [DataSingleton sharedInstance].isIncomingCallAccept = NO;
    [DataSingleton sharedInstance].isIncomingCallDecline = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
        [[DataSingleton sharedInstance].client disconnect];
        [[DataSingleton sharedInstance] removeClient];
        NSLog(@"%s", __FUNCTION__);
    });
    
    NSLog(@"Call ending from receiver end");
    [temp setEventState:info];
    deleteCallList();
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
    });
}

+(void)sofiaRejectCall {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
    });
    [DataSingleton sharedInstance].ispersonInCall = NO;
    
    NSLog(@"***Going to reject the call");
    [DataSingleton sharedInstance].sipCall_ID = nil;
    if(op){
        nua_respond(op->op_handle,SIP_486_BUSY_HERE,TAG_END());
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
        [[DataSingleton sharedInstance].client disconnect];
        [[DataSingleton sharedInstance] removeClient];
        NSLog(@"%s", __FUNCTION__);
    });
}

+(void)endOfflineCall{
    if(op){
        [DataSingleton sharedInstance].sipCall_ID = nil;
        nua_bye(op->op_handle, TAG_END());
        op->op_callstate = 0;
        op = NULL;
        NSLog(@"endOfflineCall");
    }
}

+(NSString *)fix488:(NSString *)input{
    NSString *processor;
    processor = [input stringByReplacingOccurrencesOfString:@"TCP/TLS/" withString:@"RTP/SAVPF"];
    NSLog(@"printing value of fix488 processer:%@\n",processor);
    return processor;
}

+(NSString *)killVideoLines:(NSString *)input{
    NSString *processor;
    NSRange range = [input rangeOfString:@"m=video"];
    range.length = [input length] - range.location;
    processor = [input stringByReplacingCharactersInRange:range withString:@""];
    return processor;
}

+(NSString *)fixCandMlinesWithProperIPandPort:(NSString *)input{
    NSString *processor;
    NSRange startRange;
    
    startRange.location = 0;
    NSRange endRange = [input rangeOfString:@"a=candidate"];
    startRange.length = endRange.location;
    processor = [input stringByReplacingCharactersInRange:startRange withString:@""];
    
    NSRange spaceRange;
    
    for(int i = 0; i < 4; i++){
        spaceRange = [processor rangeOfString:@" "];
        startRange.location = 0;
        startRange.length = spaceRange.location + 1; // +1 ---> delete the space itself as well
        processor = [processor stringByReplacingCharactersInRange:startRange withString:@""];
    }
    spaceRange = [processor rangeOfString:@" "];
    NSString *IPaddress = [processor substringToIndex:spaceRange.location];
    
    startRange.location = 0;
    startRange.length = spaceRange.location + 1; // +1 ---> delete the space itself as well
    processor = [processor stringByReplacingCharactersInRange:startRange withString:@""];
    
    spaceRange = [processor rangeOfString:@" "];
    NSString *PortNumber = [processor substringToIndex:spaceRange.location];
    processor = [input stringByReplacingOccurrencesOfString:@"0.0.0.0" withString:IPaddress];
    
    NSString *replacement = [NSString stringWithFormat:@"m=audio %@", PortNumber];
    processor = [processor stringByReplacingOccurrencesOfString:@"m=audio 9" withString:replacement];
    
    replacement = [NSString stringWithFormat:@"a=rtcp:%@", PortNumber];
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtcp:9" withString:replacement];
    
    return processor;
}

+(void)unregister{
    ua_sig_unreg_cb();
}

+(void)callStatus:(NSString *)status {
    
}

+(void)writeToTextFile{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fileName = [NSString stringWithFormat:@"%@/etc/resolv.conf",documentsDirectory];
    NSString *content = @"nameserver fd9f:590a:b158::1";
    [content writeToFile:fileName atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
}

+(void)ShowContentlist{
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@/etc/resolv.conf",documentsDirectory];
    NSString *content = [[NSString alloc] initWithContentsOfFile:fileName usedEncoding:nil error:nil];
    NSLog(@"showContentList:%@\n",content);
}

+(NSString *)getDNSServers {
    //dont forget to link libresolv.lib
    NSMutableString *addresses = [[NSMutableString alloc]initWithString:@"DNS Addresses \n"];
    res_state res = malloc(sizeof(struct __res_state));
    
    int result = res_ninit(res);
    if(result == 0){
        for( int i = 0; i < res->nscount; i++ ){
            NSString *s = [NSString stringWithUTF8String :  inet_ntoa(res->nsaddr_list[i].sin_addr)];
            [addresses appendFormat:@"%@\n",s];
            NSLog(@"%@",s);
        }
    }else{
        [addresses appendString:@" res_init result != 0"];
    }
    return addresses;
}

+(void)setoption:(BOOL)ms{
    iptype = ms;
}

+(BOOL)ipAddress:(NSString *)ipAddress isBetweenIpAddress:(NSString *)rangeStart andIpAddress:(NSString *)rangeEnd{
    uint32_t ip = [self convertIpAddress:ipAddress];
    uint32_t start = [self convertIpAddress:rangeStart];
    uint32_t end = [self convertIpAddress:rangeEnd];
    return ip >= start && ip <= end;
}

-(void)waitForRegistration{
    while(1){
        if(isRegister){
            isRegister = NO;
        }else{
            [NSThread sleepForTimeInterval:20.0f];
            if(!isRegister){
                [temp sendRegState:408 isExpired:false expiry:3600];
            }
        }
        break;
    }
}

@end


