//
//  SofiaWrapperThingy.h
//  webRTC_testbed
//
//  Created by FlorinAdrianOdagiu on 03/12/2015.
//  Copyright © 2015 FlorinAdrianOdagiu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "APPClient.h"
#import "thirdPartyAPI.h"
#import "callStatusInfo.h"

#include <sofia-sip/sip_status.h>
#include <sofia-sip/nua.h>
#include <sofia-sip/nua_tag.h>
#include <sofia-sip/su_tag.h>
#include <sofia-sip/su_tagarg.h>
#include <sofia-sip/sresolv.h>
#include <sofia-sip/sdp.h>
#include <sofia-sip/soa_tag.h>
#include <sofia-sip/sres_sip.h>

#define BUFSIZE 100

#include <stdlib.h>  //some of this stuff is probably not necessary
#include <string.h>  //some of this stuff is probably not necessary
#include <time.h>    //some of this stuff is probably not necessary
#include <assert.h>  //some of this stuff is probably not necessary
#include <pthread.h> //some of this stuff is probably not necessary

@class SofiaWrapperThingy;
@class ChillitalkCDglobal;

@protocol SofiaWrapperThingyDelegate <NSObject>

@required

-(void)SofiaWrapperThingySIPEventstate:(callStatusInfo*)info;
-(void)SofiaWrapperThingyRegisterStatus:(int)status isExpired:(bool)expired expiry:(long)expiry;

@end

@interface SofiaWrapperThingy : NSObject {
    id <SofiaWrapperThingyDelegate> _delegate;
}

@property (nonatomic, strong) id delegate;
@property AVAudioPlayer * ringingPlayer;
@property AVAudioPlayer * callingPlayer;
+(void)sofiaRegister:(NSString*)username password:(NSString*)pass mobile:(NSString*)number server:(NSString*)server isMiddleEast:(BOOL)ismiddleEast tlsServerIP:(NSString*)tlsserverIP iuid:(NSString*)iuid callId:(NSString*)callId;
+(void)deInitResources:(BOOL)isCallFromPushKit;
+(void)sofiamessage:(NSString*)destination message:(NSString*)msg;
+(void)sofiamessage:(NSString *)destination;
+(void)sofiaCall:(NSString *)destination  callType:(BOOL)isVideo isPSTN:(BOOL)ispst;
+(void)sofiaEndCall;
+(void)sofiaConference:(NSString *)toNumber;
+(void)sofiaReferCall:(NSString * )handle referCallType:(NSString*)referCallType;
+(void)sofiaCallParkAndFlipUnflip:(NSString * )handle callType:(NSString*)callType; // call type means park and flip unflip
+(void)switchToNetworkCall:(NSString * )handle switchToNetWork:(NSString*)switchToNetWork;
+(void)sofiaDTMF:(NSString *)digite;
+(void)sofiaUserStatus:(BOOL)isActive;
+(void)sofiaReRegistion;
+(void)unregister;
+(void)sofiaAcceptCall:(NSString*)iuid;
+(void)autoAccept:(NSString*)iuid;
+(void)sofiaLineSwitch:(NSString*)iuid;
+(void)sofiaRejectCall;
+(void)sofiahold:(BOOL)option iuid:(NSString *)iu_id;
+(void)setoption:(BOOL)ms;
+(void)sofiaReceivedCallEnd:(callStatusInfo*)info;
+(void)callBackOption;
+(void)endOfflineCall;
+(void)sofiaMergeCalls;
@property(nonatomic, assign) BOOL flagu;

-(instancetype)initWithDelegate:(id<SofiaWrapperThingyDelegate>)delegate;

struct webrtc_account{
    char user[20];
    char password[20];
    char cli[20];
    char domain[200];
    char proxy[200];
    char s_port[10];
    char o_port[10];
    char srvip[200];
};

typedef struct webrtc_account webrtc_account_t;

struct sofia_call_details
{
    const char *call_id;
    int index;
    nua_handle_t *current_handle;
};

typedef enum ua_connection_states{
    UA_IDLE = 0,
    UA_REGISTERING = 1,
    UA_AUTHENTICATING,
    UA_READY,
    UA_UNREGISTERING,
    UA_SHUTDOWN,
    
}ua_connection_states_t;

struct ua_sig{
    su_home_t      s_home[1];            /**< memory allocation object */
    su_root_t      *s_root;               /**< internal event loop */
    nua_t          *s_nua;                /**< SIP engine object */
    //soa_session_t  *s_soa;                /**< SDP offer answer object */
    nua_handle_t   *s_nh;                 /**< This for presence */
    
    char	         s_from[20];               /**< Our account name */
    char           s_domain[30];             /**< SIP domain */
    char           s_registrar[30];          /**< SIP registrar */
    char           s_proxy[30];              /**< SIP proxy */
    int             s_connection_state;
};

typedef struct ua_sig ua_sig_t;

struct ua_sig_oper{
    
    //  ssip_oper_t   *op_next;
    ///  ssip_t        *op_ssip;       /**< Backpointer */
    
    /**< Remote end identity
     *
     * Contents of To: when initiating, From: when receiving.
     */
    char const   *op_ident;
    
    /** NUA handle */
    nua_handle_t *op_handle;
    
    /** How this handle was used initially */
    ///  sip_method_t  op_method;  /* REGISTER, INVITE, MESSAGE, or SUBSCRIBE */
    char const   *op_method_name;
    
    /** Call state.
     *
     * - opc_sent when initial INVITE has been sent
     * - opc_recv when initial INVITE has been received
     * - opc_complate when 200 Ok has been sent/received
     * - opc_active when media is used
     * - opc_sent when re-INVITE has been sent
     * - opc_recv when re-INVITE has been received
     */
    enum {
        opc_none,
        opc_sent = 1,
        opc_ringing = 3,
        opc_recv= 2,
        opc_complete = 4,
        opc_resp_complete = 5,
        opc_active = 6,
        opc_sent2 = 7,
        opc_recv2 = 8,
    } op_callstate;
    
    int           op_prev_state;     /**< Previous call state */
    unsigned      op_authstate : 1; /**< Does this handle need authentication  */
    unsigned      op_persistent : 1; /**< Is this handle persistent? */
    unsigned      op_referred : 1;
    unsigned :0;
};

typedef struct ua_sig_oper ua_sig_oper_t;

void ua_sig_invite_button_cb(const char *callee,bool isVideo,bool isPSTn);
void ua_sig_bye_button_cb(void);
void ua_sig_reject_button_cb(void);
void ua_sig_msg_cb(const char *recipient,const char *msg);
void ua_sig_unreg_cb();
void sofia_hold(int hold);
void accept_the_call(const char* call_id);


@end
