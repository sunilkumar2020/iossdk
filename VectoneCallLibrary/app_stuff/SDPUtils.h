//
//  SDPUtils.h
//  webRTC_testbed
//
//  Created by Macprobook on 2/11/16.
//  Copyright (c) 2016 FlorinAdrianOdagiu. All rights reserved.
//

#ifndef webRTC_testbed_SDPUtils_h
#define webRTC_testbed_SDPUtils_h

#import <Foundation/Foundation.h>

@class RTCSessionDescription;

@interface SDPUtils : NSObject

// Updates the original SDP description to instead prefer the specified video
// codec. We do this by placing the specified codec at the beginning of the
// codec list if it exists in the sdp.
+ (RTCSessionDescription *)
descriptionForDescription:(RTCSessionDescription *)description
preferredVideoCodec:(NSString *)codec;


+ (RTCSessionDescription *)
removeH264DuplicateVideoCodec:(RTCSessionDescription *)description;

+ (RTCSessionDescription *)
descriptionForDescription:(RTCSessionDescription *)description
preferredAudioCodec:(NSString *)codec;

@end


#endif
