//
//  APPUtilities.h
//  webRTC_testbed
//
//  Created by Macprobook on 2/11/16.
//  Copyright (c) 2016 FlorinAdrianOdagiu. All rights reserved.
//

#ifndef webRTC_testbed_APPUtilities_h
#define webRTC_testbed_APPUtilities_h


#import <Foundation/Foundation.h>
@interface NSDictionary (APPUtilites)

// Creates a dictionary with the keys and values in the JSON object.
+ (NSDictionary *)dictionaryWithJSONString:(NSString *)jsonString;
+ (NSDictionary *)dictionaryWithJSONData:(NSData *)jsonData;

@end

@interface NSURLConnection (APPUtilities)

// Issues an asynchronous request that calls back on main queue.
+ (void)sendAsyncRequest:(NSURLRequest *)request
       completionHandler:(void (^)(NSURLResponse *response,
                                   NSData *data,
                                   NSError *error))completionHandler;

// Posts data to the specified URL.
+ (void)sendAsyncPostToURL:(NSURL *)url
                  withData:(NSData *)data
         completionHandler:(void (^)(BOOL succeeded,
                                     NSData *data))completionHandler;

@end

NSInteger APPGetCpuUsagePercentage();




#endif
