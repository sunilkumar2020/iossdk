//
//  APPClient.h
//
//
//  Created by Macprobook on 2/11/16.
//
//

#ifndef _APPClient_h
#define _APPClient_h

/*
 *  Copyright 2014 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#import <Foundation/Foundation.h>
#import "WebRTC/RTCVideoTrack.h"
#import <AVFoundation/AVFoundation.h>
#import "WebRTC/RTCPeerConnection.h"
#import "WebRTC/RTCPeerConnectionFactory.h"
#import "WebRTC/RTCMacros.h"
#import "WebRTC/RTCMediaStream.h"
#import "WebRTC/RTCDataChannelConfiguration.h"

typedef NS_ENUM(NSInteger, AppClientState) {
    // Disconnected from servers.
    kARDAppClientStateDisconnected,
    // Connecting to servers.
    kARDAppClientStateConnecting,
    // Connected to servers.
    kARDAppClientStateConnected,
    //iceGathering completed
    kARDAppClientStaceIceGatheringCompleted,
    kARDAppClientAudioSessionCreated,
    kARDAppClientReConnecting,
};

typedef enum
{
    RespokeICEConnectionNew,
    RespokeICEConnectionChecking,
    RespokeICEConnectionConnected,
    RespokeICEConnectionCompleted,
    RespokeICEConnectionFailed,
    RespokeICEConnectionDisconnected,
    RespokeICEConnectionClosed,
} MundioICEConnectionState;

typedef enum {
    MundioeviceStateOffline = 0,
    MundioDeviceStateReady,
    MundioDeviceStateBusy
} MundioDeviceState;

typedef enum {
    MundioDeviceConnectivityTypeNone = 0,
    MundioConnectivityTypeWifi,
    MundioConnectivityTypeCellularData,
} MundioConnectivityType;

typedef enum {
    kARDSignalingMessageTypeCandidate,
    kARDSignalingMessageTypeOffer,
    kARDSignalingMessageTypeAnswer,
    kARDSignalingMessageTypeBye,
} ARDSignalingMessageType; //stuff brought over from unincluded ARDSignalingMessage.h

@class AppClient;

@protocol AppClientDelegate <NSObject>

- (void)appClient:(AppClient *)client didChangeState:(AppClientState)state;
- (void)appClient:(AppClient *)client didChangeConnectionState:(RTCIceConnectionState)state;
- (void)appClient:(AppClient *)client didReceiveLocalVideoTrack:(RTCVideoTrack *)localVideoTrack;
- (void)appClient:(AppClient *)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack;
- (void)appClient:(AppClient *)client didError:(NSError *)error;
- (void)appClient:(AppClient *)client iceGatheringChanged1:(RTCIceGatheringState)newState;
- (void)didChangeConnectionState:(NSDictionary*)state;

@end

@interface AppClient : NSObject<RTCPeerConnectionDelegate>{
}

@property (nonatomic, weak) id<AppClientDelegate> delegate;
@property (nonatomic, strong) NSMutableArray* queuedRemoteCandidates;
@property NSDictionary* parameters;
@property BOOL candidatesGathered;
@property AVAudioPlayer * ringingPlayer;
@property BOOL Dtls;
@property BOOL offer1;

@property (nonatomic, strong) NSString *dDialNumber;
@property (nonatomic, strong) NSString *call_id;
@property (nonatomic, strong) NSString *iu_id;
@property (nonatomic, strong) NSString *outgoingPayload;
@property (nonatomic, strong) NSString *incomingPayload;
@property (nonatomic,readwrite) BOOL dIsPSTN;

@property NSMutableArray* remoteMediaStreams;
#if 1

@property(nonatomic, strong) RTCPeerConnection *peerConnection;
@property(nonatomic, strong) RTCPeerConnectionFactory *factory;

@property(nonatomic,strong) RTCAudioTrack *localAudioTrack;
@property(nonatomic,strong) RTCVideoTrack *localVideoTrack;
@property(nonatomic,strong) RTCVideoTrack *remoteVideoTrack;
@property(nonatomic, assign) BOOL isTurnComplete;
@property(nonatomic, assign) BOOL isInitiator;
@property(nonatomic, strong) NSMutableArray *iceServers;
@property(nonatomic, strong) NSMutableArray *iceCandidates;
@property(nonatomic, readwrite) BOOL isAudioOnly; //do I still need it ?
@property(nonatomic, assign) BOOL isSpeakerEnabled;
@property (nonatomic,readwrite) BOOL isSDPMediaChennelEnable;
@property(nonatomic, readwrite) long int preBytesSent;
@property(nonatomic, readwrite) long int preBytesReceived;

@property(nonatomic, readwrite) long int bytesSent;
@property(nonatomic, readwrite) long int bytesReceived;
@property(nonatomic, readwrite) long int googRtt;
@property(nonatomic, readwrite)NSNumber *currentBiteRate;
@property(nonatomic, strong) NSString *sdp;

@property(nonatomic, strong) RTCMediaConstraints *defaultPeerConnectionConstraints;

#endif

@property (nonatomic, readonly) MundioICEConnectionState iceConnectionState;

- (instancetype)initWithDelegate:(id<AppClientDelegate>)delegate;

-(void)muteUnmute:(BOOL)flage;
-(void)muteVideo;
-(void)unmuteVideo;
-(void)sendDTMF:(NSString *)digits;

//Enabling & Disabling Speakerphone
-(void)enableSpeaker;
-(void)disableSpeaker;

-(void)count;
-(void)setcallertype:(BOOL)flag;
+(NSString *)getPrimaryIPAddress;
+(NSString*)getIPAddresses;
-(void)candidateGatheringComplete;

-(NSString*)optimizePayload:(NSString*)inPayload;
//Disconnects from the AppRTC servers and any connected clients.
-(void)disconnect;
-(void)configure:(BOOL) isVideoCall;
-(void)configureInComingCall;
-(void)creatingAnserForIncomingCall;
-(BOOL)createReInviteOffer;
-(void)createReInviteAnswer:(BOOL)isVideo;
-(BOOL)reConnectWhenNetworkChanged;
-(void)removeResource;
-(void)releseSDP:(BOOL)isVideo;

-(void)startGetStatusTimer;
-(void)setLocalholdUnhold:(BOOL)isHold;
-(void)setRemoteHoldUnhold;
-(void)cofigIncomCall;
-(void)answer;
-(void)holdEnableAudioTrack:(BOOL)holdStatus;
@end

#endif
