//
//  SDPUtils.m
//  
//
//  Created by Macprobook on 2/11/16.
//
//

#import <Foundation/Foundation.h>
/*
 *  Copyright 2015 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#import "SDPUtils.h"

#import "WebRTC/RTCLogging.h"
#import "WebRTC/RTCSessionDescription.h"

@implementation SDPUtils

+ (RTCSessionDescription *)
descriptionForDescription:(RTCSessionDescription *)description
preferredVideoCodec:(NSString *)codec {
    NSString *sdpString = description.description;
    NSString *lineSeparator = @"\n";
    NSString *mLineSeparator = @" ";
    // Copied from PeerConnectionClient.java.
    // TODO(tkchin): Move this to a shared C++ file.
    NSMutableArray *lines =
    [NSMutableArray arrayWithArray:
     [sdpString componentsSeparatedByString:lineSeparator]];
    NSInteger mLineIndex = -1;
    NSString *codecRtpMap = nil;
    // a=rtpmap:<payload type> <encoding name>/<clock rate>
    // [/<encoding parameters>]
    NSString *pattern =
    [NSString stringWithFormat:@"^a=rtpmap:(\\d+) %@(/\\d+)+[\r]?$", codec];
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:pattern
                                              options:0
                                                error:nil];
    for (NSInteger i = 0; (i < lines.count) && (mLineIndex == -1 || !codecRtpMap);
         ++i) {
        NSString *line = lines[i];
        if ([line hasPrefix:@"m=video"]) {
            mLineIndex = i;
            continue;
        }
        NSTextCheckingResult *codecMatches =
        [regex firstMatchInString:line
                          options:0
                            range:NSMakeRange(0, line.length)];
        if (codecMatches) {
            codecRtpMap =
            [line substringWithRange:[codecMatches rangeAtIndex:1]];
            continue;
        }
    }
    if (mLineIndex == -1) {
        RTCLog(@"No m=video line, so can't prefer %@", codec);
        return description;
    }
    if (!codecRtpMap) {
        RTCLog(@"No rtpmap for %@", codec);
        return description;
    }
    NSArray *origMLineParts =
    [lines[mLineIndex] componentsSeparatedByString:mLineSeparator];
    if (origMLineParts.count > 3) {
        NSMutableArray *newMLineParts =
        [NSMutableArray arrayWithCapacity:origMLineParts.count];
        NSInteger origPartIndex = 0;
        // Format is: m=<media> <port> <proto> <fmt> ...
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:origMLineParts[origPartIndex++]];
        [newMLineParts addObject:codecRtpMap];
        for (; origPartIndex < origMLineParts.count; ++origPartIndex) {
            if (![codecRtpMap isEqualToString:origMLineParts[origPartIndex]]) {
                [newMLineParts addObject:origMLineParts[origPartIndex]];
            }
        }
        NSString *newMLine =
        [newMLineParts componentsJoinedByString:mLineSeparator];
        [lines replaceObjectAtIndex:mLineIndex
                         withObject:newMLine];
    } else {
        RTCLogWarning(@"Wrong SDP media description format: %@", lines[mLineIndex]);
    }
    
  
    
    NSString *mangledSdpString = [lines componentsJoinedByString:lineSeparator];
    mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
    mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
    return [[RTCSessionDescription alloc] initWithType:description.type
                                                   sdp:mangledSdpString];
}


+ (RTCSessionDescription *)
descriptionForDescription:(RTCSessionDescription *)description
preferredAudioCodec:(NSString *)codec {
    NSString *sdpString = description.description;
       NSString *lineSeparator = @"\n";
       NSString *mLineSeparator = @" ";
       // Copied from PeerConnectionClient.java.
       // TODO(tkchin): Move this to a shared C++ file.
       NSMutableArray *lines =
       [NSMutableArray arrayWithArray:
        [sdpString componentsSeparatedByString:lineSeparator]];
       NSInteger mLineIndex = -1;
       NSString *codecRtpMap = nil;
       // a=rtpmap:<payload type> <encoding name>/<clock rate>
       // [/<encoding parameters>]
       NSString *pattern =
       [NSString stringWithFormat:@"^a=rtpmap:(\\d+) %@(/\\d+)+[\r]?$", codec];
       NSRegularExpression *regex =
       [NSRegularExpression regularExpressionWithPattern:pattern
                                                 options:0
                                                   error:nil];
       for (NSInteger i = 0; (i < lines.count) && (mLineIndex == -1 || !codecRtpMap);
            ++i) {
           NSString *line = lines[i];
           if ([line hasPrefix:@"m=audio"]) {
               mLineIndex = i;
               continue;
           }
           NSTextCheckingResult *codecMatches =
           [regex firstMatchInString:line
                             options:0
                               range:NSMakeRange(0, line.length)];
           if (codecMatches) {
               codecRtpMap =
               [line substringWithRange:[codecMatches rangeAtIndex:1]];
               continue;
           }
       }
       if (mLineIndex == -1) {
           RTCLog(@"No m=video line, so can't prefer %@", codec);
           return description;
       }
       if (!codecRtpMap) {
           RTCLog(@"No rtpmap for %@", codec);
           return description;
       }
       NSArray *origMLineParts =
       [lines[mLineIndex] componentsSeparatedByString:mLineSeparator];
       if (origMLineParts.count > 3) {
           NSMutableArray *newMLineParts =
           [NSMutableArray arrayWithCapacity:origMLineParts.count];
           NSInteger origPartIndex = 0;
           // Format is: m=<media> <port> <proto> <fmt> ...
           [newMLineParts addObject:origMLineParts[origPartIndex++]];
           [newMLineParts addObject:origMLineParts[origPartIndex++]];
           [newMLineParts addObject:origMLineParts[origPartIndex++]];
           [newMLineParts addObject:codecRtpMap];
           for (; origPartIndex < origMLineParts.count; ++origPartIndex) {
               if (![codecRtpMap isEqualToString:origMLineParts[origPartIndex]]) {
                   [newMLineParts addObject:origMLineParts[origPartIndex]];
               }
           }
           NSString *newMLine =
           [newMLineParts componentsJoinedByString:mLineSeparator];
           [lines replaceObjectAtIndex:mLineIndex
                            withObject:newMLine];
       } else {
           RTCLogWarning(@"Wrong SDP media description format: %@", lines[mLineIndex]);
       }
        
       NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@", [NSString stringWithFormat:@"rtpmap:%@",codecRtpMap]];
            NSArray* filteredData = [lines filteredArrayUsingPredicate:predicate];
            NSString *profCodec =@"";
          if([filteredData count]>0)
           {
                profCodec = [filteredData firstObject];
               [lines removeObjectsInArray:filteredData];
           }
        
        NSString *mangledSdpString = [lines componentsJoinedByString:lineSeparator];
        NSRange sdp = [mangledSdpString rangeOfString:@"a=rtpmap:111 opus/48000/2"];

        if (sdp.location != NSNotFound) {
        mangledSdpString = [mangledSdpString stringByReplacingCharactersInRange:sdp withString:[NSString stringWithFormat:@"%@\na=rtpmap:111 opus/48000/2",profCodec]];
        }
  
       mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
       mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
       return [[RTCSessionDescription alloc] initWithType:description.type
                                                      sdp:mangledSdpString];
}


+ (RTCSessionDescription *)
removeH264DuplicateVideoCodec:(RTCSessionDescription *)description
{
      NSString *sdpString = description.description;
      NSString *lineSeparator = @"\n";
      NSMutableArray *lines =
      [NSMutableArray arrayWithArray:
       [sdpString componentsSeparatedByString:lineSeparator]];

    NSMutableArray *newlines=[NSMutableArray new];
    for (NSString* line in lines)
    {
        if([line containsString:@"a=rtpmap:98"] || [line containsString:@"a=rtcp-fb:98"] || [line containsString:@"a=fmtp:98"] || [line containsString:@"a=fmtp:99 apt=98"])
        {
            
        }else if([line containsString:@"m=video 9 TCP/TLS/RTP/SAVPF"])
        {
            [newlines addObject:[line stringByReplacingOccurrencesOfString:@"98 " withString:@""]];
        }else
        {
            [newlines addObject:line];
        }
    }
      NSString *mangledSdpString = [newlines componentsJoinedByString:lineSeparator];
      mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
      mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
      return [[RTCSessionDescription alloc] initWithType:description.type
                                                     sdp:mangledSdpString];
}
@end

