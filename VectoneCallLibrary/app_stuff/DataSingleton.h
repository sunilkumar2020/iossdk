//
//  DataSingleton.h
//  webRTC_testbed
//
//  Created by Macprobook on 2/11/16.
//  Copyright (c) 2016 FlorinAdrianOdagiu. All rights reserved.
//

#ifndef webRTC_testbed_DataSingleton_h
#define webRTC_testbed_DataSingleton_h

#import <Foundation/Foundation.h>
#import "APPClient.h"

@interface DataSingleton : NSObject

+(DataSingleton *) sharedInstance;

@property (nonatomic, readwrite) BOOL isSpeakerON;
@property (strong, nonatomic) AppClient *client;
@property (strong, nonatomic, retain) AppClient *client_L2;
@property (strong, nonatomic) NSString  *sipCall_ID;
@property (nonatomic, strong)  NSString *appCode;
@property (nonatomic, strong)  NSString *userID;
@property (nonatomic, readwrite)  NSString *callID;
@property (nonatomic, strong)  NSString *mpassword;
@property (nonatomic, strong)  NSString *mMobileNumber;
@property (nonatomic, strong)  NSString *sipContactIpAndPort;
@property (nonatomic, readwrite) BOOL dIsVectoneNumber;
@property (nonatomic, readwrite) BOOL isNetRechable;
@property (nonatomic, readwrite) int currentStatus;
@property  BOOL ispersonInCall;
@property (nonatomic, strong)  NSString *tlsServer;
@property (nonatomic,readwrite) BOOL isMiddleEast;
@property (strong,nonatomic) NSString  *old_IPAddress;
@property BOOL isIncomingCallAccept;
@property BOOL isIncomingCallDecline;
@property (nonatomic, strong) NSString *dynamicdomainString;
@property(nonatomic, strong) NSMutableArray<AppClient *> *clientList;

-(void)removeClient;
-(NSString*)getIUID:(NSString*)call_id;
-(void)addClient:(NSString*)call_id iuid:(NSString*)iu_id incomPayload:(NSString*)payload isAudio:(BOOL)isAudio;
@end

#endif
