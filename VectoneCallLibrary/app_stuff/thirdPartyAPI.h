//
//  thirdPartyAPI.h
//  ChillitalkWebRTC
//
//  Created by umn on 19/07/17.
//  Copyright © 2017 mundio. All rights reserved.
//

#ifndef thirdPartyAPI_h
#define thirdPartyAPI_h

#import <Foundation/Foundation.h>

@class RTCVideoTrack;
@class registration;
@protocol ThirdPartyDelegate <NSObject>

@optional

- (void)registration:(registration *)client didreceiveLocalVideoTrack:(RTCVideoTrack *)localVideoTrack;
- (void)registration:(registration *)client didreceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack;
- (void)didReciveConnectionStatus:(NSDictionary*)status;

@end

@interface registration : NSObject {
    
}

@property(nonatomic, weak) id<ThirdPartyDelegate> delegate;
-(void)isNetRechable:(BOOL)isRechabel;
-(void)registrationOnServer:(NSString*)username password:(NSString*)pass mobile:(NSString*)number appCode:(NSString*)code server:(NSString*)server  incomeNumber:(NSString*)inNumber callType:(NSString*)callType iuid:(NSString*)iuid callId:(NSString*)callId registrationCallback:(void (*)(int,bool,long))callBack;
-(void)releasePreviousThread:(BOOL)isCallFromPushKit;
-(void)unRegister_on_server;
-(void)dialPSTNNumber:(NSString *)toNumber iuid:(NSString *)iu_id fromNumber:(NSString *)fromNumber  isVectoneNumber:(bool)isVec dialCallback:(void (*)(id))callBack;
-(void)dialCall:(NSString *)toNumber iuid:(NSString *)iu_id dialCallback:(void (*)(id))callBack;
-(void)acceptCallThirdParty:(NSString*)iuid acceptCallback:(void (*)(id))callBack;
-(void)endCall;
-(void)dialVideoCall:(NSString *)toNumber iuid:(NSString *)iu_id dialCallback:(void (*)(id))callBack  Delegate:(id) mdelegate;
-(void)acceptVideoCall:(id)mdelegate iuid:(NSString*)iu_id callback:(void (*)(id))callBack;
-(void)setVOIPRejected:(BOOL)rejected;
-(void)referDialCall:(NSString *)toNumber referCallType:(NSString *)referCallType callType:(BOOL)isVideo  dialCallback:(void (*)(id))callBack;
-(void)callForParkFlipAndUnFlip:(NSString *)toNumber callType:(NSString *)callType callType:(BOOL)isVideo  dialCallback:(void (*)(id))callBack;
-(void)callHoldUnhold:(NSString *)iuid hold:(BOOL )holdUnholdFlag;
-(void)unmute:(BOOL)isUnmute;
-(void)holdEnableAudioTrack:(BOOL)holdStatus;
-(void)sendDTMF:(NSString *)toNumber;
-(void)changeLine:(NSString *)iuid;
-(void)mergeLine:(NSString *)iuid;
-(void)sendState:(BOOL)isActive;
-(void)isSpeakerEnable:(BOOL)isEnable;
-(int)returnCallConnectionStatus;
-(void)reRegistration;
@end
#endif /* thirdPartyAPI_h */

