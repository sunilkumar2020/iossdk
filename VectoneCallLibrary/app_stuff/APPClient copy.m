//
//  APPClient.m
//
//
//  Created by Macprobook on 2/11/16.
//
//



/*
 *  Copyright 2014 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */



#import <Foundation/Foundation.h>
#import "RTCAVFoundationVideoSource.h"
#import "RTCFileLogger.h"
#import "RTCIceServer.h"
#import "RTCLogging.h"
#import "RTCMediaConstraints.h"
#import "RTCMediaStream.h"
#import "RTCPair.h"
#import "SDPUtils.h"
#import "RTCConfiguration.h"
#import "RTCVideoCapturer.h"

#import "RTCSessionDescription+JSON.h"
#import "RTCIceServer+JSON.h"
#import <Foundation/Foundation.h>
#import "DataSingleton.h"
#import  "RTCTypes.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#import "RTCPeerConnectionFactory.h"
#import "RTCAudioTrack.h"

#import "RTCIceCandidate+JSON.h"



#define IOS_CELLULAR            @"pdp_ip0"
#define WIFI                    @"en0"   // iOS or OSX (simulator)
#define IOS_VPN                 @"utun0"
#define IP_ADDR_IPv4            @"ipv4"
#define IP_ADDR_IPv6            @"ipv6"


 char addrBuf[100];
 char addrBuf1[200];
char addressbuff[200];
static NSString *strIPAddtessFamily;
static NSString * const kARDDefaultSTUNServerUrl = @"turn:stun.l.google.com:19302";
static NSString * const kaServerUrl = @"webrtc.mundio.com";
//static NSString * const kaServerUrl = @"testwebrtc.mundio.com";

static NSString * const kTURNServerUrl= @"turn:stun02.mundio.com:3478?transport=tcp";
//static NSString * const kTURNServerUrl= @"turn:stun02.mundio.com:3478";

static NSString *const kSTUNServerUrl = @"stun:stun02.mundio.com:3478";

//static NSString * const kARDDefaultSTUNServerUrl = @"turn:stun02.mundio.com:3478";


// TODO(tkchin): figure out a better username for CEOD statistics.

static NSString * const kARDTurnRequestUrl = @"https://computeengineondemand.appspot.com"
@"/turn?username=iapprtc&key=4080218913";
static NSString *kARDRoomServerHostUrl =@"https://apprtc.appspot.com";

static NSString * const kARDAppClientErrorDomain = @"AppClient";
static NSInteger const kARDAppClientErrorCreateSDP = -3;
static NSInteger const kARDAppClientErrorSetSDP = -4;
int videocount;
int audiocount;


BOOL video=false;
BOOL offerConstraint=false;

//// We need a proxy to NSTimer because it causes a strong retain cycle. When
//// using the proxy, |invalidate| must be called before it properly deallocs.
//@interface ARDTimerProxy : NSObject
//
//- (instancetype)initWithInterval:(NSTimeInterval)interval
//                         repeats:(BOOL)repeats
//                    timerHandler:(void (^)(void))timerHandler;
//- (void)invalidate;
//
//@end
//
//@implementation ARDTimerProxy {
//  NSTimer *_timer;
//  void (^_timerHandler)(void);
//}
//
//- (instancetype)initWithInterval:(NSTimeInterval)interval
//                         repeats:(BOOL)repeats
//                    timerHandler:(void (^)(void))timerHandler {
//  NSParameterAssert(timerHandler);
//  if (self = [super init]) {
//    _timerHandler = timerHandler;
//    _timer = [NSTimer scheduledTimerWithTimeInterval:interval
//                                              target:self
//                                            selector:@selector(timerDidFire:)
//                                            userInfo:nil
//                                             repeats:repeats];
//  }
//  return self;
//}
//
//- (void)invalidate {
//  [_timer invalidate];
//}
//
//- (void)timerDidFire:(NSTimer *)timer {
//  _timerHandler();
//}
//
//@end


//@implementation ARDAppClient {
//  RTCFileLogger *_fileLogger;
//  ARDTimerProxy *_statsTimer;
//}
@interface AppClient()
{
    RTCMediaConstraints *mediaConstraint;
    NSArray *mandatory ;
    NSDateFormatter *test;
}
@end

@implementation AppClient


//@synthesize shouldGetStats = _shouldGetStats;
@synthesize state = _state;
@synthesize delegate = _delegate;
//@synthesize roomServerClient = _roomServerClient;
//@synthesize channel = _channel;
//@synthesize loopbackChannel = _loopbackChannel;
//@synthesize turnClient = _turnClient;
@synthesize peerConnection = _peerConnection;
@synthesize factory = _factory;
@synthesize localMediaStream = _localMediaStream;
@synthesize  localVideoTrack = _localVideoTrack;
@synthesize localAudioTrack = _localAudioTrack;
//@synthesize messageQueue = _messageQueue;
@synthesize isTurnComplete = _isTurnComplete;
//@synthesize hasReceivedSdp  = _hasReceivedSdp;
//@synthesize roomId = _roomId;
//@synthesize clientId = _clientId;
@synthesize isInitiator = _isInitiator;
@synthesize iceServers = _iceServers;
@synthesize iceCandidates = _iceCandidates;
//@synthesize webSocketURL = _websocketURL;
//@synthesize webSocketRestURL = _websocketRestURL;
@synthesize defaultPeerConnectionConstraints =_defaultPeerConnectionConstraints;
//@synthesize isLoopback = _isLoopback;
@synthesize isAudioOnly = _isAudioOnly;
@synthesize queuedRemoteCandidates;
@synthesize parameters=_parameters;
@synthesize flagi=_flagi;
@synthesize video=_video;
@synthesize sdp = _sdp;
@synthesize Dtls=_Dtls;
@synthesize serverHostUrl = _serverHostUrl;
@synthesize offer1=_offer1;


- (instancetype)init
{
    if (self = [super init])
    {
       _factory = [[RTCPeerConnectionFactory alloc] init];
        _isInitiator =  NO;
        _isIncomingCall = YES;
        
    }
  
    return self;
}

- (instancetype)initWithDelegate:(id<AppClientDelegate>)delegate
{
    if (self = [super init])
    {
        //_roomServerClient = [[ARDAppEngineClient alloc] init];
        _delegate = delegate;
        _factory = [[RTCPeerConnectionFactory alloc] init];
      //  _iceServers = [NSMutableArray arrayWithObject:[self defaultSTUN_TURNServer]];
    
      //  NSURL *turnRequestURL = [NSURL URLWithString:kARDTurnRequestUrl];
      //  _turnClient = [[CEODTURNClient alloc] initWithURL:turnRequestURL];
      //  [self configure];
        
        _isInitiator = YES;
        //test: assume I'm initiator
    }
    return self;
}


#pragma mark - Creating Constrain based on audio / video call

- (RTCMediaConstraints *) mediaConstraints:(BOOL) isVideo
{
    
    NSDictionary *mandatoryConstraints;
    if(!isVideo)
    {
        // murli did changes for new webrtc api
        if([strIPAddtessFamily  isEqual:IP_ADDR_IPv4])
        mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"false",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement", nil];
        else
         mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"false",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6", nil];
    }
    else
    {
        if([strIPAddtessFamily  isEqual:IP_ADDR_IPv4])
        mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"30",kRTCMediaConstraintsMaxFrameRate,@"15",kRTCMediaConstraintsMinFrameRate,@"1280",kRTCMediaConstraintsMaxHeight,@"720",kRTCMediaConstraintsMaxWidth,nil];
        else
 mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"30",kRTCMediaConstraintsMaxFrameRate,@"15",kRTCMediaConstraintsMinFrameRate,@"1280",kRTCMediaConstraintsMaxHeight,@"720",kRTCMediaConstraintsMaxWidth,nil];
        
    }
   
    NSLog(@"inside default peer connection dtls set as on and off based on app and pstn call");
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}




- (void)configure:(BOOL) isVideoCall {
    _isIncomingCall = NO;
    
   test  = [[NSDateFormatter alloc] init];
    [test setDateFormat:@"HH:mm:ss"];
    NSString *str = [test stringFromDate:[NSDate date]];
    NSLog(@"start ice candidate from Party A %@:",str);

    NSLog(@"AppClient::Configure peerconnection \n");
    if(!_factory)
    {
        _factory = nil;
    _factory = [[RTCPeerConnectionFactory alloc] init] ;
 //   [RTCPeerConnectionFactory initializeSSL];  need to implement it for SSL
    }
    

    if(mandatory)
        mandatory = nil;
    

  // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun01.mundio.com:3478"] username:@"admin" password:@"system123"];
   // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
  //  RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun01.mundio.com::3478?transport=tcp"] username:@"admin" password:@"system123"];
    RTCIceServer *turn = [[RTCIceServer alloc] initWithURLStrings:@[kTURNServerUrl] username:@"admin" credential:@"system123"];
    RTCIceServer *sturn = [[RTCIceServer alloc] initWithURLStrings:@[@"stun:stun.l.google.com:19302"] username:@"" credential:@""];

  // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
   //RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
//
    
  //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"root" password:@"vicarage2000"];
    

  //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun.l.google.com:19302"] username:@"" password:@""];
    
  //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    
   // NSArray *iceServers1 = [[NSArray alloc] initWithObjects:sturn,turn,nil];
  NSArray *iceServers1 = [[NSArray alloc] initWithObjects:turn,sturn,nil];

    _iceServers=[[NSMutableArray alloc] initWithArray:iceServers1];

    self.video = isVideoCall;
    if(isVideoCall)
    {
       
        _isAudioOnly = NO;
       // mediaConstraint =[self  defaultOfferConstraints:YES];
    }
    else
    {
         _isAudioOnly = YES;
    //   mediaConstraint =[self  defaultOfferConstraints:NO];
    }

    _iceCandidates = [NSMutableArray array];
   // __weak AppClient *weakSelf = self;
    //AppClient *strongSelf = weakSelf;
    self.isTurnComplete = YES;
    _peerConnection.delegate = self;
    [self startPeerConnection:isVideoCall];
    
}


- (void)configureInComingCall:(BOOL) isVideoCall {
   
    
    if(_factory)
        _factory = nil;
        
    
  //  dispatch_async(dispatch_get_main_queue(),^{
    _factory = [[RTCPeerConnectionFactory alloc] init];
  //  [RTCPeerConnectionFactory initializeSSL];
   // });
   // [NSThread sleepForTimeInterval:0.5f];
    if(mandatory)
        mandatory = nil;
    
  //  RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun01.mundio.com:3478"] username:@"admin" password:@"system123"];
    
  // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478"] username:@"root" password:@"vicarage2000"];
  //  RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
 //   RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
    RTCIceServer *turn = [[RTCIceServer alloc] initWithURLStrings:@[kTURNServerUrl] username:@"admin" credential:@"system123"];
    RTCIceServer *sturn = [[RTCIceServer alloc] initWithURLStrings:@[@"stun:stun.l.google.com:19302"] username:@"" credential:@""];


   // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    
 //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    
 //   RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"root" password:@"vicarage2000"];
    
    _isIncomingCall = YES;
 // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun.l.google.com:19302"] username:@"" password:@""];
    
   // RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
 //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun01.mundio.com"] username:@"" password:@""];
    
  //  NSArray *iceServers1 = [[NSArray alloc] initWithObjects:sturn,turn,nil];
    NSArray *iceServers1 = [[NSArray alloc] initWithObjects:turn,sturn,nil];

    _iceServers=[[NSMutableArray alloc] initWithArray:iceServers1];
    _isInitiator = NO;
    self.video = isVideoCall;
    if(isVideoCall)
    {
        videocount = 5;
        _isAudioOnly = NO;
         }
    else
    {
        _isAudioOnly = YES;
        videocount = 0;
    
    }
    

  
    NSLog(@"printing iceservers:%@ and  turn:%@\n",iceServers1,turn);

    _iceCandidates = [NSMutableArray array];
     self.isTurnComplete = YES;

  
    _peerConnection.delegate = self;
    [self processIncomingInvite:isVideoCall];

    
}



- (void)dealloc {
    //self.shouldGetStats = NO;
    
    [self disconnect];
    
}


- (void)setState:(AppClientState)state {
    if ((_state == state) && ![DataSingleton sharedInstance].isAutoReInvite) {
        return;
    }
    _state = state;
    [_delegate appClient:self didChangeState:_state];
}
//start murli
#if 0
// request for turn server to validate is
- (void)requestServersWithCompletionHandler:
(void (^)(NSArray *turnServers,
          NSError *error))completionHandler {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kaServerUrl]];
    // We need to set origin because TURN provider whitelists requests based on
    // origin.
    [request addValue:@"Mozilla/5.0" forHTTPHeaderField:@"user-agent"];
    [request addValue:kTURNServerUrl forHTTPHeaderField:@"origin"];
    [NSURLConnection sendAsyncRequest:request
                    completionHandler:^(NSURLResponse *response,
                                        NSData *data,
                                        NSError *error) {
                        NSArray *turnServers = [NSArray array];
                        if (error) {
                            completionHandler(turnServers, error);
                            return;
                        }
                        NSDictionary *dict = [NSDictionary dictionaryWithJSONData:data];
                        turnServers = [RTCIceServer serversFromCEODJSONDictionary:dict];
                        if (!turnServers) {
                            NSError *responseError = @"murli Bad response from turn server";
//                           [[NSError alloc] initWithDomain:kARDCEODTURNClientErrorDomain
//                                                       code:kARDCEODTURNClientErrorBadResponse
//                                                   userInfo:@{
//                                                              NSLocalizedDescriptionKey: @"Bad TURN response.",
//                                                              }];
                           completionHandler(turnServers, responseError);
                            return;
                        }
                        completionHandler(turnServers, nil);
                    }];
}



//end murli
- (void)requestTURNServersWithURL:(NSURL *)requestURL
                completionHandler:(void (^)(NSArray *turnServers))completionHandler {
    NSParameterAssert([requestURL absoluteString].length);
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:requestURL];
    // We need to set origin because TURN provider whitelists requests based on
    // origin.
    [request addValue:@"Mozilla/5.0" forHTTPHeaderField:@"user-agent"];
    [request addValue:self.serverHostUrl forHTTPHeaderField:@"origin"];
    [NSURLConnection sendAsyncRequest:request
                    completionHandler:^(NSURLResponse *response,
                                        NSData *data,
                                        NSError *error) {
                        NSArray *turnServers = [NSArray array];
                        if (error) {
                            NSLog(@"Unable to get TURN server.");
                            completionHandler(turnServers);
                            return;
                        }
                        NSDictionary *dict = [NSDictionary dictionaryWithJSONData:data];
                        
                        turnServers = [RTCIceServer serversFromCEODJSONDictionary:dict]; 
                       
                        completionHandler(turnServers);
                    }];
}
#endif

- (void)disconnect
{
 //   [NSThread sleepForTimeInterval:2.0f];
    NSLog(@"mihira pancey \n");
    if (_state == kARDAppClientStateDisconnected)
    {
        return;
    }
    _state = kARDAppClientStateDisconnected;
    [DataSingleton sharedInstance].sipCall_ID = nil;
    [DataSingleton sharedInstance].isAutoReInvite = NO;
    _isInitiator = NO;
    
    if (_peerConnection && self.localMediaStream) {
        [_peerConnection removeStream:self.localMediaStream];
        self.localMediaStream=nil;
        self.localVideoTrack=nil;
        self.localAudioTrack=nil;
    }
    if(_isIncomingCall  && _peerConnection && self.localMediaStream  )
    {
        [_peerConnection removeStream:_localMediaStream];
        self.localMediaStream = nil;
        [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
        [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
        _localAudioTrack = nil;
    }
    [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
    [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
    [_peerConnection close];
   _candidatesGathered =NO;
#if 1  // pintu did it 18-11-16
    [_factory stopAecDump];
    [_peerConnection stopRtcEventLog];
#endif
    [DataSingleton sharedInstance].incomingPayload = nil;
    [DataSingleton sharedInstance].outgoingPayload = nil;
    NSLog(@"changing gathering changed to NO");
    _peerConnection = nil;
 //pintu
  //  [RTCPeerConnectionFactory deinitializeSSL];
    
     // _factory = nil;
        self.state = kARDAppClientStateDisconnected;
    self.queuedRemoteCandidates = nil;
}

#pragma mark - Private



- (void)startPeerConnection:(BOOL) flage
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
  //  self.state = kARDAppClientStateConnected;
    
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
  // config.tcpCandidatePolicy = RTCTcpCandidatePolicyEnabled;
    config.tcpCandidatePolicy = RTCTcpCandidatePolicyDisabled;
    config.bundlePolicy = RTCBundlePolicyBalanced;
    
    
    config.iceServers = _iceServers;
//    config.iceTransportPolicy = RTCIceTransportPolicyRelay;
   config.iceTransportPolicy = RTCIceTransportPolicyAll;
   

    
    
    
    //
    
//   if  ([strIPAddtessFamily  isEqual:IP_ADDR_IPv4])
//   {
//     config.iceTransportPolicy=RTCIceTransportPolicyRelay;  // for ipv 4 we need relay type transport
//   }
//   else
//    {
//    config.iceTransportPolicy =RTCIceTransportPolicyRelay;
//    }
    
   // config.rtcpMuxPolicy=RTCRtcpMuxPolicyNegotiate;
     //murli changes for App to PSTN 2 call failed out of 10 dated 30-11-2016
    config.rtcpMuxPolicy=  RTCRtcpMuxPolicyRequire;
    
    mediaConstraint = [self mediaConstraints:flage];
   // dispatch_sync(dispatch_get_main_queue(),^{
    
    _peerConnection = [_factory peerConnectionWithConfiguration:config
                                                    constraints:mediaConstraint
                                                       delegate:self];
  //  });
    [NSThread sleepForTimeInterval:0.5f]; // murli dated 3-11-16
    NSLog(@"creating peerConnection");
    
    NSLog(@"print iceServers:%@\n",_iceServers);

    
    NSLog(@"checking _peerconnection is null or no");
    
    // Create AV media stream and add it to the peer connection.
    
    printf("printing video value of BOOL value:%d",flage);
    [self createLocalMediaStream:flage];
    [_peerConnection addStream:_localMediaStream];
    
    NSLog(@"added local media stream");
    
     if (_isInitiator)
      {
     NSLog(@"yes, initiator");
    
       NSLog(@"creating offer");
    // Send offer.
          
          // murli did changes for new webrtc
    
          __weak AppClient *weakSelf = self;
          [_peerConnection offerForConstraints:mediaConstraint
                             completionHandler:^(RTCSessionDescription *sdp,
                                                 NSError *error) {
                                 AppClient *strongSelf = weakSelf;
                                 [strongSelf peerConnection:strongSelf.peerConnection
                                didCreateSessionDescription:sdp
                                                      error:error];
                             }];
      }
     else
     {
            // Check if we've received an offer.
         
    
         NSLog(@"Incoming INVITE  processing as offere , not initiator");
     }
    
}






// Processes the messages that we've received from the room server and the
// signaling channel. The offer or answer message must be processed before other
// signaling messages, however they can arrive out of order. Hence, this method
// only processes pending messages if there is a peer connection object and
// if we have received either an offer or answer.
- (void)drainMessageQueueIfReady
{
    //  if (!_peerConnection || !_hasReceivedSdp) {
    //    return;
    //  }
    //  for (ARDSignalingMessage *message in _messageQueue) {
    //    [self processSignalingMessage:message];
    //  }
    //  [_messageQueue removeAllObjects];
}

// Processes the given signaling message based on its type.
//- (void)processSignalingMessage:(ARDSignalingMessage *)message {
//  NSParameterAssert(_peerConnection ||
//      message.type == kARDSignalingMessageTypeBye);
//  switch (message.type) {
//    case kARDSignalingMessageTypeOffer:
//    case kARDSignalingMessageTypeAnswer: {
//      ARDSessionDescriptionMessage *sdpMessage =
//          (ARDSessionDescriptionMessage *)message;
//      RTCSessionDescription *description = sdpMessage.sessionDescription;
//      // Prefer H264 if available.
//      RTCSessionDescription *sdpPreferringH264 =
//          [ARDSDPUtils descriptionForDescription:description
//                             preferredVideoCodec:@"H264"];
//      [_peerConnection setRemoteDescriptionWithDelegate:self
//                                     sessionDescription:sdpPreferringH264];
//      break;
//    }
//    case kARDSignalingMessageTypeCandidate: {
//      ARDICECandidateMessage *candidateMessage =
//          (ARDICECandidateMessage *)message;
//      [_peerConnection addICECandidate:candidateMessage.candidate];
//      break;
//    }
//    case kARDSignalingMessageTypeBye:
//      // Other client disconnected.
//      // TODO(tkchin): support waiting in room for next client. For now just
//      // disconnect.
//      [self disconnect];
//      break;
//  }
//}
-(void)processSignalingMessage/*WithType:(int)type*/  //this thing is for incoming messages only !!!
{
    //  NSParameterAssert(_peerConnection); /modidfied by SM on 12:24
    NSLog(@"changed _peerconenction NSASSERT");
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    

    NSMutableString *answerSDP = [NSMutableString stringWithString:[DataSingleton sharedInstance].incomingPayload];
    
    RTCSessionDescription *incomingDescription;
    RTCSdpType type;
   if(self.isInitiator)
    {
        //this means answer
        type = RTCSdpTypeAnswer;
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        NSLog(@"incoming offer and creatinganswer with initiator flag");
        
    }
    else
    {
        //this means offer
        type = RTCSdpTypeOffer;
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
    }
  


    // murli implemented for new webrtc API
    
     __weak AppClient *weakSelf = self;
     [_peerConnection setRemoteDescription:incomingDescription
                         completionHandler:^(NSError *error) {
                             AppClient *strongSelf = weakSelf;
                             [strongSelf peerConnection:strongSelf.peerConnection
                      didSetSessionDescriptionWithError:error];
                         }];
    
    NSLog(@"setRemoteDescription happening here");
    
    
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    
    
    for (NSString * key in candidates)
    {
        for (NSString * candidate in [candidates objectForKey:key]) {
            // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
            
            iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
           
            if (self.queuedRemoteCandidates)
            {
                [self.queuedRemoteCandidates addObject:candidate];
            }
           
                [_peerConnection addIceCandidate:iceCandidate];
          
        }
    }
    
    NSLog(@"added remote's candidates to peerconnection");
    
    
}


//}
//murli changed for creating offer with iNVITE 180 ringing
-(void)processIncomingInvite:(BOOL)isVideo/*WithType:(int)type*/  //this thing is for incoming messages only !!!
{
    
    
    
    NSLog(@"startPeerConnection \n");
    
   // self.state = kARDAppClientStateConnected;
    if(!isVideo)
        self.state = kARDAppClientAudioSessionCreated;

    RTCConfiguration *config = [[RTCConfiguration alloc] init];
    config.iceServers = _iceServers;
  //  config.tcpCandidatePolicy = RTCTcpCandidatePolicyEnabled;
    config.tcpCandidatePolicy = RTCTcpCandidatePolicyDisabled;
    config.bundlePolicy = RTCBundlePolicyBalanced;
   //  config.iceTransportPolicy= RTCIceTransportPolicyRelay;
    config.iceTransportPolicy=RTCIceTransportPolicyAll;
//    if  ([strIPAddtessFamily  isEqual:IP_ADDR_IPv4])
//    {
//        config.iceTransportPolicy=RTCIceTransportPolicyRelay;  // for ipv 4 we need relay type transport
//    }
//    else
//    {
//        config.iceTransportPolicy =RTCIceTransportPolicyRelay;
//    }
    
 //   config.rtcpMuxPolicy=RTCRtcpMuxPolicyNegotiate;
    //murli changes for App to PSTN 2 call failed out of 10 dated 30-11-2016
    config.rtcpMuxPolicy = RTCRtcpMuxPolicyRequire;
    mediaConstraint = [self mediaConstraints:isVideo];
   
    _peerConnection = [_factory peerConnectionWithConfiguration:config
                                                    constraints:mediaConstraint
                                                       delegate:self];

  //  [NSThread sleepForTimeInterval:0.5f];
    NSLog(@"creating peerConnection");
    
    NSLog(@"print iceServers:%@\n",_iceServers);
    
    
    NSLog(@"checking _peerconnection has null ");
    
    // Create AV media stream and add it to the peer connection.
    
    printf("printing video value of BOOL value:%d",isVideo);
   // self.state = kARDAppClientAudioSessionCreated;
    [self createIncomingLocalMediaStream:isVideo];
    [_peerConnection addStream:_localMediaStream];
    
    NSLog(@"added local media stream");
    
    NSLog(@"creating local sdp offer with remote SDP \n");
    
    NSLog(@"changed _peerconenction NSASSERT");
    
    NSLog(@"processIncomingInvite ------------ stuff from Sofia presumably");
    
    
    NSMutableString *answerSDP = [NSMutableString stringWithString:[DataSingleton sharedInstance].incomingPayload];
    
    RTCSessionDescription *incomingDescription;
     RTCSdpType type;
    type = RTCSdpTypeOffer;
   
        //this means offer
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
   
    // murli changes for new webrtc
    __weak AppClient *weakSelf = self;
       [_peerConnection setRemoteDescription:incomingDescription
                        completionHandler:^(NSError *error) {
                            AppClient *strongSelf = weakSelf;
                            [strongSelf peerConnection:strongSelf.peerConnection
                     didSetSessionDescriptionWithError:error];
                        }];

    
    NSLog(@"setRemoteDescription happening here");
    
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    
    
    for (NSString * key in candidates)
    {
        for (NSString * candidate in [candidates objectForKey:key]) {
            // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
             iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
            if (self.queuedRemoteCandidates)
            {
                [self.queuedRemoteCandidates addObject:candidate];
            }
            
            [_peerConnection addIceCandidate:iceCandidate];
            
        }
    }
    
    
    
    NSLog(@"Offer is created ");
    
    
    NSLog(@"added remote's candidates to peerconnection");
    
    
}
-(BOOL) isH264Codec:(NSString *) str_sdp
{
    BOOL isTrue = FALSE;
    NSString *codecnumber;
    NSArray *arr = [str_sdp componentsSeparatedByString:@"\n"];
    int count = -1;
    for(NSString *str in arr)
    {
        if([str    hasPrefix:@"m=video"])
        {
            NSArray *iscodec = [str componentsSeparatedByString:@" "];
            
               for(NSString *str in iscodec)
               {
                   count++;
                   if([str isEqualToString:@"UDP/TLS/RTP/SAVPF"])
                   {
                       codecnumber = [iscodec objectAtIndex:(count+1)];
                       break;
                   }
               }
            
        }
        else if([str hasPrefix:@"a=rtpmap:" ])
        {
            NSArray *temp = [str componentsSeparatedByString:@" "];
            BOOL isloopbreak = false;
            NSString *codec_number = [NSString stringWithFormat:@"a=rtpmap:%@",codecnumber];
            bool iscodece = NO;
            for(NSString *strstr in temp)
            {
                if([strstr isEqualToString:codec_number])
                    iscodece = true;
                if( iscodece && [strstr containsString:@"H264/90000"])
                {
                    isTrue = YES;
                    NSLog(@"Its H264 codec \n");
                    isloopbreak = YES;
                    break;
                }
                else if ( iscodece && [strstr containsString:@"VP8/90000"])
                    {
                        isTrue = NO;
                        isloopbreak = YES;
                        NSLog(@"Its VP8  codec\n");
                        break;
                    }
            }
            if(isloopbreak)
                break;
        }
        
    }
    
    return isTrue;
}

//pintu
-(void) creatingAnserForIncomingCall
{
    NSLog(@"changed _peerconenction NSASSERT");
    
    NSLog(@"processSignalingMessage ------------ stuff from Sofia presumably murli");
    if([DataSingleton sharedInstance].isAutoReInvite)
    {
      [DataSingleton sharedInstance].outgoingPayload = [[DataSingleton sharedInstance].incomingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:passive"];
    }
    
    
    NSMutableString *answerSDP = [NSMutableString stringWithString:[DataSingleton sharedInstance].incomingPayload];
    
    
    RTCSessionDescription *incomingDescription;
    
           //this means answer
    RTCSdpType type;
    type = RTCSdpTypeAnswer;
    RTCSessionDescription *answer_sdp = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
#if 1
    // Prefer H264 if available.
    // Changes for codec negotiation based on RFC 3264
     if( !_isAudioOnly && [self  isH264Codec:answer_sdp.description] )
    incomingDescription = [SDPUtils descriptionForDescription:answer_sdp preferredVideoCodec:@"H264"];
    else
    {
        
incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        
    }
#endif
  //  incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        NSLog(@"incoming offer and creatinganswer with initiator flag");
    // murli changes for new webrtc

    __weak AppClient *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(),^{
    [_peerConnection setRemoteDescription:incomingDescription
                        completionHandler:^(NSError *error) {
                            AppClient *strongSelf = weakSelf;
                            [strongSelf peerConnection:strongSelf.peerConnection
                     didSetSessionDescriptionWithError:error];
                        }];
    });
    NSLog(@"setRemoteDescription happening here");
    
    
    
    
  self.state =  kARDAppClientStateConnected;
    
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    
    
    for (NSString * key in candidates)
    {
        for (NSString * candidate in [candidates objectForKey:key]) {
            // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
             iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
            if (self.queuedRemoteCandidates)
            {
                [self.queuedRemoteCandidates addObject:candidate];
            }
            
            [_peerConnection addIceCandidate:iceCandidate];
            
        }
    }
    
    
    
       
    
    
    NSLog(@"created Anser at calee party for incoming call to peerconnection");
    
    
}


// Sends a signaling message to the other client. The caller will send messages
// through the room server, whereas the callee will send messages over the
// signaling channel.
//- (void)sendSignalingMessage:(ARDSignalingMessage *)message {
//  if (_isInitiator) {
//    __weak ARDAppClient *weakSelf = self;
//    [_roomServerClient sendMessage:message
//                         forRoomId:_roomId
//                          clientId:_clientId
//                 completionHandler:^(ARDMessageResponse *response,
//                                     NSError *error) {
//      ARDAppClient *strongSelf = weakSelf;
//      if (error) {
//        [strongSelf.delegate appClient:strongSelf didError:error];
//        return;
//      }
//      NSError *messageError =
//          [[strongSelf class] errorForMessageResultType:response.result];
//      if (messageError) {
//        [strongSelf.delegate appClient:strongSelf didError:messageError];
//        return;
//      }
//    }];
//  } else {
//    [_channel sendMessage:message];
//  }
//}



- (RTCMediaStream *)createLocalMediaStream
{
    RTCMediaStream* localStream = [_factory mediaStreamWithStreamId:@"ARDAMS"];
    RTCVideoTrack* localVideoTrack = [self createLocalVideoTrack];
    if (localVideoTrack)
    {
        [localStream addVideoTrack:localVideoTrack];
        [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
        NSLog(@"inside create local video stream");
    }
    [localStream addAudioTrack:[_factory audioTrackWithTrackId:@"ARDAMSa0"]];
    NSLog(@"outside local media steam for local");
    return localStream;
}




- (void)createIncomingLocalMediaStream:(BOOL)video
{
    
    NSLog(@"Creating Local Media \n");

   
    if(video)
    {
      // [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;   //sanju jID
        [DataSingleton sharedInstance].incomingLocalVideoTrack = [self createLocalVideoTrack];
        if ([DataSingleton sharedInstance].incomingLocalVideoTrack)
        {
            [_localMediaStream addVideoTrack:[DataSingleton sharedInstance].incomingLocalVideoTrack];
            [_delegate appClient:self didReceiveLocalVideoTrack:[DataSingleton sharedInstance].incomingLocalVideoTrack];
            NSLog(@"inside local video track");
        }
        
    }
    else
    {
       if (self.localMediaStream)
        {
            [_peerConnection removeStream:self.localMediaStream];
            self.localMediaStream = nil;
            _localAudioTrack = nil;
       }
       if(!_localMediaStream)
       {
        _localMediaStream = [_factory mediaStreamWithStreamId:@"ARDAMS"];
         //   self.state = kARDAppClientAudioSessionCreated;
       }
        
    }
     _localAudioTrack = [_factory audioTrackWithTrackId:@"ARDAMSa0"];
    //dispatch_async(dispatch_get_main_queue(),^{
    [_localMediaStream addAudioTrack:_localAudioTrack];
   // });
    NSLog(@"outside local media stream outside for remote");
    
    
}

-(void) removeResource
{

    if(!_isAudioOnly)
    {
        if(_isIncomingCall  && _peerConnection && self.localMediaStream  )
        {
            NSLog(@"Case2  \n");
            [_peerConnection removeStream:_localMediaStream];
            self.localMediaStream = nil;
            [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
            [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
            _localAudioTrack = nil;
        }
        else if (_peerConnection && self.localMediaStream) {
          
            [_peerConnection removeStream:self.localMediaStream];
            self.localMediaStream=nil;
            self.localVideoTrack=nil;
            self.localAudioTrack=nil;
        }
        
    }
    else if (self.localMediaStream)
    {
        
            [_peerConnection removeStream:self.localMediaStream];
            _localAudioTrack = nil;
            self.localMediaStream = nil;
            
        
    }

 
}


-(void) createLocalMediaStream:(BOOL)video
{
    

 //   _localMediaStream = [_factory mediaStreamWithLabel:@"ARDAMS"];
    if(video == true)
    {
         _localVideoTrack = [self createLocalVideoTrack];
        if (_localVideoTrack)
        {
            [_localMediaStream addVideoTrack:_localVideoTrack];
            if(!_isIncomingCall)
            [_delegate appClient:self didReceiveLocalVideoTrack:_localVideoTrack];
            NSLog(@"inside local video track");
        }
        
    }
    else
    {
        if (self.localMediaStream)
        {
            [_peerConnection removeStream:self.localMediaStream];
            _localAudioTrack = nil;
            self.localMediaStream = nil;
            
        }
        else
       _localMediaStream = [_factory mediaStreamWithStreamId:@"ARDAMS"];
        
    }
    _localAudioTrack = [_factory audioTrackWithTrackId:@"ARDAMSa0"];
  //  dispatch_async(dispatch_get_main_queue(),^{
    [_localMediaStream addAudioTrack:_localAudioTrack];
    //});
    NSLog(@"created local media stream  for remote party");
    
   // return localStream;
}

- (RTCMediaConstraints *)defaultMediaStreamConstraints {
    NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"IceRestart", nil];
    RTCMediaConstraints* constraints =
    [[RTCMediaConstraints alloc]
     initWithMandatoryConstraints:nil
     optionalConstraints:mandatoryConstraints];
    return constraints;
}

- (RTCVideoTrack *)createLocalVideoTrack
{
    //murli finidng camera resource
    NSLog(@"Case1  \n");
    if(_isIncomingCall  && _peerConnection && self.localMediaStream  )
    {
        NSLog(@"Case2  \n");
        [_peerConnection removeStream:_localMediaStream];
        self.localMediaStream = nil;
        [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
        [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
        _localAudioTrack = nil;
    }
    else if (_peerConnection && self.localMediaStream) {
        NSLog(@"Case3  \n");
        [_peerConnection removeStream:self.localMediaStream];
        self.localMediaStream=nil;
        self.localVideoTrack=nil;
        self.localAudioTrack=nil;
    }
     _localMediaStream = [_factory mediaStreamWithStreamId:@"ARDAMS"];

    
    __block RTCVideoTrack* localVideoTrack = nil;
    // The iOS simulator doesn't provide any sort of camera capture
    // support or emulation (http://goo.gl/rHAnC1) so don't bother
    // trying to open a local stream.
    // TODO(tkchin): local video capture for OSX. See
    // https://code.google.com/p/webrtc/issues/detail?id=3417.
#if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE
    if (!_isAudioOnly) {
        
        NSLog(@"Case4  \n");
        RTCMediaConstraints *mediaConstraints = [self defaultMediaStreamConstraints];
        
       RTCAVFoundationVideoSource *videoSource = [_factory avFoundationVideoSourceWithConstraints:mediaConstraints];
        // pintu for framerates 25-11-16
     //   RTCAVFoundationVideoSource *videoSource = [_factory avFoundationVideoSourceWithConstraints:mediaConstraint];
        // Create a video track
      //  if (device) {

            
         //   RTCAVFoundationVideoSource *videoSource = [RTCAVFoundationVideoSource init];
          //  RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:[device localizedName]];
        //    videoSource.useBackCamera = NO;
            //harsh 18 oct 2016
        
            localVideoTrack = [_factory videoTrackWithSource:videoSource trackId:@"ARDAMSv0"];
            

        
           // [localStream addVideoTrack:videoTrack];
        //}

    }
    
    
#endif
     NSLog(@"Case5 localVideoTrack kind %@ \n",localVideoTrack.kind);
    return localVideoTrack;
        
}


#if 1
- (void)mute
{
    if (_peerConnection.localStreams) {
        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] count]; j++) {
                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] objectAtIndex:j];
                track.isEnabled = NO;
                
            }
        }
    }
}

- (void)unmute
{
    if (_peerConnection.localStreams) {
        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] count]; j++) {
                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] objectAtIndex:j];
                track.isEnabled = YES;
                
            }
        }
    }
}

- (void)muteVideo
{
    if (_peerConnection.localStreams) {
        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] count]; j++) {
                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] objectAtIndex:j];
                track.isEnabled = NO;
                
            }
        }
    }
}

- (void)unmuteVideo
{
    if (_peerConnection.localStreams) {
        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] count]; j++) {
                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] objectAtIndex:j];
                track.isEnabled = YES;
            }
        }
    }
}

#endif

-(void)count
{
    RTCMediaStream* stream;
    NSLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
    unsigned long i=stream.videoTracks.count;
    unsigned long j=stream.audioTracks.count;
    printf("number of video and audio tracks:%lu %lu",i,j);

    }

#pragma mark - RTCPeerConnectionDelegate
// Callbacks for this delegate occur on non-main thread and need to be
// dispatched back to main queue as needed.

- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeSignalingState:(RTCSignalingState)stateChanged
{
    RTCLog(@"Signaling state changed: %ld", (long)stateChanged);
}

//pintu
- (void)peerConnection:(RTCPeerConnection *)peerConnection didAddStream:(RTCMediaStream *)stream
{
    
    
    NSLog(@"peerConnection:addedStream:   Dude...you succeeded...");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //RTCLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
        NSLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
        if (stream.videoTracks.count) {
            RTCVideoTrack *videoTrack = stream.videoTracks[0];
            
            if(_isIncomingCall)
            {
                if([DataSingleton sharedInstance].incomingRemoteVideoTrack)
                [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil; // sanju
                [DataSingleton sharedInstance].incomingRemoteVideoTrack = videoTrack;
                NSLog(@" murli remot video having \n");
            }
            [_delegate appClient:self didReceiveRemoteVideoTrack:videoTrack];
             self.state = kARDAppClientAudioSessionCreated;
                    }
            });
    self.state = kARDAppClientStateConnected;
 //   self.state = kARDAppClientAudioSessionCreated;


}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didRemoveStream:(RTCMediaStream *)stream
{
    
//    if (_peerConnection && self.localMediaStream) {
//        [_peerConnection removeStream:self.localMediaStream];
//        self.localMediaStream=nil;
//        self.localVideoTrack=nil;
//        self.localAudioTrack=nil;
//    }
//    if(_isIncomingCall  && _peerConnection && self.localMediaStream  )
//    {
//        [_peerConnection removeStream:_localMediaStream];
//        self.localMediaStream = nil;
//        [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
//        [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
//        _localAudioTrack = nil;
//    }
//    
    RTCLog(@"Stream was removed.");
}

- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection
{
    RTCLog(@"WARNING: Renegotiation needed but unimplemented.");
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceConnectionState:(RTCIceConnectionState)newState
{
#if Debug
    RTCLog(@"ICE state changed: %ld",(long) newState);
#endif
    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate appClient:self didChangeConnectionState:newState];
    });
}

    
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceGatheringState:(RTCIceGatheringState)newState
{
    //RTCLog(@"ICE gathering state changed: %d", newState);
     dispatch_async(dispatch_get_main_queue(), ^{
    
         [test setDateFormat:@"HH:mm:ss"];
         NSString *str = [test stringFromDate:[NSDate date]];
        
 
#if Debug
         
    NSLog(@"peerConnection:iceGatheringChanged: state: %ld", (long)newState);
#endif
         [_delegate appClient:self iceGatheringChanged1:newState];
         
         
         switch(newState)
         {
             case RTCIceGatheringStateNew:
                 NSLog(@"Asis completed ice candidate from Party A %@:",str);
                 
                 if ([_iceCandidates count] > 0)
                 {
                     
                     [self candidateGatheringComplete];
#if Debug
                     NSLog(@"candidate gathering RTCIceGatheringNew New state when count >1:%ld\n",(long)RTCIceGatheringStateNew);
#endif
                 }
                 else
                     printf("it is gathering yet to reach 1---RTCIceGatheringNew");
                 
                 
                 printf("RTCIceGaterhingNew");
                 break;
                 
               
             case RTCIceGatheringStateGathering:
                 if ([_iceCandidates count] > 0)
                 {
                     
                     [self candidateGatheringComplete];
#if Debug
                     NSLog(@"candidate gathering --RTCIceGatheringGathering when count >1:%ld\n",(long)RTCIceGatheringStateGathering);
#endif
                 }
                 else
                     printf("it is gathering yet to reach 1---RTCIceGatheringGathering");
                 break;
               
             case RTCIceGatheringStateComplete:
                 if ([_iceCandidates count] > 0)
                 {
                     
                     [self candidateGatheringComplete];
                     NSLog(@"Pringting  Asis completed ice candidate from Party A %@:",str);
#if Debug
                     NSLog(@"inside RTCIceGatheringComplete state Loop--candidate gathering when count >1:%ld\n",(long)RTCIceGatheringStateGathering);
#endif
                 }
                 else
                 {
                     NSLog(@"state complete collected--RTCIceGatheringComplete");
                 }

                 NSLog(@"it is reaching RTCIceGathering--complete");
                 break;
                 
         }


        NSLog(@"PCO onIceGatheringChange. %ld", (long)newState);
    });
    
}

-(void)setcallertype:(BOOL)flag
{
   // calleroption=flag;
}


//fired for LOCALly generated candidates, right ?
- (void)peerConnection:(RTCPeerConnection *)peerConnection didGenerateIceCandidate:(RTCIceCandidate *)candidate
{
    dispatch_async(dispatch_get_main_queue(), ^{
   
    NSLog(@"peerConnection:gotICECandidate:");
         
    
        
    [_iceCandidates addObject:candidate];
    
   
      if( ([_iceCandidates count] >= 1) ||  _candidatesGathered == NO)
        {
        
        [self performSelector:@selector(candidateGatheringComplete) withObject:nil afterDelay:0.5f];
        NSLog(@"firing without delay");
        NSLog(@"candidate gathering when count ==1");
    }

     });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didOpenDataChannel:(RTCDataChannel *)dataChannel
{
    
}



- (void)peerConnection:( RTCPeerConnection*)channel didReceivedSignalWithSessionDescription:(NSString *)sessionDescription withType:(NSString *)type {
    
    RTCSdpType typeoffer;
    typeoffer = RTCSdpTypeOffer;
    RTCSessionDescription *remoteDesc = [[RTCSessionDescription alloc] initWithType:typeoffer sdp:sessionDescription];
    //murli changes  for new webrtc
    __weak AppClient *weakSelf = self;
    [_peerConnection setRemoteDescription:remoteDesc
                        completionHandler:^(NSError *error) {
                            AppClient *strongSelf = weakSelf;
                            [strongSelf peerConnection:strongSelf.peerConnection
                     didSetSessionDescriptionWithError:error];
                        }];
   // [_peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:remoteDesc];
    NSLog(@"didReceivedSignalWithSessionDescription\n");
}




#pragma mark - RTCSessionDescriptionDelegate
// Callbacks for this delegate occur on non-main thread and need to be
// dispatched back to main queue as needed.

- (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error

{
    
    //ok...this happened at some point...
    //    Printing description of error:
    //    Error Domain=RTCSDPError Code=-1 "The operation couldn’t be completed. (RTCSDPError error -1.)"
    //    UserInfo=0x156bd7f0 {error=CreateAnswer can't be called before SetRemoteDescription.}
    
    
    
    NSLog(@"offer created callback; probably gets called for answer as well (it's a 'session description' itself, right?)");
    
    //first worry; hold on to the candidateless sdp
   // self.sdp = sdp.description;    //need to change the location
    
    
    //SDP example (before candindates)
    //    v=0
    //    o=- 7762952367168610188 2 IN IP4 127.0.0.1
    //    s=-
    //    t=0 0
    //    a=group:BUNDLE audio video
    //    a=msid-semantic: WMS ARDAMS
    //    m=audio 9 UDP/TLS/RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=ice-ufrag:5VvBHSTj7vSEjva1
    //    a=ice-pwd:7EoK4LSX3CMrOE+HKcR0QoQ6
    //    a=fingerprint:sha-256 D7:2B:3F:34:E9:87:27:4C:FA:32:C5:C4:7A:E6:25:CA:40:E3:BD:71:C3:28:B3:AB:3B:D2:66:BA:A4:F4:A9:4B
    //    a=setup:actpass
    //    a=mid:audio
    //    a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=sendrecv
    //    a=rtcp-mux
    //    a=rtpmap:111 opus/48000/2
    //    a=fmtp:111 minptime=10; useinbandfec=1
    //    a=rtpmap:103 ISAC/16000
    //    a=rtpmap:9 G722/8000
    //    a=rtpmap:102 ILBC/8000
    //    a=rtpmap:0 PCMU/8000
    //    a=rtpmap:8 PCMA/8000
    //    a=rtpmap:106 CN/32000
    //    a=rtpmap:105 CN/16000
    //    a=rtpmap:13 CN/8000
    //    a=rtpmap:127 red/8000
    //    a=rtpmap:126 telephone-event/8000
    //    a=maxptime:60
    //    a=ssrc:3096208943 cname:a0h5V831UvQWD8i9
    //    a=ssrc:3096208943 msid:ARDAMS ARDAMSa0
    //    a=ssrc:3096208943 mslabel:ARDAMS
    //    a=ssrc:3096208943 label:ARDAMSa0
    //    m=video 9 UDP/TLS/RTP/SAVPF 100 101 116 117 96
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=ice-ufrag:5VvBHSTj7vSEjva1
    //    a=ice-pwd:7EoK4LSX3CMrOE+HKcR0QoQ6
    //    a=fingerprint:sha-256 D7:2B:3F:34:E9:87:27:4C:FA:32:C5:C4:7A:E6:25:CA:40:E3:BD:71:C3:28:B3:AB:3B:D2:66:BA:A4:F4:A9:4B
    //    a=setup:actpass
    //    a=mid:video
    //    a=extmap:2 urn:ietf:params:rtp-hdrext:toffset
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=extmap:4 urn:3gpp:video-orientation
    //    a=recvonly
    //    a=rtcp-mux
    //    a=rtpmap:100 VP8/90000
    //    a=rtcp-fb:100 ccm fir
    //    a=rtcp-fb:100 nack
    //    a=rtcp-fb:100 nack pli
    //    a=rtcp-fb:100 goog-remb
    //    a=rtcp-fb:100 transport-cc
    //    a=rtpmap:101 VP9/90000
    //    a=rtcp-fb:101 ccm fir
    //    a=rtcp-fb:101 nack
    //    a=rtcp-fb:101 nack pli
    //    a=rtcp-fb:101 goog-remb
    //    a=rtcp-fb:101 transport-cc
    //    a=rtpmap:116 red/90000
    //    a=rtpmap:117 ulpfec/90000
    //    a=rtpmap:96 rtx/90000
    //    a=fmtp:96 apt=100
    
    
    
    
    
    
        if (error)
        {
            RTCLogError(@"Failed to create session description. Error: %@", error);
            [self disconnect];
            NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"Failed to create session description.", };
            NSError *sdpError = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                                           code:kARDAppClientErrorCreateSDP
                                                       userInfo:userInfo];
            [_delegate appClient:self didError:sdpError];
            return;
        }
        
        //      2015-12-11 11:07:24.441 webRTC_testbed[1235:996869] Failed to set session description. Error: Error Domain=RTCSDPError Code=-1 "The operation couldn’t be completed. (RTCSDPError error -1.)" UserInfo=0x16624d50 {error=Failed to set remote answer sdp: Called in wrong state: STATE_INIT}
        
        
        
        
      //  char const *l_sdp =
      //  "v=0\r\n\
        o=- 1449596610458467 1 IN IP4 192.168.3.4\r\n\
        s=X-Lite release 4.9.1 stamp 78725\r\n\
        c=IN IP4 192.168.11.41\r\n\
        t=0 0\r\n\
        m=audio 58710 RTP/AVP 8 0 101\r\n\
        a=rtpmap:101 telephone-event/8000\r\n\
        a=fmtp:101 0-15\r\n\
        a=sendrecv\r\n\
        ";
        //     NSString *hardCodedSDPString = [NSString stringWithCString:l_sdp encoding:NSUTF8StringEncoding];
        //     RTCSessionDescription *hardCodedSDP = [[RTCSessionDescription alloc] initWithType:@"offer" sdp:hardCodedSDPString];
        
      
        
    RTCSessionDescription *sdpPreferringH264;
#if 1
    
    if(self.isInitiator)
    {
        //its offer and answer for iphone to iphone
        sdpPreferringH264 = [SDPUtils descriptionForDescription:sdp preferredVideoCodec:@"H264"];
        self.sdp = sdpPreferringH264.description;
    }
    else
    {
        
        //murli Prefer H264 if available.
     if([self  isH264Codec:sdp.description])
     {
        sdpPreferringH264 = [SDPUtils descriptionForDescription:sdp preferredVideoCodec:@"H264"];
        self.sdp = sdpPreferringH264.description;
     }
    else
    {
        self.sdp = sdp.description;
        sdpPreferringH264 = sdp;
    }
    }
#endif
    
    //murli did the changes for new webrtc
    __weak AppClient *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(),^{
        
       
        [_peerConnection setLocalDescription:sdpPreferringH264
                           completionHandler:^(NSError *error) {
                               AppClient *strongSelf = weakSelf;
                               [strongSelf peerConnection:strongSelf.peerConnection
                        didSetSessionDescriptionWithError:error];
                           }];
     
    });
        
        //right after this, the ICE thingy actually starts or...resumes...or...whatever
        //the important thing is to expect gotICECandidate() to start fireing; candidades accumulate in _iceCandidates
        //then, iceGatheringChanged happens. When state is RTCIceGatheringComplete...
        //...I'm embedding all the candidates into the SDP and shoot it across the network via Sofia
        
        NSLog(@"setting local description");
    
        
        
        
        
        
        //this sends it through Google's signaling; I want mine (Sofia)
        //    ARDSessionDescriptionMessage *message = [[ARDSessionDescriptionMessage alloc] initWithDescription:sdpPreferringH264];
        //    [self sendSignalingMessage:message];

}


//so: this gests called for both offers&answers  //murli 9-9-16 error is coming here need to fix it for PSTN
- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error
{
    NSLog(@"did set description callback; question: is it for local or remote?");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error)
        {
            
            RTCLogError(@"Failed to set session description. Error: %@", error);
            
            NSLog(@"Failed to set session description. Error: %@", error);
            
           [self disconnect]; // pintu commented 13-9-16
            NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"Failed to set session description.", };
            NSError *sdpError =
            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                       code:kARDAppClientErrorSetSDP
                                   userInfo:userInfo];
            [_delegate appClient:self didError:sdpError];
            return;
        }
        
        // If we're answering and we've just set the remote offer we need to create
        // an answer and set the local description.
        
       /* if(_peerConnection.localDescription)
        {
            RTCSessionDescription *se=_peerConnection.localDescription;
            NSLog(@"inside didSetSessionDescriptionWithError printing values:%@\n",se.description);
            
        }*/
        
     /*   if(_peerConnection.signalingState == RTCSignalingHaveLocalOffer )
        {
            NSLog(@"it is RTCSignalingHaveLocalOffer");
        }
        else if(_peerConnection.signalingState == RTCSignalingHaveRemotePrAnswer)
        {
            NSLog(@" it is having RTCSignalingHaveRemotePrAnswer");
        }
        else if(_peerConnection.signalingState ==RTCSignalingHaveLocalPrAnswer && !peerConnection.localDescription)
        {
            RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
            
            [_peerConnection createAnswerWithDelegate:self constraints:constraints];
            NSLog(@"it is RTCSignalingHaveLocalPrAnswer");
            
        }
        
       else if (peerConnection.signalingState == RTCSignalingHaveRemoteOffer && !_peerConnection.localDescription) {
            // If we have a remote offer we should add it to the peer connection
            RTCMediaConstraints *constraints = [self defaultAnswerConstraints];

            [_peerConnection createAnswerWithDelegate:self constraints:constraints];
          NSLog(@"it is RTCSignalingHaveRemoteOffer");
        }
        else
            NSLog(@"it is showing RTCSignalingState:%d\n",_peerConnection.signalingState);*/
        if (!_isInitiator && !_peerConnection.localDescription)
        {
            NSLog(@"not initiator + ...well...might or might not have local description...");
            // murli changes for new webrtc
            RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
            __weak AppClient *weakSelf = self;
            [_peerConnection answerForConstraints:constraints
                                completionHandler:^(RTCSessionDescription *sdp,
                                                    NSError *error) {
                                    AppClient *strongSelf = weakSelf;
                                    [strongSelf peerConnection:strongSelf.peerConnection
                                   didCreateSessionDescription:sdp
                                                         error:error];
                                }];
          //  [_peerConnection createAnswerWithDelegate:self constraints:constraints];
            
            NSLog(@"creating answer, anyway...");
        }

//        else
//        {
//            //pintu 24-12-16
//         NSLog(@"sdp on success drain");
//           
//          [self drainRemoteCandidates];
//        }
      // }
        
  //  if (self.peerConnection.remoteDescription) {
          //  NSLog(@"remote description ");
        //   [self drainRemoteCandidates];
    // }
    });
}

- (void)drainRemoteCandidates {
    for (RTCIceCandidate* candidate in self.queuedRemoteCandidates) {
        [self.peerConnection addIceCandidate:candidate];
    }
    self.queuedRemoteCandidates = nil;
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error stream:(BOOL)val
{
    NSLog(@"did set description callback; question: is it for local or remote?");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error)
        {
            
            RTCLogError(@"Failed to set session description. Error: %@", error);
            
            NSLog(@"Failed to set session description. Error: %@", error);
            
            [self disconnect];
            NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"Failed to set session description.", };
            NSError *sdpError =
            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                       code:kARDAppClientErrorSetSDP
                                   userInfo:userInfo];
            [_delegate appClient:self didError:sdpError];
            return;
        }
        
        // If we're answering and we've just set the remote offer we need to create
        //   // an answer and set the local description.
        if (!_isInitiator && !_peerConnection.localDescription)
            
        {
            NSLog(@"not initiator + ...well...might or might not have local description...entering into");
            NSLog(@"audio mode and video mode flags");
            // murli changes for new webrtc
            RTCMediaConstraints *constraints = [self defaultAnswerConstraints:val];
            
          __weak AppClient *weakSelf = self;
            [_peerConnection answerForConstraints:constraints
                                completionHandler:^(RTCSessionDescription *sdp,
                                                    NSError *error) {
                                    AppClient *strongSelf = weakSelf;
                                    [strongSelf peerConnection:strongSelf.peerConnection
                                   didCreateSessionDescription:sdp
                                                         error:error];
                                }];
            
            NSLog(@"creating answer, anyway...");
        }
        
    });
}

- (void)candidateGatheringComplete
{
    if (!_candidatesGathered)
    {
        
        _candidatesGathered = YES;
        [DataSingleton sharedInstance].outgoingPayload= [self outgoingUpdateSdpWithCandidates:_iceCandidates];
        
       [DataSingleton sharedInstance].outgoingPayload=[self fixCandMlinesWithProperIPandPort:[DataSingleton sharedInstance].outgoingPayload];
       // [DataSingleton sharedInstance].outgoingPayload = [[DataSingleton sharedInstance].outgoingPayload stringByReplacingOccurrencesOfString:@"IP4" withString:@"IP6"];  // sanjay
        [DataSingleton sharedInstance].outgoingPayload=[self fix489:[DataSingleton sharedInstance].outgoingPayload];
        //[DataSingleton sharedInstance].outgoingPayload=[self killvidaudparam:[DataSingleton sharedInstance].outgoingPayload];
       // [DataSingleton sharedInstance].outgoingPayload=[self fix490:[DataSingleton sharedInstance].outgoingPayload];
        [DataSingleton sharedInstance].outgoingPayload = [[DataSingleton sharedInstance].outgoingPayload stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
        
        [DataSingleton sharedInstance].outgoingPayload = [[DataSingleton sharedInstance].outgoingPayload stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];

        
        NSString *str = [test stringFromDate:[NSDate date]];
        NSLog(@"ice candidate from Party A completed %@:",str);
       self.state = kARDAppClientStaceIceGatheringCompleted;
      //   self.state = kARDAppClientAudioSessionCreated;
        NSLog(@"inside candidategathering-complete");
    }
    
}



- (RTCMediaConstraints *)defaultOfferConstraints
{
    printf("entering inside of default offer constraints");
    
    NSString *video = _isAudioOnly ? @"false" : @"true";
    //murli did changes for new webrtc
//    NSArray *mandatoryConstraints = @[
//                                      [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"],
//                                      [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:video],
//                                      [[RTCPair alloc] initWithKey:@"IceRestart" value:@"true"]
//                                      ];
    NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",video,@"OfferToReceiveVideo",@"true",@"IceRestart", nil];
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    printf("default offer constraints as video false");
    return constraints;
}

- (RTCMediaConstraints *)defaultOfferConstraints:(BOOL)changing
{
    NSDictionary *mandatoryConstraints;
  // murli did the changes for new webrtc
  
    if(!changing)
    {
        mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"false",@"OfferToReceiveVideo",@"true",@"IceRestart", nil];
        }
    else
        mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo", nil];
    
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}

- (RTCMediaConstraints *)defaultAnswerConstraints
{
    printf("setting here offer constraints with video as null");
    return [self defaultOfferConstraints];
}

- (RTCMediaConstraints *)defaultAnswerConstraints:(BOOL)value
{
    return [self defaultOfferConstraints:value];  //changed on 12:08 on thursday by saravanan Murugesan
}



#pragma mark - The  (ICE candidates + SDP)  love affair
// from candidateless sdp stored at self.sdp and candidates stored at array, we construct a full sdp
- (NSString*)outgoingUpdateSdpWithCandidates:(NSArray *)array
{
    // split audio & video candidates in 2 groups of strings
    NSMutableString * audioCandidates = [[NSMutableString alloc] init];
    NSMutableString * videoCandidates = [[NSMutableString alloc] init];
    for (int i = 0; i < _iceCandidates.count; i++)
    {
        RTCIceCandidate *iceCandidate = (RTCIceCandidate*)[_iceCandidates objectAtIndex:i];
        if ([iceCandidate.sdpMid isEqualToString:@"audio"]) {
            // don't forget to prepend an 'a=' to make this an attribute line and to append '\r\n'
            [audioCandidates appendFormat:@"a=%@\r\n",iceCandidate.sdp];
        }
        if ([iceCandidate.sdpMid isEqualToString:@"video"]) {
            // don't forget to prepend an 'a=' to make this an attribute line and to append '\r\n'
            [videoCandidates appendFormat:@"a=%@\r\n",iceCandidate.sdp];
        }
    }
    
    // insert inside the candidateless SDP the candidates per media type
    NSMutableString *searchedString = [self.sdp mutableCopy];
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"a=rtcp:.*?\\r\\n";
    NSError  *error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionDotMatchesLineSeparators
                                                                             error:&error];
    if (error != nil) {
        NSLog(@"outgoingUpdateSdpWithCandidates: regex error");
        return @"";
    }
    
    NSTextCheckingResult* match = [regex firstMatchInString:searchedString options:0 range:searchedRange];
    int matchIndex = 0;
    if (matchIndex == 0) {
        [regex replaceMatchesInString:searchedString options:0 range:[match range] withTemplate:[NSString stringWithFormat:@"%@%@",
                                                                                                 @"$0",audioCandidates]];
    }
    
    // search again since the searchedString has been altered
    NSArray* matches = [regex matchesInString:searchedString options:0 range:searchedRange];
    if ([matches count] == 2) {
        // count of 2 means we also have video. If we don't we shouldn't do anything
        NSTextCheckingResult* match = [matches objectAtIndex:1];
        [regex replaceMatchesInString:searchedString options:0 range:[match range] withTemplate:[NSString stringWithFormat:@"%@%@",
                                                                                                 @"$0", videoCandidates]];
    }
    
    // important: the complete message also has the sofia handle (so that sofia knows which active session to associate this with)
    NSString * completeMessage = [NSString stringWithFormat:@"%@", searchedString];
    
    return completeMessage;
    
    
    
    
    //something like this:
    //note the a=candidate: lines
    
    //    v=0
    //    o=- 2198459656716439323 2 IN IP4 127.0.0.1
    //    s=-
    //    t=0 0
    //    a=group:BUNDLE audio video
    //    a=msid-semantic: WMS ARDAMS
    //    m=audio 9 UDP/TLS/RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=candidate:4192287035 1 udp 2122260223 192.168.3.4 56577 typ host generation 0
    //    a=candidate:4192287035 2 udp 2122260222 192.168.3.4 62940 typ host generation 0
    //    a=candidate:3076703691 1 tcp 1518280447 192.168.3.4 50269 typ host tcptype passive generation 0
    //    a=candidate:3076703691 2 tcp 1518280446 192.168.3.4 50270 typ host tcptype passive generation 0
    //    a=ice-ufrag:4NW63yhwV6tpY1Tz
    //    a=ice-pwd:cNX2yQTBhPBZTBAI5gHaWh/2
    //    a=fingerprint:sha-256 BF:E8:53:72:30:E2:C9:B0:83:EB:F4:1B:04:6C:EF:73:14:38:83:18:09:69:0E:4D:67:F3:D8:88:27:26:F4:5C
    //    a=setup:actpass
    //    a=mid:audio
    //    a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=sendrecv
    //    a=rtcp-mux
    //    a=rtpmap:111 opus/48000/2
    //    a=fmtp:111 minptime=10; useinbandfec=1
    //    a=rtpmap:103 ISAC/16000
    //    a=rtpmap:9 G722/8000
    //    a=rtpmap:102 ILBC/8000
    //    a=rtpmap:0 PCMU/8000
    //    a=rtpmap:8 PCMA/8000
    //    a=rtpmap:106 CN/32000
    //    a=rtpmap:105 CN/16000
    //    a=rtpmap:13 CN/8000
    //    a=rtpmap:127 red/8000
    //    a=rtpmap:126 telephone-event/8000
    //    a=maxptime:60
    //    a=ssrc:313247115 cname:mH9QZ3fiEOprI/Zw
    //    a=ssrc:313247115 msid:ARDAMS ARDAMSa0
    //    a=ssrc:313247115 mslabel:ARDAMS
    //    a=ssrc:313247115 label:ARDAMSa0
    //    m=video 9 UDP/TLS/RTP/SAVPF 100 101 116 117 96
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=candidate:4192287035 1 udp 2122260223 192.168.3.4 58573 typ host generation 0
    //    a=candidate:4192287035 2 udp 2122260222 192.168.3.4 62976 typ host generation 0
    //    a=candidate:3076703691 1 tcp 1518280447 192.168.3.4 50271 typ host tcptype passive generation 0
    //    a=candidate:3076703691 2 tcp 1518280446 192.168.3.4 50272 typ host tcptype passive generation 0
    //    a=ice-ufrag:4NW63yhwV6tpY1Tz
    //    a=ice-pwd:cNX2yQTBhPBZTBAI5gHaWh/2
    //    a=fingerprint:sha-256 BF:E8:53:72:30:E2:C9:B0:83:EB:F4:1B:04:6C:EF:73:14:38:83:18:09:69:0E:4D:67:F3:D8:88:27:26:F4:5C
    //    a=setup:actpass
    //    a=mid:video
    //    a=extmap:2 urn:ietf:params:rtp-hdrext:toffset
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=extmap:4 urn:3gpp:video-orientation
    //    a=recvonly
    //    a=rtcp-mux
    //    a=rtpmap:100 VP8/90000
    //    a=rtcp-fb:100 ccm fir
    //    a=rtcp-fb:100 nack
    //    a=rtcp-fb:100 nack pli
    //    a=rtcp-fb:100 goog-remb
    //    a=rtcp-fb:100 transport-cc
    //    a=rtpmap:101 VP9/90000
    //    a=rtcp-fb:101 ccm fir
    //    a=rtcp-fb:101 nack
    //    a=rtcp-fb:101 nack pli
    //    a=rtcp-fb:101 goog-remb
    //    a=rtcp-fb:101 transport-cc
    //    a=rtpmap:116 red/90000
    //    a=rtpmap:117 ulpfec/90000
    //    a=rtpmap:96 rtx/90000
    //    a=fmtp:96 apt=100
}



// remove candidate lines from the given sdp and return them as elements of an NSArray
-(NSDictionary*)incomingFilterCandidatesFromSdp:(NSMutableString*)sdp
{
    NSMutableArray * audioCandidates = [[NSMutableArray alloc] init];
    NSMutableArray * videoCandidates = [[NSMutableArray alloc] init];
    
    NSString *searchedString = sdp;
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"m=audio|m=video|a=(candidate.*)\\r\\n";
    NSError  *error = nil;
    
    NSString * collectionState = @"none";
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    NSArray* matches = [regex matchesInString:searchedString options:0 range: searchedRange];
    for (NSTextCheckingResult* match in matches) {
        //NSString* matchText = [searchedString substringWithRange:[match range]];
        NSString * stringMatch = [searchedString substringWithRange:[match range]];
        if ([stringMatch isEqualToString:@"m=audio"]) {
            // enter audio collection state
            collectionState = @"audio";
            audiocount++;
            printf("audio count:%d\n",audiocount);
            continue;
        }
        if ([stringMatch isEqualToString:@"m=video"]) {
            // enter audio video collection state
           // collectionState = @"audio";
            collectionState=@"audio";
            printf("videocount:%d and audiocount:%d\n",videocount,audiocount);
            videocount++;
            audiocount++;
            continue;
        }
        
        if ([collectionState isEqualToString:@"audio"]) {
            [audioCandidates addObject:[searchedString substringWithRange:[match rangeAtIndex:1]]];
        }
        if ([collectionState isEqualToString:@"video"]) {
            [videoCandidates addObject:[searchedString substringWithRange:[match rangeAtIndex:1]]];
        }
    }
    
    NSString *removePattern = @"a=(candidate.*)\\r\\n";
NSRegularExpression* removeRegex = [NSRegularExpression regularExpressionWithPattern:removePattern options:0 error:&error];
    // remove the candidates (we want a candidateless SDP)
    [removeRegex replaceMatchesInString:sdp options:0 range:NSMakeRange(0, [sdp length]) withTemplate:@""];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:audioCandidates, @"audio",
            videoCandidates, @"video", nil];
}


//restore in the future
- (NSString *)fix488:(NSString *)input
{
    NSString *processor;
    
    processor = [input stringByReplacingOccurrencesOfString:@"UDP/TLS/" withString:@"RTP/SAVPF"];
    
    NSLog(@"printing value of fix488 processer:%@\n",processor);
    
    return processor;
}
-(NSString*)fix489:(NSString*)input
{
    NSString *processor;
   
    [input stringByReplacingOccurrencesOfString:@"sendonly" withString:@"recvonly"];
    processor = [input stringByReplacingOccurrencesOfString:@"recvonly" withString:@"sendrecv"];

    printf("fix 489");
    return processor;
}

-(NSString*)fix490:(NSString*)input
{
    NSString *processor;
    
    processor = [input stringByReplacingOccurrencesOfString:@"typ host" withString:@"typ relay"];
   
    printf("fix 489");
    return processor;
}

- (NSString *)killVideoLines:(NSString *)input
{
    NSString *processor;
    
    NSRange range = [input rangeOfString:@"m=video"];
    
    range.length = [input length] - range.location;
    
    processor = [input stringByReplacingCharactersInRange:range withString:@""];
    
    return processor;
}

char * deblank(char *str)
{
    char *out = str, *put = str;
    
    for(; *str != '\0'; ++str)
    {
        if(*str != ' ')
            *put++ = *str;
    }
    *put = '\0';
    
    return out;
}

+(NSString*)killvidaudparam:(NSString*)input
{
    NSString *processor;
    
  
 processor = [input stringByReplacingOccurrencesOfString:@"m=video 9 UDP/TLS/RTP/SAVPF 100 116 117 96" withString:@"m=video 9 UDP/TLS/RTP/SAVPF 100"];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"m=audio 9 UDP/TLS/RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126" withString:@"m=audio 9 UDP/TLS/RTP/SAVPF 111"];
    
    
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:103 ISAC/16000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:9 G722/8000" withString:@" "];
   
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:102 ILBC/8000"  withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:0 PCMU/8000" withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:8 PCMA/8000" withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:106 CN/32000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:105 CN/16000" withString:@" "];
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:13 CN/8000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:127 red/8000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:126 telephone-event/8000" withString:@" "];
    

    
    
  

  
    
    NSLog(@"printing sdp after modification:%@\n",processor);
    
    return processor;


                    //          return processor;
                              


    
}

+ (NSDictionary *)getIPAddresses
{
   
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    //char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
    struct ifaddrs *interfaces;
    if (!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                      //  memcpy((__bridge void *)(addrBuf1),addrBuf,sizeof(addrBuf));
                    //   addrBuf1=[[[NSMutableString alloc] appendString:addrBuf1]];
              
                       
                        
              

                        type = IP_ADDR_IPv4;
                       
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                       //  memcpy((__bridge void *)(addrBuf2),addrBuf,sizeof(addrBuf));
                     
              

                       // addrBuf2=[str mutableCopy];
                        // NSLog(@"printing address ipv6:%@\n",addrBuf2);
                        type = IP_ADDR_IPv6;
                    
                    }
                    
                }
                if(type) {
                  
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}






// primary address is the one we want to be used based on charging. For example if both wifi & cellular network
// are available we want to use wifi
+ (NSString *)getPrimaryIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_interfaces = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_interfaces = interfaces;
        while(temp_interfaces != NULL) {
            //murli implementing for IPV6 as well 2-10-16
            if(temp_interfaces->ifa_addr->sa_family == AF_INET   )
            {
                // Check if interface is en0 which is the wifi connection on the iPhone mainly EA0/iPV4 contans the Primary /ip Address
                
                if([[NSString stringWithUTF8String:temp_interfaces->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_interfaces->ifa_addr)->sin_addr)];
                    strIPAddtessFamily =IP_ADDR_IPv4;
                    break;
                    
                }
            }
            
            
            else
            {
                NSDictionary *addresses = (NSDictionary*)[AppClient getIPAddresses];
                
                NSLog(@"addresses: %@\n",addresses);
                
                // prefer wifi over cellular. TODO: need to add ipv6 logic if we want to support it
                NSArray * preference;
                // preference= @[WIFI@"/"IP_ADDR_IPv4,WIFI@"/"IP_ADDR_IPv6, ETH_OVER_THUNDERBOLT@"/"IP_ADDR_IPv4, ETH_OVER_THUNDERBOLT@"/"IP_ADDR_IPv6,IOS_CELLULAR@"/"IP_ADDR_IPv4];
                
                preference= @[WIFI@"/"IP_ADDR_IPv4,WIFI@"/"IP_ADDR_IPv6,IOS_CELLULAR@"/"IP_ADDR_IPv4];
                NSLog(@"IP Preference: %@",preference);
                NSString * key ;
                for (key in preference) {
                    if ([addresses objectForKey:key]) {
                        NSLog(@"address: %@",addresses);
                        strIPAddtessFamily =IP_ADDR_IPv6;
                        break;
                        //return [addresses objectForKey:key];
                    }
                }
                if(addresses)
                    return [addresses objectForKey:key];
                else
                return @"";
                
            }
            
            
            temp_interfaces = temp_interfaces->ifa_next;
        }
    }
    // Free memory
    NSLog(@"geting Vectone Sim ip  addresses:%@\n",address);
    freeifaddrs(interfaces);
    return address;
   
}



- (NSString *)fixCandMlinesWithProperIPandPort:(NSString *)input
{
    NSString *processor;
    
    NSRange startRange;
    startRange.location = 0;
    
    NSRange endRange = [input rangeOfString:@"a=candidate"];
    startRange.length = endRange.location;
    processor = [input stringByReplacingCharactersInRange:startRange withString:@""];
    
    NSRange spaceRange;
    
    for (int i = 0; i < 4; i++)
    {
        spaceRange = [processor rangeOfString:@" "];
        startRange.location = 0;
        startRange.length = spaceRange.location + 1; // +1 ---> delete the space itself as well
        processor = [processor stringByReplacingCharactersInRange:startRange withString:@""];
    }
    
    spaceRange = [processor rangeOfString:@" "];
    NSString *IPaddress = [processor substringToIndex:spaceRange.location];
    
    startRange.location = 0;
    startRange.length = spaceRange.location + 1; // +1 ---> delete the space itself as well
    processor = [processor stringByReplacingCharactersInRange:startRange withString:@""];
    
    spaceRange = [processor rangeOfString:@" "];
    NSString *PortNumber = [processor substringToIndex:spaceRange.location];
    
    
    
    
    processor = [input stringByReplacingOccurrencesOfString:@"0.0.0.0" withString:IPaddress];
    
    NSString *replacement = [NSString stringWithFormat:@"m=audio %@", PortNumber];
    processor = [processor stringByReplacingOccurrencesOfString:@"m=audio 9" withString:replacement];
   
    
    replacement = [NSString stringWithFormat:@"a=rtcp:%@", PortNumber];
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtcp:9" withString:replacement];
    
    
    return processor;
    
    //    v=0
    //    o=- 8883056594029298043 2 IN IP4 127.0.0.1
    //    s=-
    //    t=0 0
    //    a=group:BUNDLE audio video
    //    a=msid-semantic: WMS ARDAMS
    //    m=audio 58748 RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126
    //    c=IN IP4 192.168.3.4
    //    a=rtcp:58748 IN IP4 192.168.3.4
    //    a=candidate:4192287035 1 udp 2122260223 192.168.3.4 58748 typ host generation 0
    //    a=candidate:4192287035 2 udp 2122260222 192.168.3.4 60953 typ host generation 0
    //    a=candidate:3076703691 1 tcp 1518280447 192.168.3.4 52467 typ host tcptype passive generation 0
    //    a=candidate:3076703691 2 tcp 1518280446 192.168.3.4 52468 typ host tcptype passive generation 0
    //    a=ice-ufrag:6ISVj+ay8ddGC6fi
    //    a=ice-pwd:hl9tn7wZhxM7Fh8GOh1u3rt0
    //    a=fingerprint:sha-256 ED:76:DA:16:CE:C1:9A:AC:57:96:51:F1:CA:31:B4:5A:89:F8:65:56:1F:EB:36:FA:10:50:38:4D:63:27:58:13
    //    a=setup:actpass
    //    a=mid:audio
    //    a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=sendrecv
    //    a=rtcp-mux
    //    a=rtpmap:111 opus/48000/2
    //    a=fmtp:111 minptime=10; useinbandfec=1
    //    a=rtpmap:103 ISAC/16000
    //    a=rtpmap:9 G722/8000
    //    a=rtpmap:102 ILBC/8000
    //    a=rtpmap:0 PCMU/8000
    //    a=rtpmap:8 PCMA/8000
    //    a=rtpmap:106 CN/32000
    //    a=rtpmap:105 CN/16000
    //    a=rtpmap:13 CN/8000
    //    a=rtpmap:127 red/8000
    //    a=rtpmap:126 telephone-event/8000
    //    a=maxptime:60
    //    a=ssrc:93419890 cname:uu3+D2nw8Ig3EjdW
    //    a=ssrc:93419890 msid:ARDAMS ARDAMSa0
    //    a=ssrc:93419890 mslabel:ARDAMS
    //    a=ssrc:93419890 label:ARDAMSa0
}

- (void)workaroundTruncation:(NSMutableString*)sdp
{
    NSString *pattern = @"cnam$";
    NSError  *error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    [regex replaceMatchesInString:sdp options:0 range:NSMakeRange(0, [sdp length]) withTemplate:@"$0e:r5NeEmW7rYyFBr5w"];
}

#pragma mark - ARDSignalingChannelDelegate
//
//- (void)channel:(id<ARDSignalingChannel>)channel
//    didReceiveMessage:(ARDSignalingMessage *)message {
//  switch (message.type) {
//    case kARDSignalingMessageTypeOffer:
//    case kARDSignalingMessageTypeAnswer:
//      // Offers and answers must be processed before any other message, so we
//      // place them at the front of the queue.
//      _hasReceivedSdp = YES;
//      [_messageQueue insertObject:message atIndex:0];
//      break;
//    case kARDSignalingMessageTypeCandidate:
//      [_messageQueue addObject:message];
//      break;
//    case kARDSignalingMessageTypeBye:
//      // Disconnects can be processed immediately.
//      [self processSignalingMessage:message];
//      return;
//  }
//  [self drainMessageQueueIfReady];
//}
//
//- (void)channel:(id<ARDSignalingChannel>)channel
//    didChangeState:(ARDSignalingChannelState)state {
//  switch (state) {
//    case kARDSignalingChannelStateOpen:
//      break;
//    case kARDSignalingChannelStateRegistered:
//      break;
//    case kARDSignalingChannelStateClosed:
//    case kARDSignalingChannelStateError:
//      // TODO(tkchin): reconnection scenarios. Right now we just disconnect
//      // completely if the websocket connection fails.
//      [self disconnect];
//      break;
//  }
//}

#pragma mark - Collider methods

//- (void)registerWithColliderIfReady {
//  if (!self.hasJoinedRoomServerRoom) {
//    return;
//  }
//  // Open WebSocket connection.
//  if (!_channel) {
//    _channel =
//        [[ARDWebSocketChannel alloc] initWithURL:_websocketURL
//                                         restURL:_websocketRestURL
//                                        delegate:self];
//    if (_isLoopback) {
//      _loopbackChannel =
//          [[ARDLoopbackWebSocketChannel alloc] initWithURL:_websocketURL
//                                                   restURL:_websocketRestURL];
//    }
//  }
//  [_channel registerForRoomId:_roomId clientId:_clientId];
//  if (_isLoopback) {
//    [_loopbackChannel registerForRoomId:_roomId clientId:@"LOOPBACK_CLIENT_ID"];
//  }
//}

#pragma mark - Errors

//+ (NSError *)errorForJoinResultType:(ARDJoinResultType)resultType {
//  NSError *error = nil;
//  switch (resultType) {
//    case kARDJoinResultTypeSuccess:
//      break;
//    case kARDJoinResultTypeUnknown: {
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorUnknown
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Unknown error.",
//      }];
//      break;
//    }
//    case kARDJoinResultTypeFull: {
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorRoomFull
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Room is full.",
//      }];
//      break;
//    }
//  }
//  return error;
//}
//
//+ (NSError *)errorForMessageResultType:(ARDMessageResultType)resultType {
//  NSError *error = nil;
//  switch (resultType) {
//    case kARDMessageResultTypeSuccess:
//      break;
//    case kARDMessageResultTypeUnknown:
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorUnknown
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Unknown error.",
//      }];
//      break;
//    case kARDMessageResultTypeInvalidClient:
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorInvalidClient
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Invalid client.",
//      }];
//      break;
//    case kARDMessageResultTypeInvalidRoom:
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorInvalidRoom
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Invalid room.",
//      }];
//      break;
//  }
//  return error;
//}

#pragma mark - RENEGOTIATION REINVITE


-(BOOL) createReInviteOffer
{
    
    
    if (_peerConnection != NULL) {
        
        NSLog(@"murli createReInviteOffer \n");
        [DataSingleton  sharedInstance].callStatus = @"ReConnecting";
        _candidatesGathered = NO;
        _isIncomingCall = NO;
        self.isInitiator = NO;
        _isInitiator = true;
        [DataSingleton sharedInstance].isAutoReInvite = YES;
      self.state = kARDAppClientReConnecting;
        NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"IceRestart", nil];
        RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
        _peerConnection.delegate = self;
        
        // Create AV media stream and add it to the peer connection.
        
        [self createLocalMediaStream:_video];
        [_peerConnection addStream:_localMediaStream];
        
        NSLog(@"creating offer");
        __weak AppClient *weakSelf = self;
        
        [_peerConnection offerForConstraints:constraints
                           completionHandler:^(RTCSessionDescription *sdp,
                                               NSError *error) {
                               AppClient *strongSelf = weakSelf;
                               [strongSelf peerConnection:strongSelf.peerConnection
                              didCreateSessionDescription:sdp
                                                    error:error];
                           }];
        
    }
    
    return YES;
}

-(void) createReInviteAnswer:(BOOL) isVideo
{
    
    if (_peerConnection != NULL) {
        [DataSingleton  sharedInstance].callStatus = @"ReConnecting";
        NSLog(@"\n  APPClient::createReInviteAnswer Receiving   REINVITED  SDP: \n%@", [DataSingleton sharedInstance].incomingPayload);
        _isIncomingCall = YES;
        _isInitiator = false;
        _isAudioOnly = isVideo;
         self.state = kARDAppClientReConnecting;
       // [ _iceCandidates removeAllObjects];
        if(self.queuedRemoteCandidates)
        {
        [_peerConnection removeIceCandidates:self.queuedRemoteCandidates];
        [self.queuedRemoteCandidates removeAllObjects];
            self.queuedRemoteCandidates = nil;
       }
        
        self.queuedRemoteCandidates = [NSMutableArray array];
        //_iceCandidates = [NSMutableArray array];
        _candidatesGathered = NO;
        
     //  self.state = kARDAppClientReConnecting;
        
        
        
        _peerConnection.delegate = self;
        _isIncomingCall = YES;
        
        if(!isVideo)
            self.state = kARDAppClientAudioSessionCreated;
        NSLog(@"creating peerConnection");
        printf("printing video value of BOOL value:%d",isVideo);
        
       NSLog(@"added local media stream");
        NSMutableString *answerSDP = [NSMutableString stringWithString:[DataSingleton sharedInstance].incomingPayload];
        RTCSessionDescription *incomingDescription;
        self.sdp = nil;
        RTCSdpType type;
        type = RTCSdpTypeOffer;
        //this means offer
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        
        // murli changes for new webrtc
       __weak AppClient *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(),^{
            [_peerConnection setRemoteDescription:incomingDescription
                                completionHandler:^(NSError *error) {
                                    AppClient *strongSelf = weakSelf;
                                    [strongSelf peerConnection:strongSelf.peerConnection
                             didSetSessionDescriptionWithError:error];
                                }];
            
            
            
            NSLog(@"setRemoteDescription happening here");
            
            NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
            RTCIceCandidate *iceCandidate;
            
            
            for (NSString * key in candidates)
            {
                for (NSString * candidate in [candidates objectForKey:key]) {
                    // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
                    iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
                    if (self.queuedRemoteCandidates)
                    {
                        [self.queuedRemoteCandidates addObject:candidate];
                    }
                    
                    [_peerConnection addIceCandidate:iceCandidate];
                    
                }
            }
            
            
            
        });
        NSLog(@"Offer is created ");

        
        
        
    }
}

-(BOOL) reConnectWhenNetworkChanged
{
     [DataSingleton sharedInstance].isAutoReInvite = YES;
    NSLog(@"murli reConnectWhenNetworkChanged \n");
    [DataSingleton sharedInstance].isAutoReInvite = YES;
    _isInitiator = YES;
    self.isTurnComplete = YES;
    _peerConnection.delegate = self;
    
    [_peerConnection removeIceCandidates:_iceCandidates];
    
    [_iceCandidates removeAllObjects];
    _iceCandidates = [NSMutableArray array];
    return ([self createReInviteOffer]);
    
}




@end



