//
//  thirdPartyAPI.m
//  ChillitalkWebRTC
//
//  Created by umn on 19/07/17.
//  Copyright © 2017 mundio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SofiaWrapperThingy.h"
#import "thirdPartyAPI.h"
#import "DataSingleton.h"
#import "Reachability.h"
#import "SoundManager.h"
#import "ARDBitrateTracker.h"

extern SofiaWrapperThingy *temp;
AppClient *tempClient = NULL;

@interface APIWrapperCls : NSObject<SofiaWrapperThingyDelegate>
@property (nonatomic, assign) void (*regCallbackFunction)(int,bool,long);
@property (nonatomic, assign) void (*dialCallbackFunction)(id);

@end

@implementation APIWrapperCls{
    
}

@synthesize regCallbackFunction;
@synthesize dialCallbackFunction;

-(void)SofiaWrapperThingySIPEventstate:(callStatusInfo*)info{
    if(*dialCallbackFunction != nil){
        if(info.call_id != nil){
            info.iu_id = [[DataSingleton sharedInstance]getIUID:info.call_id];
            (*dialCallbackFunction)(info);
        }
    }
}

-(void)SofiaWrapperThingyRegisterStatus:(int)status isExpired:(bool)expired expiry:(long)expiry{
    [DataSingleton sharedInstance].currentStatus = status;
    if(*regCallbackFunction != nil){
        (*regCallbackFunction)(status,expired,expiry);
    }
}
@end

@interface registration()<AppClientDelegate>{
    APIWrapperCls *wrapper;
}

@property (nonatomic) CFAbsoluteTime startTime;
@property (nonatomic) CFAbsoluteTime stopTime;
@property (nonatomic) long long bytesReceived;
@property (nonatomic, copy) void (^speedTestCompletionHandler)(CGFloat megabytesPerSecond, NSError *error);
@property BOOL isUserCallEnded;

@end

@implementation registration

-(id)init{
    if(self = [super init]){
        wrapper = [[APIWrapperCls alloc]init];
        temp = [[SofiaWrapperThingy alloc] initWithDelegate:wrapper];
        if(tempClient == NULL){
            tempClient = [[AppClient alloc] initWithDelegate:self];
        }
    }
    return self;
}

+(DataSingleton *)sharedInstance{
    static DataSingleton *singleton = nil;
    @synchronized(self){
        if(!singleton){
            singleton = [[DataSingleton alloc] init];
        }
    }
    return singleton;
}

//Delegate functions for handling video tracks.
-(void)appClient:(AppClient *)client didReceiveLocalVideoTrack:(RTCVideoTrack *)localVideoTrack{
    NSLog(@"MM didReceiveLocalVideoTrack");
    [_delegate registration:self didreceiveLocalVideoTrack:localVideoTrack];
}

-(void)appClient:(AppClient *)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack{
    NSLog(@"MM didReceiveRemoteVideoTrack");
    [_delegate registration:self didreceiveRemoteVideoTrack:remoteVideoTrack];
}

-(void)didChangeConnectionState:(NSDictionary*)state{
    NSLog(@"didChangeConnectionState :%@",state);
    [_delegate didReciveConnectionStatus:state];
}

-(void)isNetRechable:(BOOL)isRechabel{
    [[DataSingleton sharedInstance] setIsNetRechable:isRechabel];
}

-(void)registrationOnServer:(NSString*)username password:(NSString*)pass mobile:(NSString*)number appCode:(NSString*)code server:(NSString*)server  incomeNumber:(NSString*)inNumber callType:(NSString*)callType iuid:(NSString*)iuid callId:(NSString*)callId registrationCallback:(void (*)(int,bool,long))callBack{
    wrapper.regCallbackFunction = callBack;
    [DataSingleton sharedInstance].appCode=code;
    [DataSingleton sharedInstance].userID = username;
    [DataSingleton sharedInstance].mpassword = pass;
    [DataSingleton sharedInstance].mMobileNumber = number;
    [DataSingleton sharedInstance].isMiddleEast=NO;
    [DataSingleton sharedInstance].tlsServer=server;
    [DataSingleton sharedInstance].dIsVectoneNumber = YES;
    [DataSingleton sharedInstance].callID = callId;
    
    @try{
        [SofiaWrapperThingy sofiaRegister:username password:pass mobile:number server:server isMiddleEast:NO tlsServerIP:server iuid:iuid callId:callId];
    }@catch(NSException *exception){
        NSLog(@"sofiaRegister throwing exception %@",exception.reason);
    }
}

-(void)reRegistration{
    [SofiaWrapperThingy sofiaReRegistion];
}

-(void)releasePreviousThread:(BOOL)isCallFromPushKit{
    [SofiaWrapperThingy deInitResources:isCallFromPushKit];
}

-(void)unRegister_on_server{
    [SofiaWrapperThingy unregister];
}

-(void)dialPSTNNumber:(NSString *)toNumber iuid:(NSString *)iu_id fromNumber:(NSString *)fromNumber  isVectoneNumber:(bool)isVec dialCallback:(void (*)(id))callBack{
    wrapper.dialCallbackFunction = callBack;
    [DataSingleton sharedInstance].mMobileNumber = fromNumber;
    if(tempClient == NULL){
        tempClient = [[AppClient alloc] initWithDelegate:self];
    }
    tempClient.isAudioOnly = YES;
    tempClient.isInitiator = YES;
    tempClient.dIsPSTN = YES;
    tempClient.dDialNumber = toNumber;
    tempClient.iu_id = iu_id;
    [DataSingleton sharedInstance].dIsVectoneNumber=isVec;
    _isUserCallEnded = NO;
    
    [self testDownloadSpeedWithTimout:10.0 completionHandler:^(CGFloat megabytesPerSecond, NSError *error){
         NSLog(@"testDownloadSpeed: %f",megabytesPerSecond);
         if(error || megabytesPerSecond<=0){
             //wrapper.dialCallbackFunction(1000);
         }else{
             if(_isUserCallEnded != YES){
                 dispatch_async(dispatch_get_main_queue(),^{
                     [tempClient configure:NO];
                     [DataSingleton sharedInstance].client =tempClient;
                 });
             }
         }
     }];
}

-(void)dialCall:(NSString *)toNumber iuid:(NSString *)iu_id dialCallback:(void (*)(id))callBack{
    wrapper.dialCallbackFunction = callBack;
    if(tempClient == NULL){
        tempClient = [[AppClient alloc] initWithDelegate:self];
    }
    tempClient.isAudioOnly = YES;
    tempClient.isInitiator = YES;
    tempClient.dIsPSTN = NO;
    tempClient.dDialNumber = toNumber;
    tempClient.iu_id = iu_id;
    _isUserCallEnded = NO;
    [DataSingleton sharedInstance].dIsVectoneNumber=YES;

    [self testDownloadSpeedWithTimout:10.0 completionHandler:^(CGFloat megabytesPerSecond, NSError *error){
         NSLog(@"testDownloadSpeed: %f",megabytesPerSecond);
         if(error || megabytesPerSecond<=0){
             //wrapper.dialCallbackFunction(1000);
         }else{
             if(_isUserCallEnded != YES){
                 dispatch_async(dispatch_get_main_queue(),^{
                     [tempClient configure:NO];
                     [DataSingleton sharedInstance].client =tempClient;
                     //[SofiaWrapperThingy sofiaCall:tempClient.dDialNumber callType:tempClient.isAudioOnly isPSTN:tempClient.dIsPSTN];
                 });
             }
         }
     }];
}

-(void)acceptCallThirdParty:(NSString*)iuid acceptCallback:(void (*)(id))callBack{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
    });
    wrapper.dialCallbackFunction = callBack;
    [DataSingleton sharedInstance].isMiddleEast=NO;
    [DataSingleton sharedInstance].isIncomingCallAccept = YES;
    [SofiaWrapperThingy sofiaAcceptCall:iuid];
}

-(void)endCall{
    NSLog(@"MM endCall");
    _isUserCallEnded = YES;
    [SofiaWrapperThingy sofiaEndCall];
}

-(void)dialVideoCall:(NSString *)toNumber iuid:(NSString *)iu_id dialCallback:(void (*)(id))callBack  Delegate:(id) mdelegate{
    wrapper.dialCallbackFunction = callBack;
    if(tempClient == NULL){
        tempClient = [[AppClient alloc] initWithDelegate:self];
    }
    tempClient.isAudioOnly = NO;
    tempClient.isInitiator = YES;
    tempClient.dIsPSTN = NO;
    tempClient.dDialNumber = toNumber;
    tempClient.iu_id = iu_id;
    _delegate=mdelegate;
    [DataSingleton sharedInstance].dIsVectoneNumber = YES;
    _isUserCallEnded = NO;

    [self testDownloadSpeedWithTimout:10.0 completionHandler:^(CGFloat megabytesPerSecond, NSError *error){
         NSLog(@"testDownloadSpeed: %f",megabytesPerSecond);
         if(error || megabytesPerSecond<=0){
             //wrapper.dialCallbackFunction(1000);
         }else{
             if(_isUserCallEnded != YES){
                 [tempClient enableSpeaker];
                 dispatch_async(dispatch_get_main_queue(),^{
                     [tempClient configure:YES];
                     [DataSingleton sharedInstance].client =tempClient;
                 });
             }
         }
     }];
}

-(void)acceptVideoCall:(id)mdelegate iuid:(NSString*)iu_id callback:(void (*)(id))callBack{
    wrapper.dialCallbackFunction = callBack;
    _delegate = mdelegate;
    [SofiaWrapperThingy sofiaAcceptCall:iu_id];
    [_delegate registration:self didreceiveLocalVideoTrack:[DataSingleton sharedInstance].client.localVideoTrack];
    [_delegate registration:self didreceiveRemoteVideoTrack:[DataSingleton sharedInstance].client.remoteVideoTrack];
}

-(void)setVOIPRejected:(BOOL)rejected{
    if(rejected){
        [DataSingleton sharedInstance].isIncomingCallDecline = YES;
        [self rejectCall];
    }
}

-(void)referDialCall:(NSString *)toNumber referCallType:(NSString *)referCallType callType:(BOOL)isVideo  dialCallback:(void (*)(id))callBack{
    wrapper.dialCallbackFunction = callBack;
    [SofiaWrapperThingy sofiaReferCall:toNumber referCallType:referCallType];
}

-(void)callForParkFlipAndUnFlip:(NSString *)toNumber callType:(NSString *)callType callType:(BOOL)isVideo  dialCallback:(void (*)(id))callBack{
    wrapper.dialCallbackFunction = callBack;
    [SofiaWrapperThingy sofiaCallParkAndFlipUnflip:toNumber callType:callType];
}

-(void)callHoldUnhold:(NSString *)iuid hold:(BOOL)holdUnholdFlag{
    [SofiaWrapperThingy sofiahold:holdUnholdFlag iuid:iuid];
}

-(void)unmute:(BOOL)isUnmute{
    [[DataSingleton sharedInstance].client muteUnmute:isUnmute];
}

-(void)holdEnableAudioTrack:(BOOL)holdStatus{
    [[DataSingleton sharedInstance].client holdEnableAudioTrack:holdStatus];
}

-(void)sendDTMF:(NSString *)toNumber{
    [[DataSingleton sharedInstance].client sendDTMF:toNumber];
    //[SofiaWrapperThingy sofiaDTMF:toNumber];
}

-(void)changeLine:(NSString *)iuid{
}

-(void)mergeLine:(NSString *)iuid{
    [SofiaWrapperThingy sofiaMergeCalls];
}

-(void)sendState:(BOOL)isActive{
    //[SofiaWrapperThingy sofiaUserStatus:isActive];
}

-(void)isSpeakerEnable:(BOOL)isEnable{
    if(isEnable == YES){
        [[DataSingleton sharedInstance].client enableSpeaker];
    }else{
        [[DataSingleton sharedInstance].client disableSpeaker];
    }
}

-(int)returnCallConnectionStatus{
    int currentCallConnectionStatus = [DataSingleton sharedInstance].currentStatus;
    return currentCallConnectionStatus;
}

-(void)conference:(NSString *)toNumber{
    [SofiaWrapperThingy sofiaConference:toNumber];
}

-(void)switchLine:(NSString *)iuid{
    [SofiaWrapperThingy sofiaLineSwitch:iuid];
}

-(void)appClient:(AppClient *)client iceGatheringChanged1:(RTCIceGatheringState)newState{
    //RTCLog(@"ICE gathering state changed: %d", newState);
    switch(newState){
        case  RTCIceGatheringStateNew:
              NSLog(@"RTCIceGatheringStateNew");
              break;
            
        case RTCIceGatheringStateGathering:
             NSLog(@"RTCIceGatheringStateGathering");
             break;
            
        case RTCIceGatheringStateComplete:
             NSLog(@"RTCIceGatheringStateComplete\n");
             if(client.isInitiator){
                 @try{
                     [SofiaWrapperThingy sofiaCall:client.dDialNumber callType:client.isAudioOnly isPSTN:client.dIsPSTN];
                 }@catch(NSException *exception){
                     NSLog(@"Exception = %@",exception.reason);
                 }
             }
             break;
    }
}

-(void)appClient:(AppClient *)client didChangeConnectionState:(RTCIceConnectionState)state{
    //Write code
}

-(void)appClient:(AppClient *)client didChangeState:(AppClientState)state{
    //Write Code
}

-(void)appClient:(AppClient *)client didError:(NSError *)error{
    //Write Code
}

-(void)rejectCall{
    [SofiaWrapperThingy sofiaRejectCall];
}

-(void)stopRBT{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SoundManager sharedManager] stopMusic:NO];
    });
}

-(void)testDownloadSpeedWithTimout:(NSTimeInterval)timeout completionHandler:(nonnull void (^)(CGFloat megabytesPerSecond, NSError * _Nullable error))completionHandler{
    NSURL *url = [NSURL URLWithString:@"https://www.google.co.in/"];
    
    self.startTime = CFAbsoluteTimeGetCurrent();
    self.stopTime = self.startTime;
    self.bytesReceived = 0;
    self.speedTestCompletionHandler = completionHandler;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.timeoutIntervalForResource = timeout;
    @try{
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:(id)self delegateQueue:nil];
        [[session dataTaskWithURL:url] resume];
    }@catch(NSException *exception){
        NSLog(@"Reason of exception = %@",exception.reason);
    }
}

-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
    self.bytesReceived += [data length];
    self.stopTime = CFAbsoluteTimeGetCurrent();
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    CFAbsoluteTime elapsed = self.stopTime - self.startTime;
    CGFloat speed = elapsed != 0 ? self.bytesReceived / (CFAbsoluteTimeGetCurrent() - self.startTime) / 1024.0 : -1;
    //treat timeout as no error (as we're testing speed, not worried about whether we got entire resource or not
    if(error == nil || ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorTimedOut)){
        self.speedTestCompletionHandler(speed, nil);
    }else{
        self.speedTestCompletionHandler(speed, error);
    }
}

@end

