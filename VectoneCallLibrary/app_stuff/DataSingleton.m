//
//  DataSingleton.m
//  
//
//  Created by Macprobook on 2/11/16.
//
//

#import "DataSingleton.h"
#import "SofiaWrapperThingy.h"
#import "Reachability.h"
#import "SoundManager.h"

@implementation DataSingleton

-(id)init{
    if(self = [super init]){
        
    }
    return self;
}

+(DataSingleton *)sharedInstance {
    static DataSingleton *singleton = nil;
    @synchronized(self){
        if(!singleton){
            singleton = [[DataSingleton alloc] init];
        }
    }
    return singleton;
}

void myExceptionHandler(NSException *exception){

}

-(void)getExpiryTime{
    
}

-(BOOL)getCurrentAppState{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if(state == UIApplicationStateBackground || state == UIApplicationStateInactive) {
        return NO; // background
    }else if (state == UIApplicationStateActive) {
        return YES; // foreground
    }else{
        return YES;
    }
}

-(void)removeClient{
    [self.client disconnect];
    self.client = nil;
    [self.client_L2 disconnect];
    self.client_L2 = nil;
}

-(NSString*)getIUID:(NSString*)call_id{
    if([_client.call_id isEqualToString:call_id])
        return _client.iu_id;
    else if([_client_L2.call_id isEqualToString:call_id])
        return _client.iu_id;
    else
        return @"";
}

-(void)addClient:(NSString*)call_id iuid:(NSString*)iu_id incomPayload:(NSString*)payload isAudio:(BOOL)isAudio{
    if(_ispersonInCall){
        if(!_client_L2){
            _client_L2 = [[AppClient alloc] init];
        }
        _client_L2.incomingPayload = payload;
        _client_L2.incomingPayload = [_client_L2.incomingPayload stringByReplacingOccurrencesOfString:@"setup:passive" withString:@"setup:actpass"];
        _client_L2.call_id = call_id;
        _client_L2.iu_id = iu_id;
        _client_L2.isInitiator = NO;
        _client_L2.isAudioOnly = isAudio;
        if(_isIncomingCallAccept){
            [_client_L2 configureInComingCall];
        }
    }else{
        if(!_client){
            _client = [[AppClient alloc] init];
        }
        _client.incomingPayload = payload;
        _client.incomingPayload = [_client.incomingPayload stringByReplacingOccurrencesOfString:@"setup:passive" withString:@"setup:actpass"];
        _client.call_id = call_id;
        _client.iu_id = iu_id;
        _client.isInitiator = NO;
        _client.isAudioOnly = isAudio;
        if(_isIncomingCallAccept){
            [_client configureInComingCall];
        }
    }
}

@end

