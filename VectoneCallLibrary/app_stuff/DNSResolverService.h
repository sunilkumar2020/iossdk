//
//  DNSResolverService.h
//  
//
//  Created by joe on 15/12/11.
//  Copyright © 2015年 joe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DNSResolverService : NSObject

- (void)getIPByHost:(NSString *_Nullable)host  completionHandler:(nonnull void (^)(NSArray* _Nonnull ips))completionHandler;

@end
