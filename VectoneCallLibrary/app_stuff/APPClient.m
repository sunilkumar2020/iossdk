//  APPClient.m
//  Created by Macprobook on 2/11/16.

/*
 *  Copyright 2014 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#import <Foundation/Foundation.h>
#import <WebRTC/RTCAudioTrack.h>
#import <WebRTC/RTCCameraVideoCapturer.h>
#import <WebRTC/RTCConfiguration.h>
#import <WebRTC/RTCDefaultVideoDecoderFactory.h>
#import <WebRTC/RTCDefaultVideoEncoderFactory.h>
#import <WebRTC/RTCFileLogger.h>
#import <WebRTC/RTCFileVideoCapturer.h>
#import <WebRTC/RTCIceServer.h>
#import <WebRTC/RTCLogging.h>
#import <WebRTC/RTCMediaConstraints.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCPeerConnectionFactory.h>
#import <WebRTC/RTCRtpSender.h>
#import <WebRTC/RTCRtpTransceiver.h>
#import <WebRTC/RTCTracing.h>
#import <WebRTC/RTCVideoSource.h>
#import <WebRTC/RTCVideoTrack.h>
#import <WebRTC/RTCDtmfSender.h>
#import "ARDSettingsModel.h"
#import "SofiaWrapperThingy.h"

#import "WebRTC/RTCLegacyStatsReport.h"
#import "SDPUtils.h"

#import "WebRTC/RTCDataChannel.h"
#import "RTCSessionDescription+JSON.h"
#import "RTCIceServer+JSON.h"
#import <Foundation/Foundation.h>
#import "DataSingleton.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#import "WebRTC/RTCAudioTrack.h"
#import "RTCIceCandidate+JSON.h"
#import "Reachability.h"

#define IOS_CELLULAR            @"pdp_ip0"
#define WIFI                    @"en0"   // iOS or OSX (simulator)
#define IOS_VPN                 @"utun0"
#define IP_ADDR_IPv4            @"ipv4"
#define IP_ADDR_IPv6            @"ipv6"

char addrBuf[100];
char addrBuf1[200];
char addressbuff[200];
static NSString *strIPAddtessFamily;
static NSString * const kARDDefaultSTUNServerUrl = @"turn:stun.l.google.com:19302";
static NSString * const kTURNServerUrl= @"turn:stun02.mundio.com:3478";
static NSString * const kSTUNServerUrl = @"stun:stun02.mundio.com:3478";
static int const kKbpsMultiplier = 1000;

//static NSString *const kARDDefaultSTUNServerUrl = @"turn:stun02.mundio.com:3478";
static NSString * const kARDMediaStreamId = @"ARDAMS";
static NSString * const kARDAudioTrackId = @"ARDAMSa0";
static NSString * const kARDVideoTrackId = @"ARDAMSv0";
static NSString * const kARDVideoTrackKind = @"video";

static NSString * const kARDTurnRequestUrl = @"https://computeengineondemand.appspot.com"@"/turn?username=iapprtc&key=4080218913";
static NSString *kARDRoomServerHostUrl =@"https://apprtc.appspot.com";

static NSString * const kARDAppClientErrorDomain = @"AppClient";
static NSInteger const kARDAppClientErrorCreateSDP = -3;
static NSInteger const kARDAppClientErrorSetSDP = -4;
int videocount;
int audiocount;

BOOL video=false;
BOOL offerConstraint=false;

@interface AppClient() {
    RTCMediaConstraints *mediaConstraint;
    NSDateFormatter *test;
    NSTimer *getStatsTimer;
}
@end

@implementation AppClient

@synthesize delegate = _delegate;
@synthesize peerConnection = _peerConnection;
@synthesize factory = _factory;
@synthesize  localVideoTrack = _localVideoTrack;
@synthesize localAudioTrack = _localAudioTrack;
@synthesize isTurnComplete = _isTurnComplete;
@synthesize isInitiator = _isInitiator;
@synthesize iceServers = _iceServers;
@synthesize iceCandidates = _iceCandidates;
@synthesize defaultPeerConnectionConstraints =_defaultPeerConnectionConstraints;
@synthesize isAudioOnly = _isAudioOnly;
@synthesize queuedRemoteCandidates;
@synthesize parameters=_parameters;
@synthesize sdp = _sdp;
@synthesize isSpeakerEnabled = _isSpeakerEnabled;
@synthesize outgoingPayload;
@synthesize incomingPayload;
@synthesize call_id =_call_id;
@synthesize iu_id =_iu_id;

-(instancetype)init{
    if(self = [super init]){
    }
    return self;
}

-(instancetype)initWithDelegate:(id<AppClientDelegate>)delegate{
    if(self = [super init]){
        _delegate = delegate;
    }
    return self;
}


#pragma mark - Creating Constrain based on audio / video call

-(void)releseSDP:(BOOL)isVideo{
    
}

-(void)setRemoteHoldUnhold{
    
}

-(RTCMediaConstraints *)mediaConstraints{
    NSDictionary *mandatoryConstraints;
    if(self.isAudioOnly){
        if([strIPAddtessFamily isEqual:IP_ADDR_IPv4]){
            mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"false",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl",nil];
        }else{
            mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"false",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl",nil];
        }
    }else{
        if([strIPAddtessFamily  isEqual:IP_ADDR_IPv4]){
            mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl",nil];
            
        }else{
            mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl", nil];
        }
    }
    NSLog(@"inside default peer connection dtls set as on and off based on app and pstn call");
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}

-(void)configure:(BOOL)isVideoCall{
    _candidatesGathered = NO;
    test  = [[NSDateFormatter alloc] init];
    [test setDateFormat:@"HH:mm:ss"];
    NSString *str = [test stringFromDate:[NSDate date]];
    NSLog(@"start ice candidate from Party A %@:",str);
    
    NSLog(@"AppClient::Configure peerconnection \n");
//    if(!_factory)
//    {
        if(isVideoCall)
        {
        RTCDefaultVideoDecoderFactory *decoderFactory = [[RTCDefaultVideoDecoderFactory alloc] init];
        RTCDefaultVideoEncoderFactory *encoderFactory = [[RTCDefaultVideoEncoderFactory alloc] init];
        ARDSettingsModel *settings =[[ARDSettingsModel alloc]init];
        encoderFactory.preferredCodec = [settings currentVideoCodecSettingFromStore];
        _factory = [[RTCPeerConnectionFactory alloc] initWithEncoderFactory:encoderFactory
                                                             decoderFactory:decoderFactory];
        }else
         _factory = [[RTCPeerConnectionFactory alloc] init];
    
        //   [RTCPeerConnectionFactory initializeSSL];  need to implement it for SSL
//    }
    
    
//    if(mandatory)
//        mandatory = nil;
    
    
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun01.mundio.com:3478"] username:@"admin" password:@"system123"];
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
    //  RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun01.mundio.com::3478?transport=tcp"] username:@"admin" password:@"system123"];
    

    RTCIceServer *turn ;

     turn  = [[RTCIceServer alloc] initWithURLStrings:@[kTURNServerUrl] username:@"admin" credential:@"system123"];
 
    
  //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURLStrings:@[@"stun:stun.l.google.com:19302"] username:@"" credential:@""];
    
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
    //RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    //
    
    //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"root" password:@"vicarage2000"];
    
    
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun.l.google.com:19302"] username:@"" password:@""];
    
    //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    
    // NSArray *iceServers1 = [[NSArray alloc] initWithObjects:sturn,turn,nil];
    NSArray *iceServers1 = [[NSArray alloc] initWithObjects:turn,nil];
    
    _iceServers=[[NSMutableArray alloc] initWithArray:iceServers1];
    
    if(isVideoCall)
    {
        _isAudioOnly = NO;
        // mediaConstraint =[self  defaultOfferConstraints:YES];
    }
    else
    {
        _isAudioOnly = YES;
        //   mediaConstraint =[self  defaultOfferConstraints:NO];
    }
    
    _iceCandidates = [NSMutableArray array];
    // __weak AppClient *weakSelf = self;
    //AppClient *strongSelf = weakSelf;
    self.isTurnComplete = YES;
    _peerConnection.delegate = self;
    [self startPeerConnection:isVideoCall];
    
}


- (void)configureInComingCall {
    
   
//    [self cofigIncomCall]; //VV_Test
//    return; //VV_Test
//
    
    
    if(!_isAudioOnly)
           {
           RTCDefaultVideoDecoderFactory *decoderFactory = [[RTCDefaultVideoDecoderFactory alloc] init];
           RTCDefaultVideoEncoderFactory *encoderFactory = [[RTCDefaultVideoEncoderFactory alloc] init];
           ARDSettingsModel *settings =[[ARDSettingsModel alloc]init];
           encoderFactory.preferredCodec = [settings currentVideoCodecSettingFromStore];
           _factory = [[RTCPeerConnectionFactory alloc] initWithEncoderFactory:encoderFactory
                                                                decoderFactory:decoderFactory];
           }else
            _factory = [[RTCPeerConnectionFactory alloc] init] ;
    
    
    //  dispatch_async(dispatch_get_main_queue(),^{
//    _factory = [[RTCPeerConnectionFactory alloc] init];
    //  [RTCPeerConnectionFactory initializeSSL];
    // });
    // [NSThread sleepForTimeInterval:0.5f];

    //  RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun01.mundio.com:3478"] username:@"admin" password:@"system123"];
    
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478"] username:@"root" password:@"vicarage2000"];
    //  RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
    //   RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478?transport=tcp"] username:@"admin" password:@"system123"];
    RTCIceServer *turn = [[RTCIceServer alloc] initWithURLStrings:@[kTURNServerUrl] username:@"admin" credential:@"system123"];
    
  //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURLStrings:@[@"stun:stun.l.google.com:19302"] username:@"" credential:@""];
    
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"turn:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    
    //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    
    //   RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"root" password:@"vicarage2000"];
    self.outgoingPayload = @"";
    _candidatesGathered = NO;
    // RTCIceServer *turn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun.l.google.com:19302"] username:@"" password:@""];
    
    // RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun02.mundio.com:3478"] username:@"admin" password:@"system123"];
    //  RTCIceServer *sturn = [[RTCIceServer alloc] initWithURI:[NSURL URLWithString:@"stun:stun01.mundio.com"] username:@"" password:@""];
    
    //  NSArray *iceServers1 = [[NSArray alloc] initWithObjects:sturn,turn,nil];
    NSArray *iceServers1 = [[NSArray alloc] initWithObjects:turn,nil];
    
    _iceServers=[[NSMutableArray alloc] initWithArray:iceServers1];
    _isInitiator = NO;
    if(!_isAudioOnly)
    {
        videocount = 5;
        _isAudioOnly = NO;
    }
    else
    {
        _isAudioOnly = YES;
        videocount = 0;
        
    }
    
    

    NSLog(@"printing iceservers:%@ and  turn:%@\n",iceServers1,turn);
    
    _iceCandidates = [NSMutableArray array];
    self.isTurnComplete = YES;
    
    
    _peerConnection.delegate = self;
    [self processIncomingInvite];
    
    
}
- (void)cofigIncomCall {
    
    
    if(self.isAudioOnly)
    {
        _factory = [[RTCPeerConnectionFactory alloc] init] ;
    }else
    {
        RTCDefaultVideoDecoderFactory *decoderFactory = [[RTCDefaultVideoDecoderFactory alloc] init];
        RTCDefaultVideoEncoderFactory *encoderFactory = [[RTCDefaultVideoEncoderFactory alloc] init];
        ARDSettingsModel *settings =[[ARDSettingsModel alloc]init];
        encoderFactory.preferredCodec = [settings currentVideoCodecSettingFromStore];
        _factory = [[RTCPeerConnectionFactory alloc] initWithEncoderFactory:encoderFactory
                                                             decoderFactory:decoderFactory];
    }


      RTCIceServer *turn ;

       turn  = [[RTCIceServer alloc] initWithURLStrings:@[kTURNServerUrl] username:@"admin" credential:@"system123"];
   
      NSArray *iceServers1 = [[NSArray alloc] initWithObjects:turn,nil];
      
      _iceServers=[[NSMutableArray alloc] initWithArray:iceServers1];
   
      _iceCandidates = [NSMutableArray array];
  
      self.isTurnComplete = YES;
      _peerConnection.delegate = self;

       RTCConfiguration *config = [[RTCConfiguration alloc] init];
        config.tcpCandidatePolicy = RTCTcpCandidatePolicyDisabled;
        config.bundlePolicy = RTCBundlePolicyBalanced;
        
        
        config.iceServers = _iceServers;
        config.iceTransportPolicy = RTCIceTransportPolicyRelay;
        config.rtcpMuxPolicy=RTCRtcpMuxPolicyRequire;
        
        
  
         config.sdpSemantics = RTCSdpSemanticsUnifiedPlan;
    
        mediaConstraint = [self mediaConstraints];

        
        _peerConnection = [_factory peerConnectionWithConfiguration:config
                                                        constraints:mediaConstraint
                                                           delegate:self];

//        [NSThread sleepForTimeInterval:0.5f];

         [self createMediaSenders];
    
            [_peerConnection offerForConstraints:mediaConstraint
                                      completionHandler:^(RTCSessionDescription *sdp,
                                                       NSError *error) {
                   
                if(!error)
                {
                      
                         RTCSessionDescription *sdpPreferringH264;
             
                         if([sdp.description length]>0)
                         {
                         NSString *mangledSdpString = sdp.description;
                         mangledSdpString = [mangledSdpString  stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
                         mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:offerv=0" withString:@""];
                         mangledSdpString = [mangledSdpString  stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
                         mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:answerv=0" withString:@""];
                         mangledSdpString = [self optimizePayload:mangledSdpString];
                         sdpPreferringH264 = [[RTCSessionDescription alloc] initWithType:sdp.type sdp:mangledSdpString];
                         self.sdp = sdpPreferringH264.description;
    
                         dispatch_async(dispatch_get_main_queue(),^{
                             
                             [_peerConnection setLocalDescription:sdpPreferringH264
                                                completionHandler:^(NSError *error) {
                                               NSLog(@"setLocalDescription ERROR: %@",error);
                                                }];
                             
                         });
                         
                    }
                }else
                {
                    NSLog(@"offerForConstraints ERROR: %@",error);
                }
                                  
               }];
  
 
   
}

- (void)answer
{
    self.incomingPayload = [self.incomingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:active"];
    NSMutableString *answerSDP = [NSMutableString stringWithString:self.incomingPayload];
    RTCSessionDescription *incomingDescription;
    RTCSdpType type;
    type = RTCSdpTypeAnswer;
    incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
//    __weak AppClient *weakSelf = self;
    [_peerConnection setRemoteDescription:incomingDescription
                               completionHandler:^(NSError *error) {
        NSLog(@"setRemoteDescription ERROR: %@",error);
                               }];
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    for (NSString * key in candidates)
    {
               for (NSString * candidate in [candidates objectForKey:key]) {
                   // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
                   iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
                   if (self.queuedRemoteCandidates)
                   {
                       [self.queuedRemoteCandidates addObject:candidate];
                   }
                   
                   [_peerConnection addIceCandidate:iceCandidate];
                   
               }
     }
}

-(void)holdEnableAudioTrack:(BOOL)holdStatus {
    if(holdStatus){
        for(RTCRtpTransceiver *transceiver in _peerConnection.transceivers){
            RTCMediaStreamTrack *receiverTrack = transceiver.receiver.track;
            RTCMediaStreamTrack *senderTrack = transceiver.sender.track;
            
            RTCRtpParameters *rtpParameters = transceiver.sender.parameters;
            //RTCRtpParameters *rtpParametersReciver = transceiver.receiver.parameters;
            
            rtpParameters.encodings[0].isActive = false;
            transceiver.sender.parameters = rtpParameters;
            transceiver.direction = RTCRtpTransceiverDirectionInactive;
            
            if(receiverTrack != NULL){
                if(receiverTrack.kind == kRTCMediaStreamTrackKindAudio){
                    [receiverTrack setIsEnabled:false];
                }
            }
            if(senderTrack != NULL){
                if(senderTrack.kind == kRTCMediaStreamTrackKindAudio){
                    [senderTrack setIsEnabled:false];
                }
            }
        }
    }else{
        for(RTCRtpTransceiver *transceiver in _peerConnection.transceivers){
            RTCMediaStreamTrack *receiverTrack = transceiver.receiver.track;
            RTCMediaStreamTrack *senderTrack = transceiver.sender.track;
            
            RTCRtpParameters *rtpParameters = transceiver.sender.parameters;
            //RTCRtpParameters *rtpParametersReciver = transceiver.receiver.parameters;
            
            rtpParameters.encodings[0].isActive = true;
            transceiver.sender.parameters = rtpParameters;
            
            if(receiverTrack != NULL){
                if(receiverTrack.kind == kRTCMediaStreamTrackKindAudio){
                    [receiverTrack setIsEnabled:true];
                }
            }
            if(senderTrack != NULL){
                if(senderTrack.kind == kRTCMediaStreamTrackKindAudio){
                    [senderTrack setIsEnabled:true];
                }
            }
            
            transceiver.direction = RTCRtpTransceiverDirectionSendRecv;
        }
    }
}

-(void)dealloc {
    //self.shouldGetStats = NO;
    
    [self disconnect];
    NSLog(@"%s", __FUNCTION__);
}

-(void)setState:(AppClientState)state {
  
}
//start murli
#if 0
// request for turn server to validate is
- (void)requestServersWithCompletionHandler:
(void (^)(NSArray *turnServers,
          NSError *error))completionHandler {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kaServerUrl]];
    // We need to set origin because TURN provider whitelists requests based on
    // origin.
    [request addValue:@"Mozilla/5.0" forHTTPHeaderField:@"user-agent"];
    [request addValue:kTURNServerUrl forHTTPHeaderField:@"origin"];
    [NSURLConnection sendAsyncRequest:request
                    completionHandler:^(NSURLResponse *response,
                                        NSData *data,
                                        NSError *error) {
                        NSArray *turnServers = [NSArray array];
                        if (error) {
                            completionHandler(turnServers, error);
                            return;
                        }
                        NSDictionary *dict = [NSDictionary dictionaryWithJSONData:data];
                        turnServers = [RTCIceServer serversFromCEODJSONDictionary:dict];
                        if (!turnServers) {
                            NSError *responseError = @"murli Bad response from turn server";
                            //                           [[NSError alloc] initWithDomain:kARDCEODTURNClientErrorDomain
                            //                                                       code:kARDCEODTURNClientErrorBadResponse
                            //                                                   userInfo:@{
                            //                                                              NSLocalizedDescriptionKey: @"Bad TURN response.",
                            //                                                              }];
                            completionHandler(turnServers, responseError);
                            return;
                        }
                        completionHandler(turnServers, nil);
                    }];
}



//end murli
- (void)requestTURNServersWithURL:(NSURL *)requestURL
                completionHandler:(void (^)(NSArray *turnServers))completionHandler {
    NSParameterAssert([requestURL absoluteString].length);
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:requestURL];
    // We need to set origin because TURN provider whitelists requests based on
    // origin.
    [request addValue:@"Mozilla/5.0" forHTTPHeaderField:@"user-agent"];
    [request addValue:self.serverHostUrl forHTTPHeaderField:@"origin"];
    [NSURLConnection sendAsyncRequest:request
                    completionHandler:^(NSURLResponse *response,
                                        NSData *data,
                                        NSError *error) {
                        NSArray *turnServers = [NSArray array];
                        if (error) {
                            NSLog(@"Unable to get TURN server.");
                            completionHandler(turnServers);
                            return;
                        }
                        NSDictionary *dict = [NSDictionary dictionaryWithJSONData:data];
                        
                        turnServers = [RTCIceServer serversFromCEODJSONDictionary:dict];
                        
                        completionHandler(turnServers);
                    }];
}
#endif

- (void)disconnect
{
    [DataSingleton sharedInstance].sipCall_ID = nil;
    [DataSingleton sharedInstance].isIncomingCallAccept = NO;
    [DataSingleton sharedInstance].isIncomingCallDecline = NO;
    _isInitiator = NO;
    
    
    [self stopGetStatusTimer];
//    self.localMediaStream=nil;
    self.localVideoTrack=nil;
    self.localAudioTrack=nil;
    self.remoteVideoTrack=nil;
 
    if(_peerConnection)
    {
    [_peerConnection close];
//    [_peerConnection removeStream:_localMediaStream];
    [_peerConnection stopRtcEventLog];
    _peerConnection  = nil;
    }
    _candidatesGathered =NO;
    [_factory stopAecDump];
    self.incomingPayload = nil;
    self.outgoingPayload = nil;
    NSLog(@"changing gathering changed to NO");
    self.state = kARDAppClientStateDisconnected;
    self.queuedRemoteCandidates = nil;
    _isSpeakerEnabled = NO;
        
}
#pragma mark - Private



- (void)startPeerConnection:(BOOL) flage
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    //  self.state = kARDAppClientStateConnected;
    
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
    // config.tcpCandidatePolicy = RTCTcpCandidatePolicyEnabled;
    config.tcpCandidatePolicy = RTCTcpCandidatePolicyDisabled;
    config.bundlePolicy = RTCBundlePolicyBalanced;
    
    config.iceServers = _iceServers;
    config.iceTransportPolicy = RTCIceTransportPolicyRelay;
//    config.candidateNetworkPolicy=RTCCandidateNetworkPolicyLowCost;
    config.rtcpMuxPolicy=RTCRtcpMuxPolicyRequire;
//       config.iceTransportPolicy = RTCIceTransportPolicyAll;
    
    
//    RTCCertificate *pcert = [RTCCertificate generateCertificateWithParams:@{
//       @"expires" : @100000,
//       @"name" : @"RSASSA-PKCS1-v1_5"
//     }];
     config.sdpSemantics = RTCSdpSemanticsUnifiedPlan;
//     config.certificate = pcert;

    
    
    //
    
    //   if  ([strIPAddtessFamily  isEqual:IP_ADDR_IPv4])
    //   {
    //     config.iceTransportPolicy=RTCIceTransportPolicyRelay;  // for ipv 4 we need relay type transport
    //   }
    //   else
    //    {
    //    config.iceTransportPolicy =RTCIceTransportPolicyRelay;
    //    }
    
    // config.rtcpMuxPolicy=RTCRtcpMuxPolicyNegotiate;
    //murli changes for App to PSTN 2 call failed out of 10 dated 30-11-2016
//    config.rtcpMuxPolicy=  RTCRtcpMuxPolicyRequire;
    
    mediaConstraint = [self mediaConstraints];
    // dispatch_sync(dispatch_get_main_queue(),^{
    

    
    _peerConnection = [_factory peerConnectionWithConfiguration:config
                                                    constraints:mediaConstraint
                                                       delegate:self];
    
//
//    NSNumber *minBR=[NSNumber numberWithUnsignedInteger:8000];
//    NSNumber *curBR=[NSNumber numberWithUnsignedInteger:8000];
//    NSNumber *maxBR=[NSNumber numberWithUnsignedInteger:8000];
//
//    BOOL isSetRate= [_peerConnection setBweMinBitrateBps:minBR currentBitrateBps:curBR maxBitrateBps:maxBR];

    //  });
    [NSThread sleepForTimeInterval:0.5f]; // murli dated 3-11-16
    
//    RTCDataChannelConfiguration *datainit = [[RTCDataChannelConfiguration alloc] init];
//    
//    @try {
//        
//        datainit.isNegotiated = YES;
//        
//        datainit.isOrdered = YES;
//        
//        datainit.maxRetransmits = 30;
//        
//        datainit.maxPacketLifeTime = 30000;
//        
////        datainit.channelId = 100;
//        
//        
//        
//        RTCDataChannel *dataChannel = [_peerConnection dataChannelForLabel:@"MyDataChannel" configuration:datainit];
//        
//        dataChannel.delegate = self;
//        
//        
//        
//        RTCDataBuffer *buffer = [[RTCDataBuffer alloc] initWithData:[@"Hello" dataUsingEncoding:NSUTF8StringEncoding] isBinary:NO];
//        
//        [dataChannel sendData:buffer];
//        
//    }
//    
//    @catch(NSException *nse) {
//        
//        NSLog(@"%@", nse);
//        
//    }
    
    
    NSLog(@"print iceServers:%@\n",_iceServers);
    
    
    NSLog(@"checking _peerconnection is null or no");
    
    // Create AV media stream and add it to the peer connection.
    
    printf("printing video value of BOOL value:%d",flage);
//    [self createLocalMediaStream:flage];
//    [_peerConnection addStream:_localMediaStream];
    [self createMediaSenders];
    
    NSLog(@"added local media stream");
    
    if (_isInitiator)
    {
        NSLog(@"yes, initiator");
        
        NSLog(@"creating offer");
        // Send offer.
        
        // murli did changes for new webrtc
        
        __weak AppClient *weakSelf = self;
        [_peerConnection offerForConstraints:mediaConstraint
                           completionHandler:^(RTCSessionDescription *sdp,
                                               NSError *error) {
                               AppClient *strongSelf = weakSelf;
                               [strongSelf peerConnection:strongSelf.peerConnection
                              didCreateSessionDescription:sdp
                                                    error:error];
                           }];
    }
    else
    {
        // Check if we've received an offer.
        
        
        NSLog(@"Incoming INVITE  processing as offere , not initiator");
    }
    
    
    
}






// Processes the messages that we've received from the room server and the
// signaling channel. The offer or answer message must be processed before other
// signaling messages, however they can arrive out of order. Hence, this method
// only processes pending messages if there is a peer connection object and
// if we have received either an offer or answer.
- (void)drainMessageQueueIfReady
{
    //  if (!_peerConnection || !_hasReceivedSdp) {
    //    return;
    //  }
    //  for (ARDSignalingMessage *message in _messageQueue) {
    //    [self processSignalingMessage:message];
    //  }
    //  [_messageQueue removeAllObjects];
}

// Processes the given signaling message based on its type.
//- (void)processSignalingMessage:(ARDSignalingMessage *)message {
//  NSParameterAssert(_peerConnection ||
//      message.type == kARDSignalingMessageTypeBye);
//  switch (message.type) {
//    case kARDSignalingMessageTypeOffer:
//    case kARDSignalingMessageTypeAnswer: {
//      ARDSessionDescriptionMessage *sdpMessage =
//          (ARDSessionDescriptionMessage *)message;
//      RTCSessionDescription *description = sdpMessage.sessionDescription;
//      // Prefer H264 if available.
//      RTCSessionDescription *sdpPreferringH264 =
//          [ARDSDPUtils descriptionForDescription:description
//                             preferredVideoCodec:@"H264"];
//      [_peerConnection setRemoteDescriptionWithDelegate:self
//                                     sessionDescription:sdpPreferringH264];
//      break;
//    }
//    case kARDSignalingMessageTypeCandidate: {
//      ARDICECandidateMessage *candidateMessage =
//          (ARDICECandidateMessage *)message;
//      [_peerConnection addICECandidate:candidateMessage.candidate];
//      break;
//    }
//    case kARDSignalingMessageTypeBye:
//      // Other client disconnected.
//      // TODO(tkchin): support waiting in room for next client. For now just
//      // disconnect.
//      [self disconnect];
//      break;
//  }
//}
-(void)processSignalingMessage/*WithType:(int)type*/  //this thing is for incoming messages only !!!
{
    //  NSParameterAssert(_peerConnection); /modidfied by SM on 12:24
    NSLog(@"changed _peerconenction NSASSERT");
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    
    NSMutableString *answerSDP = [NSMutableString stringWithString:self.incomingPayload];
    
    RTCSessionDescription *incomingDescription;
    RTCSdpType type;
    if(self.isInitiator)
    {
        //this means answer
        type = RTCSdpTypeAnswer;
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        NSLog(@"incoming offer and creatinganswer with initiator flag");
        
    }
    else
    {
        //this means offer
        type = RTCSdpTypeOffer;
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
    }
    
    
    
    // murli implemented for new webrtc API
    
    __weak AppClient *weakSelf = self;
    [_peerConnection setRemoteDescription:incomingDescription
                        completionHandler:^(NSError *error) {
                            AppClient *strongSelf = weakSelf;
                            [strongSelf peerConnection:strongSelf.peerConnection
                     didSetSessionDescriptionWithError:error];
                        }];
    
    NSLog(@"setRemoteDescription happening here");
    
    
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    
    
    for (NSString * key in candidates)
    {
        for (NSString * candidate in [candidates objectForKey:key]) {
            // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
            
            iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
            
            if (self.queuedRemoteCandidates)
            {
                [self.queuedRemoteCandidates addObject:iceCandidate];
            }
            
            [_peerConnection addIceCandidate:iceCandidate];
            
        }
    }
    
    NSLog(@"added remote's candidates to peerconnection");
    
    
}


//}
//murli changed for creating offer with iNVITE 180 ringing
-(void)processIncomingInvite/*WithType:(int)type*/  //this thing is for incoming messages only !!!
{
    
    NSLog(@"startPeerConnection \n");
    
   if(_isAudioOnly)
    self.state = kARDAppClientAudioSessionCreated;
    
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
    config.iceServers = _iceServers;
    //  config.tcpCandidatePolicy = RTCTcpCandidatePolicyEnabled;
    config.tcpCandidatePolicy = RTCTcpCandidatePolicyDisabled;
    config.bundlePolicy = RTCBundlePolicyBalanced;
    config.iceTransportPolicy= RTCIceTransportPolicyRelay;
//      config.iceTransportPolicy=RTCIceTransportPolicyAll;
    //    if  ([strIPAddtessFamily  isEqual:IP_ADDR_IPv4])
    //    {
    //        config.iceTransportPolicy=RTCIceTransportPolicyRelay;  // for ipv 4 we need relay type transport
    //    }
    //    else
    //    {
    //        config.iceTransportPolicy =RTCIceTransportPolicyRelay;
    //    }
    
    //   config.rtcpMuxPolicy=RTCRtcpMuxPolicyNegotiate;
    //murli changes for App to PSTN 2 call failed out of 10 dated 30-11-2016
    config.rtcpMuxPolicy = RTCRtcpMuxPolicyRequire;
    config.sdpSemantics = RTCSdpSemanticsUnifiedPlan;

    mediaConstraint = [self mediaConstraints];
    
    _peerConnection = [_factory peerConnectionWithConfiguration:config
                                                    constraints:mediaConstraint
                                                       delegate:self];
    
//      [NSThread sleepForTimeInterval:0.5f];
    NSLog(@"creating peerConnection");
    
    NSLog(@"print iceServers:%@\n",_iceServers);
    
    
    NSLog(@"checking _peerconnection has null ");
    
    // Create AV media stream and add it to the peer connection.
    
    // self.state = kARDAppClientAudioSessionCreated;
//    [self createIncomingLocalMediaStream:isVideo];
//    [_peerConnection addStream:_localMediaStream];
    [self createMediaSenders];
    
    
    NSLog(@"creating local sdp offer with remote SDP \n");
    
    NSLog(@"changed _peerconenction NSASSERT");
    
    NSLog(@"processIncomingInvite ------------ stuff from Sofia presumably");
    
    
//    [DataSingleton sharedInstance].incomingPayload = [[DataSingleton sharedInstance].incomingPayload stringByReplacingOccurrencesOfString:@"a=fmtp:111 maxplaybackrate=16000;maxaveragebitrate=28000;minptime=10;useinbandfec=1"
//       withString:@"a=fmtp:111 minptime=10;useinbandfec=1"];
    NSMutableString *answerSDP = [NSMutableString stringWithString:self.incomingPayload];
    
  
    
    RTCSessionDescription *incomingDescription;
    RTCSdpType type;
    type = RTCSdpTypeOffer;
    
    //this means offer
    incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
    
    // murli changes for new webrtc
    __weak AppClient *weakSelf = self;
    [_peerConnection setRemoteDescription:incomingDescription
                        completionHandler:^(NSError *error) {
                            AppClient *strongSelf = weakSelf;
                            [strongSelf peerConnection:strongSelf.peerConnection
                     didSetSessionDescriptionWithError:error];
                        }];
    
    
    NSLog(@"setRemoteDescription happening here");
    
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    
    
    for (NSString * key in candidates)
    {
        for (NSString * candidate in [candidates objectForKey:key]) {
            // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
            iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
            if (self.queuedRemoteCandidates)
            {
                [self.queuedRemoteCandidates addObject:candidate];
            }
            
            [_peerConnection addIceCandidate:iceCandidate];
            
        }
    }
    
}
-(BOOL) isH264Codec:(NSString *) str_sdp
{
    BOOL isTrue = FALSE;
    NSString *codecnumber;
    NSArray *arr = [str_sdp componentsSeparatedByString:@"\n"];
    int count = -1;
    for(NSString *str in arr)
    {
        if([str    hasPrefix:@"m=video"])
        {
            NSArray *iscodec = [str componentsSeparatedByString:@" "];
            
            for(NSString *str in iscodec)
            {
                count++;
                if([str isEqualToString:@"TCP/TLS/RTP/SAVPF"])
                {
                    codecnumber = [iscodec objectAtIndex:(count+1)];
                    break;
                }
            }
            
        }
        else if([str hasPrefix:@"a=rtpmap:" ])
        {
            NSArray *temp = [str componentsSeparatedByString:@" "];
            BOOL isloopbreak = false;
            NSString *codec_number = [NSString stringWithFormat:@"a=rtpmap:%@",codecnumber];
            bool iscodece = NO;
            for(NSString *strstr in temp)
            {
                if([strstr isEqualToString:codec_number])
                    iscodece = true;
                if( iscodece && [strstr containsString:@"H264/90000"])
                {
                    isTrue = YES;
                    NSLog(@"Its H264 codec \n");
                    isloopbreak = YES;
                    break;
                }
                else if ( iscodece && [strstr containsString:@"VP8/90000"])
                {
                    isTrue = NO;
                    isloopbreak = YES;
                    NSLog(@"Its VP8  codec\n");
                    break;
                }
            }
            if(isloopbreak)
                break;
        }
        
    }
    
    return isTrue;
}

//pintu
-(void) creatingAnserForIncomingCall
{
    NSLog(@"changed _peerconenction NSASSERT");
    
    
    NSLog(@"processSignalingMessage ------------ stuff from Sofia presumably murli");
    if([DataSingleton sharedInstance].client.dIsPSTN != YES)
    {
        if(_isInitiator)
        {
            self.incomingPayload = [self.incomingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:active"];
            
        }else
        {
            self.incomingPayload = [self.incomingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:passive"];
        }
    }
   
    NSMutableString *answerSDP = [NSMutableString stringWithString:self.incomingPayload];
    
    
    if(self.queuedRemoteCandidates)
    {
        [_peerConnection removeIceCandidates:self.queuedRemoteCandidates];
        [self.queuedRemoteCandidates removeAllObjects];
        self.queuedRemoteCandidates = nil;
    }
    
    self.queuedRemoteCandidates = [NSMutableArray array];
    
    RTCSessionDescription *incomingDescription;
    
    //this means answer
    RTCSdpType type;
    type = RTCSdpTypeAnswer;
    RTCSessionDescription *answer_sdp = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
#if 1
    // Prefer H264 if available.
    // Changes for codec negotiation based on RFC 3264
    if( !_isAudioOnly && [self  isH264Codec:answer_sdp.description] )
        incomingDescription = [SDPUtils descriptionForDescription:answer_sdp preferredVideoCodec:@"H264"];
    else
    {
        
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        
    }
#endif
    //  incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
    NSLog(@"incoming offer and creatinganswer with initiator flag");
    // murli changes for new webrtc
    
//    __weak AppClient *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(),^{
        [_peerConnection setRemoteDescription:incomingDescription
                            completionHandler:^(NSError *error) {
//                                AppClient *strongSelf = weakSelf;
//                                [strongSelf peerConnection:strongSelf.peerConnection
//                         didSetSessionDescriptionWithError:error];
                                NSLog(@"error :%@",error);

                            }];
    });
    NSLog(@"setRemoteDescription happening here");
    
    
    self.state =  kARDAppClientStateConnected;
    
    NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
    RTCIceCandidate *iceCandidate;
    
    
    for (NSString * key in candidates)
    {
        for (NSString * candidate in [candidates objectForKey:key]) {
            // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
            iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
            if (self.queuedRemoteCandidates)
            {
                [self.queuedRemoteCandidates addObject:iceCandidate];
            }
            
            [_peerConnection addIceCandidate:iceCandidate];
            
        }
    }
    
    if (_isSpeakerEnabled == YES)
    {
        [self performSelector:@selector(enableSpeaker) withObject:nil afterDelay:1];
    }
    //[self setMaxBitrateForPeerConnectionSender:@(6)];
    
    NSLog(@"created Anser at calee party for incoming call to peerconnection");
    
    
}


// Sends a signaling message to the other client. The caller will send messages
// through the room server, whereas the callee will send messages over the
// signaling channel.
//- (void)sendSignalingMessage:(ARDSignalingMessage *)message {
//  if (_isInitiator) {
//    __weak ARDAppClient *weakSelf = self;
//    [_roomServerClient sendMessage:message
//                         forRoomId:_roomId
//                          clientId:_clientId
//                 completionHandler:^(ARDMessageResponse *response,
//                                     NSError *error) {
//      ARDAppClient *strongSelf = weakSelf;
//      if (error) {
//        [strongSelf.delegate appClient:strongSelf didError:error];
//        return;
//      }
//      NSError *messageError =
//          [[strongSelf class] errorForMessageResultType:response.result];
//      if (messageError) {
//        [strongSelf.delegate appClient:strongSelf didError:messageError];
//        return;
//      }
//    }];
//  } else {
//    [_channel sendMessage:message];
//  }
//}






//- (void)createIncomingLocalMediaStream:(BOOL)video
//{
//
//    NSLog(@"Creating Local Media \n");
//
//
//    if(video)
//    {
//        // [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;   //sanju jID
//        [DataSingleton sharedInstance].incomingLocalVideoTrack = [self createLocalVideoTrack];
//        if ([DataSingleton sharedInstance].incomingLocalVideoTrack)
//        {
//            [_delegate appClient:self didReceiveLocalVideoTrack:[DataSingleton sharedInstance].incomingLocalVideoTrack];
//            NSLog(@"inside local video track");
//        }
//
//    }
//    else
//    {
//        if (self.localMediaStream)
//        {
//            [_peerConnection removeStream:self.localMediaStream];
//            self.localMediaStream = nil;
//            _localAudioTrack = nil;
//        }
//        _localMediaStream = [_factory mediaStreamWithStreamId:@"ARDAMS"];
//
//    }
//    _localAudioTrack = [_factory audioTrackWithTrackId:@"ARDAMSa0"];
//    //dispatch_async(dispatch_get_main_queue(),^{
//    [_localMediaStream addAudioTrack:_localAudioTrack];
//    // });
////    if (_isSpeakerEnabled == YES)
////    {
////        [self performSelector:@selector(enableSpeaker) withObject:nil afterDelay:1];
////    }
//
//    NSLog(@"outside local media stream outside for remote");
//
//
//}

-(void) removeResource
{
    
//    if(!_isAudioOnly)
//    {
//        if(_isIncomingCall  && _peerConnection && self.localMediaStream  )
//        {
//            NSLog(@"Case2  \n");
//            [_peerConnection removeStream:_localMediaStream];
////            self.localMediaStream = nil;
////            [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
////            [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
//            _localAudioTrack = nil;
//        }
//        else if (_peerConnection && self.localMediaStream) {
//
//            [_peerConnection removeStream:self.localMediaStream];
//            self.localMediaStream=nil;
//            self.localVideoTrack=nil;
//            self.localAudioTrack=nil;
//        }
//
//    }
//    else if (self.localMediaStream)
//    {
//
//        [_peerConnection removeStream:self.localMediaStream];
//        _localAudioTrack = nil;
//        self.localMediaStream = nil;
//    }
    
//    self.localMediaStream=nil;
    self.localVideoTrack=nil;
    self.localAudioTrack=nil;
    self.remoteVideoTrack=nil;
}


//-(void) createLocalMediaStream:(BOOL)video
//{
//
//
//    //   _localMediaStream = [_factory mediaStreamWithLabel:@"ARDAMS"];
//    if(video == YES)
//    {
//        _localVideoTrack = [self createLocalVideoTrack];
//        if (_localVideoTrack)
//        {
//            [_localMediaStream addVideoTrack:_localVideoTrack];
//            if(!_isIncomingCall)
//                [_delegate appClient:self didReceiveLocalVideoTrack:_localVideoTrack];
//            NSLog(@"inside local video track");
//        }
//
//    }
//    else
//    {
//        if (self.localMediaStream)
//        {
//            [_peerConnection removeStream:self.localMediaStream];
//            _localAudioTrack = nil;
//            self.localMediaStream = nil;
//        }
//
//        _localMediaStream = [_factory mediaStreamWithStreamId:@"ARDAMS"];
//
//    }
//    _localAudioTrack = [_factory audioTrackWithTrackId:@"ARDAMSa0"];
//    //  dispatch_async(dispatch_get_main_queue(),^{
//    [_localMediaStream addAudioTrack:_localAudioTrack];
//    //});
////    if (_isSpeakerEnabled == YES)
////    {
////        [self performSelector:@selector(enableSpeaker) withObject:nil afterDelay:1];
////    }
//    NSLog(@"created local media stream  for remote party");
//
//
//    // return localStream;
//}

- (RTCMediaConstraints *)defaultMediaStreamConstraints {
    // NSString *aspectRatio = [NSString stringWithFormat:@"%f",(double)16/9];
#if 0
    //   its working
    NSString *aspectRatio =@"0.5";
    
    // NSDictionary  *mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"30",kRTCMediaConstraintsMinFrameRate,@"720",kRTCMediaConstraintsMaxHeight,@"1280",kRTCMediaConstraintsMaxWidth,aspectRatio,kRTCMediaConstraintsMinAspectRatio,@"true",@"IceRestart", nil];
    NSDictionary  *mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"30",kRTCMediaConstraintsMinFrameRate,@"375",kRTCMediaConstraintsMaxHeight,@"439",kRTCMediaConstraintsMaxWidth,aspectRatio,kRTCMediaConstraintsMinAspectRatio,@"true",@"IceRestart", nil];
    
    RTCMediaConstraints* constraints =[[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:mandatoryConstraints];
    
    return constraints;
#endif
#if 1
    //   its working
    // NSString *aspectRatio =@"1.77";
//    NSString *aspectRatio = [NSString stringWithFormat:@"%f",(double)16/9];
    // NSDictionary  *mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"30",kRTCMediaConstraintsMinFrameRate,@"720",kRTCMediaConstraintsMaxHeight,@"1280",kRTCMediaConstraintsMaxWidth,aspectRatio,kRTCMediaConstraintsMinAspectRatio,@"true",@"IceRestart", nil];
    NSDictionary  *mandatoryConstraints =[[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"DtlsSrtpKeyAgreement",@"true",@"googIPv6",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl", nil];
    
    RTCMediaConstraints* constraints =[[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:mandatoryConstraints];
    
    return constraints;
#endif
    
#if 0
    NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"IceRestart", nil];
    RTCMediaConstraints* constraints =
    [[RTCMediaConstraints alloc]
     initWithMandatoryConstraints:nil
     optionalConstraints:mandatoryConstraints];
    return constraints;
#endif
}

- (RTCRtpTransceiver *)videoTransceiver {
  for (RTCRtpTransceiver *transceiver in _peerConnection.transceivers) {
    if (transceiver.mediaType == RTCRtpMediaTypeVideo) {
      return transceiver;
    }
  }
  return nil;
}

- (void)createMediaSenders {
  RTCMediaConstraints *constraints = [self defaultOfferConstraints];
  RTCAudioSource *source = [_factory audioSourceWithConstraints:constraints];
  RTCAudioTrack *track = [_factory audioTrackWithSource:source
                                                trackId:kARDAudioTrackId];
   _localAudioTrack = track;
  [_peerConnection addTrack:_localAudioTrack streamIds:@[ kARDMediaStreamId ]];
    
if(!self.isAudioOnly)
{
  _localVideoTrack = [_factory videoTrackWithSource:[_factory videoSource] trackId:kARDVideoTrackId];
  if (_localVideoTrack) {
    [_peerConnection addTrack:_localVideoTrack streamIds:@[ kARDMediaStreamId ]];
    RTCVideoTrack *trackLocal = (RTCVideoTrack *)([self videoTransceiver].sender.track);
    [_delegate appClient:self didReceiveLocalVideoTrack:trackLocal];
    // We can set up rendering for the remote track right away since the transceiver already has an
    // RTCRtpReceiver with a track. The track will automatically get unmuted and produce frames
    // once RTP is received.
    RTCVideoTrack *track = (RTCVideoTrack *)([self videoTransceiver].receiver.track);
      _remoteVideoTrack = track;
    [_delegate appClient:self didReceiveRemoteVideoTrack:track];
  }
}
}

//- (RTCVideoTrack *)createLocalVideoTrack
//{
//
//    RTCVideoSource *source = [_factory videoSource];
//
//    return [_factory videoTrackWithSource:source trackId:kARDVideoTrackId];
//
//}



- (void)setLocalholdUnhold:(BOOL)isHold
{

      NSString*sdp=self.outgoingPayload;
       if(sdp != nil)
       {
      NSMutableString*answerSDP;
       
       if(isHold)
       {
           sdp = [sdp stringByReplacingOccurrencesOfString:@"sendrecv" withString:@"inactive"];
       }
       else
       {
           sdp = [sdp stringByReplacingOccurrencesOfString:@"inactive" withString:@"sendrecv"];
       }
       
       
           answerSDP = [NSMutableString stringWithString:sdp];
       
              RTCSessionDescription *outgDescription;
              RTCSdpType type;
              type = RTCSdpTypeOffer;
       
              //this means offer
              outgDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
       
              // murli changes for new webrtc
              __weak AppClient *weakSelf = self;
               [_peerConnection setLocalDescription:outgDescription
                                      completionHandler:^(NSError *error) {
                                          AppClient *strongSelf = weakSelf;
                                          [strongSelf peerConnection:strongSelf.peerConnection
                                   didSetSessionDescriptionWithError:error];
                                      }];
       }
    
}
#if 1
- (void)muteUnmute:(BOOL)flage
{
    for (RTCRtpSender *sender in _peerConnection.senders)
    {
        if(flage)
            sender.track.isEnabled = false;
        else
           sender.track.isEnabled = true;

    }
        
        
//    if (_peerConnection.localmeStreams) {
//        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
//            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] count]; j++) {
//                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] objectAtIndex:j];
//                track.isEnabled = NO;
//
//            }
//        }
//    }
    
}

//- (void)unmute
//{
//    if (_peerConnection.localStreams) {
//        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
//            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] count]; j++) {
//                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] audioTracks] objectAtIndex:j];
//                track.isEnabled = YES;
//            }
//        }
//    }
////    if (_isSpeakerEnabled == YES)
////    {
////        [self performSelector:@selector(enableSpeaker) withObject:nil afterDelay:1];
////    } else {
////        [self performSelector:@selector(disableSpeaker) withObject:nil afterDelay:1];
////    }
//}

- (void)muteVideo
{
    if (_peerConnection.localStreams) {
        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] count]; j++) {
                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] objectAtIndex:j];
                track.isEnabled = NO;
                
            }
        }
    }
}

- (void)unmuteVideo
{
    if (_peerConnection.localStreams) {
        for (int i = 0; i < [_peerConnection.localStreams count]; i++) {
            for (int j = 0; j < [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] count]; j++) {
                RTCMediaStreamTrack * track = [[[_peerConnection.localStreams objectAtIndex:i] videoTracks] objectAtIndex:j];
                track.isEnabled = YES;
            }
        }
    }
//    if (_isSpeakerEnabled == YES)
//    {
//        [self performSelector:@selector(enableSpeaker) withObject:nil afterDelay:1];
//    } else {
//        [self performSelector:@selector(disableSpeaker) withObject:nil afterDelay:1];
//    }
}

#endif

#pragma mark - enable/disable speaker

- (void)enableSpeaker {
//    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    
   AVAudioSession *session = [AVAudioSession sharedInstance];
               NSError *error = nil;
               if (![session setMode:AVAudioSessionModeVoiceChat error:&error]) {
                   NSLog(@"AVAudiosession setMode %@",error);
               }

               if (![session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error]) {
                   NSLog(@"AVAudiosession overrideOutputAudioPort %@",error);
               }
    
    _isSpeakerEnabled = YES;
    NSLog(@"enableSpeaker \n");
    
}

- (void)disableSpeaker {
    // Overwrite the audio route
             AVAudioSession *session = [AVAudioSession sharedInstance];
             NSError *error = nil;
             if (![session setMode:AVAudioSessionModeVoiceChat error:&error]) {
                 NSLog(@"AVAudiosession setMode %@",error);
             }

             if (![session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error]) {
                 NSLog(@"AVAudiosession overrideOutputAudioPort %@",error);
             }
    _isSpeakerEnabled = NO;
    
    NSLog(@"disableSpeaker \n");
    
}
-(void)count
{
    RTCMediaStream* stream;
    NSLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
    unsigned long i=stream.videoTracks.count;
    unsigned long j=stream.audioTracks.count;
    printf("number of video and audio tracks:%lu %lu",i,j);
    
}

#pragma mark - RTCPeerConnectionDelegate
// Callbacks for this delegate occur on non-main thread and need to be
// dispatched back to main queue as needed.

- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeSignalingState:(RTCSignalingState)stateChanged
{
    NSLog(@"Signaling state changed: %ld", (long)stateChanged);
}

//pintu
- (void)peerConnection:(RTCPeerConnection *)peerConnection didAddStream:(RTCMediaStream *)stream
{
    
//    NSLog(@"peerConnection:addedStream:   Dude...you succeeded...",peerConnection);
    
    

    dispatch_async(dispatch_get_main_queue(), ^{
        //RTCLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
        NSLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
        if (stream.videoTracks.count) {
            RTCVideoTrack *videoTrack = stream.videoTracks[0];
            
//            if(_isIncomingCall)
//            {
//                if([DataSingleton sharedInstance].incomingRemoteVideoTrack)
//                    [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil; // sanju
//                [DataSingleton sharedInstance].incomingRemoteVideoTrack = videoTrack;
                NSLog(@" murli remot video having \n");
//            }
            _remoteVideoTrack = videoTrack;
            [_delegate appClient:self didReceiveRemoteVideoTrack:videoTrack];
            self.state = kARDAppClientAudioSessionCreated;
        }
       
    });
    if (_isSpeakerEnabled == YES)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(enableSpeaker) withObject:nil afterDelay:1];
        });
      
        NSLog(@" SP ON");

    }
    self.state = kARDAppClientStateConnected;
    //   self.state = kARDAppClientAudioSessionCreated;
    
    
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didRemoveStream:(RTCMediaStream *)stream
{
    
    //    if (_peerConnection && self.localMediaStream) {
    //        [_peerConnection removeStream:self.localMediaStream];
    //        self.localMediaStream=nil;
    //        self.localVideoTrack=nil;
    //        self.localAudioTrack=nil;
    //    }
    //    if(_isIncomingCall  && _peerConnection && self.localMediaStream  )
    //    {
    //        [_peerConnection removeStream:_localMediaStream];
    //        self.localMediaStream = nil;
    //        [DataSingleton sharedInstance].incomingRemoteVideoTrack = nil;
    //        [DataSingleton sharedInstance].incomingLocalVideoTrack = nil;
    //        _localAudioTrack = nil;
    //    }
    //
    RTCLog(@"Stream was removed.");
}

- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection
{
    RTCLog(@"WARNING: Renegotiation needed but unimplemented.");
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceConnectionState:(RTCIceConnectionState)newState
{
    
    
  
#if Debug
    RTCLog(@"ICE state changed: %ld",(long) newState);
#endif
    dispatch_async(dispatch_get_main_queue(), ^{
        //pintu
      //  [_delegate appClient:self didChangeConnectionState:newState];
      //  murli commented for time being.
//        if (newState == RTCIceConnectionStateCompleted)
//        {
//            [self startGetStatusTimer];
//        }
    });

}

/// Method that calls your timer.
- (void)stopGetStatusTimer
{
    [getStatsTimer invalidate];
    getStatsTimer = nil;
    _preBytesSent=0;
    _bytesSent=0;
    _googRtt=0;
    
    _bytesReceived=0;
    _preBytesReceived=0;
    
}

// Method that kicks it all off
- (void)startGetStatusTimer
{
    [getStatsTimer invalidate];
    getStatsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                               target:self
                                             selector:@selector(getRTCLegacyStatsReport)
                                             userInfo:nil
                                              repeats:YES];
}
-(void)getRTCLegacyStatsReport
{
    
//    RTCMediaStreamTrack * track = _peerConnection.senders[0].track;
//    [_peerConnection statsForTrack:track statsOutputLevel:RTCStatsOutputLevelStandard completionHandler:^(NSArray<RTCLegacyStatsReport *> * _Nonnull stats) {
//
//        NSLog(@"RTCLegacyStatsReport :%@",stats);
//    }];
    
//    NSLog(@"Prev ByteSent  :%ld",_bytesSent);

    
    [_peerConnection statsForTrack:nil
                            statsOutputLevel:RTCStatsOutputLevelStandard
                           completionHandler:^(NSArray<RTCLegacyStatsReport *> * _Nonnull stats) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   for (RTCLegacyStatsReport *report in stats)
                                   {
                                    
                                       if ([report.reportId isEqualToString:@"Conn-audio-1-0"] &&[report.type rangeOfString:@"googCandidatePair"].location != NSNotFound)
                                       {
                                           NSDictionary *values = report.values;
                                           long int bReceived=[[values objectForKey:@"bytesReceived"] integerValue];
                                           _bytesReceived=bReceived-_preBytesReceived;
                                           _preBytesReceived=bReceived;
                                           
                                           long int bSent=[[values objectForKey:@"bytesSent"] integerValue];
                                           _bytesSent=bSent-_preBytesSent;
                                           _preBytesSent=bSent;
                                           _googRtt=[[values objectForKey:@"googRtt"] integerValue];
                                          // NSLog(@"_bytesSent :%ld  googRtt :%ld",_bytesSent,_googRtt);
                                           NSString*latency=[NSString stringWithFormat:@"%ld",_googRtt];

                                           float per = 0.0;
                                           if (_bytesSent == 0)
                                           {
                                               per=-1.0;
                                               latency=@"0";
                                           }
                                           if (_bytesSent>0)
                                            per=(1360-_googRtt)/800.0;
                                           
                                           if (per>=1)
                                           {
                                               [self updateBiteRate];
                                           }else if(per>0.75)
                                           {
                                               [self updateBiteRate];

                                           } else if(per>0.50)
                                           {
                                               [self reduceBiteRate];
                                           }
                                           else if(per>0.1)
                                           {
                                               [self reduceBiteRate];

                                           }else if(per>0.05)
                                           {
                                               [self reduceBiteRate];
                                           }
                                         
                                           NSDictionary *dict = @{ @"connectivity_strength" : [NSString stringWithFormat:@"%f",per],
                                                                   @"bytesSent" : [NSString stringWithFormat:@"%ld",_bytesSent],
                                                                   @"bytesReceived":[NSString stringWithFormat:@"%ld",_bytesReceived],
                                                                   @"Current_bitRate":[NSString stringWithFormat:@"%@k",_currentBiteRate],
                                                                   @"Latency":latency,
                                                                   };

                                           [_delegate didChangeConnectionState:dict];
                                       }
                                   }
                                   
                                   
//                                  NSLog(@"RTCLegacyStatsReport :%@",stats);
//                                   [self parseAudioSendRecvStatsReport:[stats objectAtIndex:8]];
//
                                   
//                                   if(!stats)
//                                   {
//                                       NSLog(@"Status not come");
//
//                                   }else
//                                   {
//                                       for (RTCLegacyStatsReport *report in stats) {
//                                           [self parseStatsReport:report];
//                                       }
//                                   }
                               
//                                   _statsBuilder = [[ARDStatsBuilder alloc] init];
//
//
//                                   for (RTCLegacyStatsReport *report in stats) {
//                                       [_statsBuilder parseStatsReport:report];
//                                   }
//                                   NSLog(@" _statsBuilder.statsString : %@", _statsBuilder.statsString);
                               

                               });
                               

                           }];
    
    
    
}

-(NSArray*)getValideReport:(NSArray*)array
{
    NSPredicate *sPredicate =
    [NSPredicate predicateWithFormat:@"SELF.reportId contains[c] 'Conn-audio-'"];
    return [array filteredArrayUsingPredicate:sPredicate];
}

-(void)updateBiteRate
{
    if([DataSingleton sharedInstance].client.dIsPSTN)
    {
        if([_currentBiteRate  isEqual: @(6)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:8];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
        }
        
    }else{
        if(_currentBiteRate == [NSNumber numberWithUnsignedInteger:48])
        {
            //        NSLog(@"Excelent Connection");
        }
        else if([_currentBiteRate  isEqual: @(6)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:8];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
            
        }else if([_currentBiteRate  isEqual: @(8)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:16];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
            
        }else if([_currentBiteRate  isEqual: @(16)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:32];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
            
        }else if([_currentBiteRate  isEqual: @(32)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:48];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
        }
    }
    
//    NSLog(@"Current BitRate: %ld",(long)[_currentBiteRate integerValue]);
}
-(void)reduceBiteRate
{
    
    if([DataSingleton sharedInstance].client.dIsPSTN)
    {
       
        if([_currentBiteRate  isEqual: @(8)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:6];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
        }
        
    }else{
        if(_currentBiteRate == [NSNumber numberWithUnsignedInteger:6])
        {
            //        NSLog(@"Poor Connection");
        }
        else if([_currentBiteRate  isEqual: @(8)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:6];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
            
        }else if([_currentBiteRate  isEqual: @(16)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:8];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
            
        }else if([_currentBiteRate  isEqual: @(32)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:16];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
        }else if([_currentBiteRate  isEqual: @(48)])
        {
            _currentBiteRate=[NSNumber numberWithUnsignedInteger:32];
            [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
        }
    }
    
   
    
//    NSLog(@"reduceBiteRate BitRate: %ld",(long)[_currentBiteRate integerValue]);
}


- (void)sendDTMF:(NSString *)digits {
    for (RTCRtpSender *sender in _peerConnection.senders) {
            if(sender.dtmfSender.canInsertDtmf)
            {
                [sender.dtmfSender insertDtmf:digits duration:0.400 interToneGap:0.50];
                NSLog(@"send DTMF : %@",digits);
            }
        }
   
}


- (void)setMaxBitrateForPeerConnectionSender:(NSNumber *)maxBitrate {
    for (RTCRtpSender *sender in _peerConnection.senders) {
        if (sender.track != nil) {
                [self setMaxBitrate:maxBitrate forSender:sender];
        }
    }
}

- (void)setMaxBitrate:(NSNumber *)maxBitrate forSender:(RTCRtpSender *)sender {
    if (maxBitrate.intValue <= 0) {
        return;
    }
    RTCRtpParameters *parametersToModify = sender.parameters;
    for (RTCRtpEncodingParameters *encoding in parametersToModify.encodings) {
        encoding.maxBitrateBps = @(maxBitrate.intValue * kKbpsMultiplier);
    }
    [sender setParameters:parametersToModify];
}


- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceGatheringState:(RTCIceGatheringState)newState
{
    
//        NSLog(@"peerConnection.senders[0].parameters.encodings[0].maxBitrateBps = %@",peerConnection.senders[0].parameters.encodings[0].maxBitrateBps);
    
    //RTCLog(@"ICE gathering state changed: %d", newState);
    dispatch_async(dispatch_get_main_queue(), ^{
        
      
        NSString *str = [test stringFromDate:[NSDate date]];
        
        
        switch(newState)
        {
            case RTCIceGatheringStateNew:
                
//                peerConnection.senders[0].parameters.encodings[0].maxBitrateBps=[NSNumber numberWithUnsignedInteger:8000];
                
                if ([_iceCandidates count] > 0)
                {
                    
                    [self candidateGatheringComplete];
                }
                else
                    printf("it is gathering yet to reach 1---RTCIceGatheringNew");
                
                
                printf("RTCIceGaterhingNew");
                break;
                
                
            case RTCIceGatheringStateGathering:
                if ([_iceCandidates count] > 0)
                {
                    
                    [self candidateGatheringComplete];
                }
                else
                    printf("it is gathering yet to reach 1---RTCIceGatheringGathering");
                break;
                
            case RTCIceGatheringStateComplete:
                if ([_iceCandidates count] > 0)
                {
                    
                    [self candidateGatheringComplete];
                    NSLog(@"Pringting  Asis completed ice candidate from Party A %@:",str);
                }
                else
                {
                    NSLog(@"state complete collected--RTCIceGatheringComplete");
                }
                
                NSLog(@"it is reaching RTCIceGathering--complete");
                break;
                
         
                
        }
        
        
        NSLog(@"PCO onIceGatheringChange. %ld", (long)newState);
    });
    
}

-(void)setcallertype:(BOOL)flag
{
    // calleroption=flag;
}


//fired for LOCALly generated candidates, right ?
- (void)peerConnection:(RTCPeerConnection *)peerConnection didGenerateIceCandidate:(RTCIceCandidate *)candidate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSLog(@"peerConnection:gotICECandidate:");
        
     
        
        [_iceCandidates addObject:candidate];
        
        
        if( ([_iceCandidates count] >= 1) ||  _candidatesGathered == NO)
        {
            
            [self performSelector:@selector(candidateGatheringComplete) withObject:nil afterDelay:0.5f];
            NSLog(@"firing without delay");
            NSLog(@"candidate gathering when count ==1");
        }
        
    });
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didOpenDataChannel:(RTCDataChannel *)dataChannel
{
    
}

- (void)peerConnection:(nonnull RTCPeerConnection *)peerConnection didRemoveIceCandidates:(nonnull NSArray<RTCIceCandidate *> *)candidates {
    /*Write Code*/
}




- (void)peerConnection:( RTCPeerConnection*)channel didReceivedSignalWithSessionDescription:(NSString *)sessionDescription withType:(NSString *)type {
    
    RTCSdpType typeoffer;
    typeoffer = RTCSdpTypeOffer;
    RTCSessionDescription *remoteDesc = [[RTCSessionDescription alloc] initWithType:typeoffer sdp:sessionDescription];
    //murli changes  for new webrtc
    __weak AppClient *weakSelf = self;
    [_peerConnection setRemoteDescription:remoteDesc
                        completionHandler:^(NSError *error) {
                            AppClient *strongSelf = weakSelf;
                            [strongSelf peerConnection:strongSelf.peerConnection
                     didSetSessionDescriptionWithError:error];
                        }];
    // [_peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:remoteDesc];
    NSLog(@"didReceivedSignalWithSessionDescription\n");
}




#pragma mark - RTCSessionDescriptionDelegate
// Callbacks for this delegate occur on non-main thread and need to be
// dispatched back to main queue as needed.

- (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error

{
    
    //ok...this happened at some point...
    //    Printing description of error:
    //    Error Domain=RTCSDPError Code=-1 "The operation couldn’t be completed. (RTCSDPError error -1.)"
    //    UserInfo=0x156bd7f0 {error=CreateAnswer can't be called before SetRemoteDescription.}
    
    
    
    NSLog(@"offer created callback; probably gets called for answer as well (it's a 'session description' itself, right?)");
    
    //first worry; hold on to the candidateless sdp
    // self.sdp = sdp.description;    //need to change the location
    
    
    //SDP example (before candindates)
    //    v=0
    //    o=- 7762952367168610188 2 IN IP4 127.0.0.1
    //    s=-
    //    t=0 0
    //    a=group:BUNDLE audio video
    //    a=msid-semantic: WMS ARDAMS
    //    m=audio 9 UDP/TLS/RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=ice-ufrag:5VvBHSTj7vSEjva1
    //    a=ice-pwd:7EoK4LSX3CMrOE+HKcR0QoQ6
    //    a=fingerprint:sha-256 D7:2B:3F:34:E9:87:27:4C:FA:32:C5:C4:7A:E6:25:CA:40:E3:BD:71:C3:28:B3:AB:3B:D2:66:BA:A4:F4:A9:4B
    //    a=setup:actpass
    //    a=mid:audio
    //    a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=sendrecv
    //    a=rtcp-mux
    //    a=rtpmap:111 opus/48000/2
    //    a=fmtp:111 minptime=10; useinbandfec=1
    //    a=rtpmap:103 ISAC/16000
    //    a=rtpmap:9 G722/8000
    //    a=rtpmap:102 ILBC/8000
    //    a=rtpmap:0 PCMU/8000
    //    a=rtpmap:8 PCMA/8000
    //    a=rtpmap:106 CN/32000
    //    a=rtpmap:105 CN/16000
    //    a=rtpmap:13 CN/8000
    //    a=rtpmap:127 red/8000
    //    a=rtpmap:126 telephone-event/8000
    //    a=maxptime:60
    //    a=ssrc:3096208943 cname:a0h5V831UvQWD8i9
    //    a=ssrc:3096208943 msid:ARDAMS ARDAMSa0
    //    a=ssrc:3096208943 mslabel:ARDAMS
    //    a=ssrc:3096208943 label:ARDAMSa0
    //    m=video 9 UDP/TLS/RTP/SAVPF 100 101 116 117 96
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=ice-ufrag:5VvBHSTj7vSEjva1
    //    a=ice-pwd:7EoK4LSX3CMrOE+HKcR0QoQ6
    //    a=fingerprint:sha-256 D7:2B:3F:34:E9:87:27:4C:FA:32:C5:C4:7A:E6:25:CA:40:E3:BD:71:C3:28:B3:AB:3B:D2:66:BA:A4:F4:A9:4B
    //    a=setup:actpass
    //    a=mid:video
    //    a=extmap:2 urn:ietf:params:rtp-hdrext:toffset
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=extmap:4 urn:3gpp:video-orientation
    //    a=recvonly
    //    a=rtcp-mux
    //    a=rtpmap:100 VP8/90000
    //    a=rtcp-fb:100 ccm fir
    //    a=rtcp-fb:100 nack
    //    a=rtcp-fb:100 nack pli
    //    a=rtcp-fb:100 goog-remb
    //    a=rtcp-fb:100 transport-cc
    //    a=rtpmap:101 VP9/90000
    //    a=rtcp-fb:101 ccm fir
    //    a=rtcp-fb:101 nack
    //    a=rtcp-fb:101 nack pli
    //    a=rtcp-fb:101 goog-remb
    //    a=rtcp-fb:101 transport-cc
    //    a=rtpmap:116 red/90000
    //    a=rtpmap:117 ulpfec/90000
    //    a=rtpmap:96 rtx/90000
    //    a=fmtp:96 apt=100
    
    
    
    
    
    
    if (error)
    {
        RTCLogError(@"Failed to create session description. Error: %@", error);
        [self disconnect];
        NSLog(@"%s", __FUNCTION__);
        
//        NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"Failed to create session description.", };
//        NSError *sdpError = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                                       code:kARDAppClientErrorCreateSDP
//                                                   userInfo:userInfo];

        //   [_delegate appClient:self didError:sdpError];
        return;
    }
    
    //      2015-12-11 11:07:24.441 webRTC_testbed[1235:996869] Failed to set session description. Error: Error Domain=RTCSDPError Code=-1 "The operation couldn’t be completed. (RTCSDPError error -1.)" UserInfo=0x16624d50 {error=Failed to set remote answer sdp: Called in wrong state: STATE_INIT}
    
    
    
    
    //  char const *l_sdp =
    //  "v=0\r\n\
    o=- 1449596610458467 1 IN IP4 192.168.3.4\r\n\
    s=X-Lite release 4.9.1 stamp 78725\r\n\
    c=IN IP4 192.168.11.41\r\n\
    t=0 0\r\n\
    m=audio 58710 RTP/AVP 8 0 101\r\n\
    a=rtpmap:101 telephone-event/8000\r\n\
    a=fmtp:101 0-15\r\n\
    a=sendrecv\r\n\
    ";
    //     NSString *hardCodedSDPString = [NSString stringWithCString:l_sdp encoding:NSUTF8StringEncoding];
    //     RTCSessionDescription *hardCodedSDP = [[RTCSessionDescription alloc] initWithType:@"offer" sdp:hardCodedSDPString];
    
    
    
    RTCSessionDescription *sdpPreferringH264;
  
#if 1
    if([sdp.description length]>0)
    {
    NSString *mangledSdpString = sdp.description;
    mangledSdpString = [mangledSdpString  stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
    mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:offerv=0" withString:@""];
    mangledSdpString = [mangledSdpString  stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
    mangledSdpString = [mangledSdpString stringByReplacingOccurrencesOfString:@"RTCSessionDescription:answerv=0" withString:@""];
    mangledSdpString = [self optimizePayload:mangledSdpString];
    sdpPreferringH264 = [[RTCSessionDescription alloc] initWithType:sdp.type sdp:mangledSdpString];
    self.sdp = sdpPreferringH264.description;
        /*
        
    if(self.isInitiator)
    {
//        sdpPreferringH264 = [SDPUtils descriptionForDescription:sdpPreferringH264 preferredAudioCodec:@"G722"];
        //its offer and answer for iphone to iphone
        
    //MMU    sdpPreferringH264 = [SDPUtils descriptionForDescription:sdpPreferringH264 preferredVideoCodec:@"H264"];
        self.sdp = sdpPreferringH264.description;
    }
    else
    {
        // Prefer H264 if available.
        if([self  isH264Codec:sdp.description])
        {
            sdpPreferringH264 = [SDPUtils descriptionForDescription:sdpPreferringH264 preferredVideoCodec:@"H264"];
            self.sdp = sdpPreferringH264.description;
        }
        else
        {
            self.sdp = sdpPreferringH264.description;
//            sdpPreferringH264 = sdp;
        }
    }
         */
#endif
    


    // did the changes for new webrtc
    __weak AppClient *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        
        [_peerConnection setLocalDescription:sdpPreferringH264
                           completionHandler:^(NSError *error) {
                               AppClient *strongSelf = weakSelf;
                               [strongSelf peerConnection:strongSelf.peerConnection
                        didSetSessionDescriptionWithError:error];
                           }];
        
    });
    
    //right after this, the ICE thingy actually starts or...resumes...or...whatever
    //the important thing is to expect gotICECandidate() to start fireing; candidades accumulate in _iceCandidates
    //then, iceGatheringChanged happens. When state is RTCIceGatheringComplete...
    //...I'm embedding all the candidates into the SDP and shoot it across the network via Sofia
    
    NSLog(@"setting local description");
    
    
    }
    
    
    
    //this sends it through Google's signaling; I want mine (Sofia)
    //    ARDSessionDescriptionMessage *message = [[ARDSessionDescriptionMessage alloc] initWithDescription:sdpPreferringH264];
    //    [self sendSignalingMessage:message];
    
}


//so: this gests called for both offers&answers  //murli 9-9-16 error is coming here need to fix it for PSTN
- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error
{
    NSLog(@"did set description callback; question: is it for local or remote?");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error)
        {
            
            RTCLogError(@"Failed to set session description. Error: %@", error);
            
            NSLog(@"Failed to set session description. Error: %@", error);
            
            [self disconnect];
            NSLog(@"%s", __FUNCTION__);
            
            NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"Failed to set session description.", };
            NSError *sdpError =
            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                       code:kARDAppClientErrorSetSDP
                                   userInfo:userInfo];
//            [_delegate appClient:self didError:sdpError];
            return;
        }
        
        // If we're answering and we've just set the remote offer we need to create
        // an answer and set the local description.
        
        /* if(_peerConnection.localDescription)
         {
         RTCSessionDescription *se=_peerConnection.localDescription;
         NSLog(@"inside didSetSessionDescriptionWithError printing values:%@\n",se.description);
         
         }*/
        
        /*   if(_peerConnection.signalingState == RTCSignalingHaveLocalOffer )
         {
         NSLog(@"it is RTCSignalingHaveLocalOffer");
         }
         else if(_peerConnection.signalingState == RTCSignalingHaveRemotePrAnswer)
         {
         NSLog(@" it is having RTCSignalingHaveRemotePrAnswer");
         }
         else if(_peerConnection.signalingState ==RTCSignalingHaveLocalPrAnswer && !peerConnection.localDescription)
         {
         RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
         
         [_peerConnection createAnswerWithDelegate:self constraints:constraints];
         NSLog(@"it is RTCSignalingHaveLocalPrAnswer");
         
         }
         
         else if (peerConnection.signalingState == RTCSignalingHaveRemoteOffer && !_peerConnection.localDescription) {
         // If we have a remote offer we should add it to the peer connection
         RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
         
         [_peerConnection createAnswerWithDelegate:self constraints:constraints];
         NSLog(@"it is RTCSignalingHaveRemoteOffer");
         }
         else
         NSLog(@"it is showing RTCSignalingState:%d\n",_peerConnection.signalingState);*/
        if (!_isInitiator && !_peerConnection.localDescription)
        {
            NSLog(@"not initiator + ...well...might or might not have local description...");
            // murli changes for new webrtc
            RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
            __weak AppClient *weakSelf = self;
            [_peerConnection answerForConstraints:constraints
                                completionHandler:^(RTCSessionDescription *sdp,
                                                    NSError *error) {
                                    AppClient *strongSelf = weakSelf;
                                    [strongSelf peerConnection:strongSelf.peerConnection
                                   didCreateSessionDescription:sdp
                                                         error:error];
                                }];
            //  [_peerConnection createAnswerWithDelegate:self constraints:constraints];
            
            NSLog(@"creating answer, anyway...");
        }
        
      
    });
}

- (void)drainRemoteCandidates {
    for (RTCIceCandidate* candidate in self.queuedRemoteCandidates) {
        [self.peerConnection addIceCandidate:candidate];
    }
    self.queuedRemoteCandidates = nil;
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *)error stream:(BOOL)val
{
    NSLog(@"did set description callback; question: is it for local or remote?");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error)
        {
            
            RTCLogError(@"Failed to set session description. Error: %@", error);
            
            NSLog(@"Failed to set session description. Error: %@", error);
            
            [self disconnect];
            NSLog(@"%s", __FUNCTION__);
            
            NSDictionary *userInfo = @{ NSLocalizedDescriptionKey: @"Failed to set session description.", };
            NSError *sdpError =
            [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                       code:kARDAppClientErrorSetSDP
                                   userInfo:userInfo];
//            [_delegate appClient:self didError:sdpError];
            return;
        }
        
        // If we're answering and we've just set the remote offer we need to create
        //   // an answer and set the local description.
        if (!_isInitiator && !_peerConnection.localDescription)
            
        {
            NSLog(@"not initiator + ...well...might or might not have local description...entering into");
            NSLog(@"audio mode and video mode flags");
            // murli changes for new webrtc
            RTCMediaConstraints *constraints = [self defaultAnswerConstraints:_isAudioOnly];
            
            __weak AppClient *weakSelf = self;
            [_peerConnection answerForConstraints:constraints
                                completionHandler:^(RTCSessionDescription *sdp,
                                                    NSError *error) {
                                    AppClient *strongSelf = weakSelf;
                                    [strongSelf peerConnection:strongSelf.peerConnection
                                   didCreateSessionDescription:sdp
                                                         error:error];
                                }];
            
            NSLog(@"creating answer, anyway...");
        }
        
    });
}


- (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription1:(RTCSessionDescription *)sdp error:(NSError *)error

{
  
    RTCSessionDescription *sdpPreferringH264;
#if 1
    
    if(self.isInitiator)
    {
        //its offer and answer for iphone to iphone
        sdpPreferringH264 = [SDPUtils descriptionForDescription:sdp preferredVideoCodec:@"H264"];
        self.sdp = sdpPreferringH264.description;
    }
    else
    {
        
        // Prefer H264 if available.
        if([self  isH264Codec:sdp.description])
        {
            sdpPreferringH264 = [SDPUtils descriptionForDescription:sdp preferredVideoCodec:@"H264"];
            self.sdp = sdpPreferringH264.description;
        }
        else
        {
            self.sdp = sdp.description;
            sdpPreferringH264 = sdp;
        }
    }
#endif
//    sdpPreferringH264 = [SDPUtils descriptionForDescription:sdp preferredAudioCodec:@"OPUS"];
    
    
    // did the changes for new webrtc
    __weak AppClient *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [_peerConnection setLocalDescription:sdpPreferringH264
                           completionHandler:^(NSError *error) {
                               AppClient *strongSelf = weakSelf;
                               [strongSelf peerConnection:strongSelf.peerConnection
                        didSetSessionDescriptionWithError:error];
                           }];
        
//        _currentBiteRate=[NSNumber numberWithUnsignedInteger:6];
//        [self setMaxBitrateForPeerConnectionSender:_currentBiteRate];
        
    });
    
}

//so: this gests called for both offers&answers  //murli 9-9-16 error is coming here need to fix it for PSTN
- (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError1:(NSError *)error
{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error)
        {
           NSLog(@"Failed to set session description. Error: %@", error);
        }
        
            // murli changes for new webrtc
        NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"IceRestart", nil];
            RTCMediaConstraints *constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];;
            __weak AppClient *weakSelf = self;
            [_peerConnection answerForConstraints:constraints
                                completionHandler:^(RTCSessionDescription *sdp,
                                                    NSError *error) {
                                    
                                    NSLog(@"creating answer error :%@",error);

                                    if (sdp !=nil) {
                                        AppClient *strongSelf = weakSelf;
                                        [strongSelf peerConnection:strongSelf.peerConnection
                                      didCreateSessionDescription1:sdp
                                                             error:error];
                                    }
                                   
                                }];
            
            NSLog(@"creating answer, anyway...");
    });
    
}




#pragma mark - modify for SDP optimization

-(void)candidateGatheringComplete {
    if(!_candidatesGathered){
        _candidatesGathered = YES;
        self.outgoingPayload= [self outgoingUpdateSdpWithCandidates:_iceCandidates];
        
        if([self.outgoingPayload length]>0)
        {
        if([self.outgoingPayload containsString:@"a=candidate"])
        self.outgoingPayload=[self fixCandMlinesWithProperIPandPort:self.outgoingPayload];
            
        self.outgoingPayload=[self fix489:self.outgoingPayload];
        self.outgoingPayload = [self.outgoingPayload stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\noffer\n" withString:@""];
        self.outgoingPayload = [self.outgoingPayload stringByReplacingOccurrencesOfString:@"RTCSessionDescription:\nanswer\n" withString:@""];
         [_delegate appClient:self iceGatheringChanged1:RTCIceGatheringStateComplete];
            
              if([DataSingleton sharedInstance].isIncomingCallAccept == YES)
                {
                    if ([self.outgoingPayload containsString:@"a=candidate"])
                    {
            //        [_delegate autoAccept:self.iu_id];
//                        [self creatingAnserForIncomingCall]; //VV_Test
                    [SofiaWrapperThingy autoAccept:self.iu_id];
                    [DataSingleton sharedInstance].isIncomingCallAccept = NO;
                    }
                }
        }
    }
    
  
    
}

-(NSString*) optimizePayload : (NSString*) inPayload
{
 
    if([inPayload length]>0)
    {
            NSString *lineSeparator = @"\n";
            NSMutableArray *lines =
            [NSMutableArray arrayWithArray:
             [inPayload componentsSeparatedByString:lineSeparator]];

          NSMutableArray *newlines=[NSMutableArray new];
          for (NSString* line in lines)
          {
              if(![line containsString:@"a=extmap:"])
              {
                   [newlines addObject:line];
              }
          }
            NSString *mangledSdpString = [newlines componentsJoinedByString:lineSeparator];
        
        return [mangledSdpString stringByReplacingOccurrencesOfString:@"a=fmtp:111 minptime=10;useinbandfec=1\r\n"
        withString:@"a=fmtp:111 maxaveragebitrate=28000;maxplaybackrate=16000;minptime=10;useinbandfec=1\r\na=maxptime:60\r\n"];
        
    }else{
        return inPayload;
    }
    
    /*
    if ([DataSingleton sharedInstance].client.isAudioOnly)
    {
    NSUInteger len = [inPayload length];
    
    NSArray *splitsArray = [inPayload componentsSeparatedByCharactersInSet:
                        [NSCharacterSet characterSetWithCharactersInString:@"\n"]
                        ];
    NSString *optzSDP = [[NSString alloc] init];
    
    NSUInteger arrLength = [splitsArray count];
    
    for (NSUInteger i = 0; i < arrLength-1 ; i++)
    {
//        if ([splitsArray[i] hasPrefix:@"a=ssrc"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:103"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:104"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:9"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:102"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:0"]
//            //            || [splitsArray[i] hasPrefix:@"a=rtpmap:8"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:106"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:105"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:13"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:110"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:112"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:113"]
//            || [splitsArray[i] hasPrefix:@"a=rtpmap:126"]
//
//            // || [splitsArray[i] hasPrefix:@"a=group"]
//            || [splitsArray[i] hasPrefix:@"a=msid-semantic"]
//            || ([splitsArray[i] rangeOfString:@"a=rtcp.*IP4" options:NSRegularExpressionSearch].location != NSNotFound)
//            || [splitsArray[i] hasPrefix:@"a=rtcp-fb"]
//            || [splitsArray[i] hasPrefix:@"a=extmap"]
//            || [splitsArray[i] hasPrefix:@"a=ice-options"]
////            || [splitsArray[i] hasPrefix:@"a=fmtp:111"]
//            )
//            continue;
//        if ([splitsArray[i] hasPrefix:@"m=audio"])
//        {
//
//            NSString *string = splitsArray[i];
//            NSError *error = nil;
//            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"103.*126" options:NSRegularExpressionCaseInsensitive error:&error];
//            NSString *modifiedString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, [string length]) withTemplate:@""];
//            modifiedString=[NSString stringWithFormat:@"%@ 8",modifiedString];
//            NSLog(@"%@", modifiedString);
//
//            optzSDP = [optzSDP stringByAppendingString:modifiedString];
//
//        }
//        else
//        if ([splitsArray[i] containsString:@"a=rtpmap:111"])
//        {
//            optzSDP = [optzSDP stringByAppendingString:@"a=rtpmap:111 opus/16000\n"];
//
//        }
         if ([splitsArray[i] containsString:@"a=fmtp:111"])
        {
//            fmtp:120 maxaveragebitrate=28000;maxplaybackrate=16000;minptime=10;useinbandfec=1
            NSString *modifiedString=@"a=fmtp:111 maxaveragebitrate=28000;maxplaybackrate=16000;minptime=10;useinbandfec=1\r\na=maxptime:60\r\n";
            
           optzSDP = [optzSDP stringByAppendingString:modifiedString];
        }
        else
            optzSDP = [optzSDP stringByAppendingString:splitsArray[i]];
    }
    
    NSLog(@"Length of payloads : %lu : %lu", (unsigned long)len, (unsigned long)[optzSDP length]);
        return optzSDP;

    }else
    {
        return inPayload;

    }
     
     */
}
-(NSString*) updateBitRateSdpPayload : (NSString*)inPayload bitRate:(NSString*)bitRate
{
    NSArray *splitsArray = [inPayload componentsSeparatedByCharactersInSet:
                            [NSCharacterSet characterSetWithCharactersInString:@"\n"]
                            ];
    NSString *optzSDP = [[NSString alloc] init];
    
    NSUInteger arrLength = [splitsArray count];
    
    for (NSUInteger i = 0; i < arrLength-1 ; i++)
    {
        if ([splitsArray[i] containsString:@"a=rtpmap:111"])
        {
            optzSDP = [optzSDP stringByAppendingString:[NSString stringWithFormat:@"a=rtpmap:111 OPUS/%@/1\n",bitRate]];
            
        }
//        else if ([splitsArray[i] containsString:@"a=fmtp:111"])
//        {
//            NSString *modifiedString=splitsArray[i];
//            modifiedString=[modifiedString stringByAppendingString:[NSString stringWithFormat:@";maxplaybackrate=%@; sprop-maxcapturerate=%@;maxaveragebitrate=16000",bitRate,bitRate]];
//            optzSDP = [optzSDP stringByAppendingString:modifiedString];
//        }
        else
            optzSDP = [optzSDP stringByAppendingString:splitsArray[i]];
    }
    
    return optzSDP;
}
- (RTCMediaConstraints *)defaultOfferConstraints
{
    printf("entering inside of default offer constraints");
    
    NSString *video = _isAudioOnly ? @"false" : @"true";
    NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",video,@"OfferToReceiveVideo",@"true",@"IceRestart",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl", nil];
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    printf("default offer constraints as video false");
    return constraints;
}

- (RTCMediaConstraints *)defaultOfferConstraints:(BOOL)isAudio
{
    NSDictionary *mandatoryConstraints;
    // murli did the changes for new webrtc
    
    if(isAudio)
    {
        mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"false",@"OfferToReceiveVideo",@"true",@"IceRestart",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl", nil];
    }
    else
        mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"OfferToReceiveAudio",@"true",@"OfferToReceiveVideo",@"true",@"googEchoCancellation",@"true",@"googAutoGainControl",@"true",@"googHighpassFilter",@"true",@"googNoiseSuppression",@"flase",@"levelControl", nil];
    
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}

- (RTCMediaConstraints *)defaultAnswerConstraints
{
    printf("setting here offer constraints with video as null");
    return [self defaultOfferConstraints];
}

- (RTCMediaConstraints *)defaultAnswerConstraints:(BOOL)value
{
    return [self defaultOfferConstraints:value];  //changed on 12:08 on thursday by saravanan Murugesan
}



#pragma mark - The  (ICE candidates + SDP)  love affair
// from candidateless sdp stored at self.sdp and candidates stored at array, we construct a full sdp
- (NSString*)outgoingUpdateSdpWithCandidates:(NSArray *)array
{
    // split audio & video candidates in 2 groups of strings
    NSMutableString * audioCandidates = [[NSMutableString alloc] init];
    NSMutableString * videoCandidates = [[NSMutableString alloc] init];
    for (int i = 0; i < _iceCandidates.count; i++)
    {
        RTCIceCandidate *iceCandidate = (RTCIceCandidate*)[_iceCandidates objectAtIndex:i];
        if ([iceCandidate.sdpMid isEqualToString:@"0"]||[iceCandidate.sdpMid isEqualToString:@"audio"]) {
            // don't forget to prepend an 'a=' to make this an attribute line and to append '\r\n'
            [audioCandidates appendFormat:@"a=%@\r\n",iceCandidate.sdp];
        }
        if ([iceCandidate.sdpMid isEqualToString:@"1"]||[iceCandidate.sdpMid isEqualToString:@"video"]) {
            // don't forget to prepend an 'a=' to make this an attribute line and to append '\r\n'
            [videoCandidates appendFormat:@"a=%@\r\n",iceCandidate.sdp];
        }
    }
    
    // insert inside the candidateless SDP the candidates per media type
    NSMutableString *searchedString = [self.sdp mutableCopy];
    if(searchedString !=nil)
    {
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"a=rtcp:.*?\\r\\n";
    NSError  *error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionDotMatchesLineSeparators
                                                                             error:&error];
    if (error != nil) {
        NSLog(@"outgoingUpdateSdpWithCandidates: regex error");
        return @"";
    }
    
    NSTextCheckingResult* match = [regex firstMatchInString:searchedString options:0 range:searchedRange];
    int matchIndex = 0;
    if (matchIndex == 0) {
        [regex replaceMatchesInString:searchedString options:0 range:[match range] withTemplate:[NSString stringWithFormat:@"%@%@",
                                                                                                 @"$0",audioCandidates]];
    }
    
    // search again since the searchedString has been altered
    NSArray* matches = [regex matchesInString:searchedString options:0 range:searchedRange];
    if ([matches count] == 2) {
        // count of 2 means we also have video. If we don't we shouldn't do anything
        NSTextCheckingResult* match = [matches objectAtIndex:1];
        [regex replaceMatchesInString:searchedString options:0 range:[match range] withTemplate:[NSString stringWithFormat:@"%@%@",
                                                                                                 @"$0", videoCandidates]];
    }
    
    // important: the complete message also has the sofia handle (so that sofia knows which active session to associate this with)
    NSString * completeMessage = [NSString stringWithFormat:@"%@", searchedString];
    
    return completeMessage;
    }else{
        return @"";
    }
    
    
    
    //something like this:
    //note the a=candidate: lines
    
    //    v=0
    //    o=- 2198459656716439323 2 IN IP4 127.0.0.1
    //    s=-
    //    t=0 0
    //    a=group:BUNDLE audio video
    //    a=msid-semantic: WMS ARDAMS
    //    m=audio 9 UDP/TLS/RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=candidate:4192287035 1 udp 2122260223 192.168.3.4 56577 typ host generation 0
    //    a=candidate:4192287035 2 udp 2122260222 192.168.3.4 62940 typ host generation 0
    //    a=candidate:3076703691 1 tcp 1518280447 192.168.3.4 50269 typ host tcptype passive generation 0
    //    a=candidate:3076703691 2 tcp 1518280446 192.168.3.4 50270 typ host tcptype passive generation 0
    //    a=ice-ufrag:4NW63yhwV6tpY1Tz
    //    a=ice-pwd:cNX2yQTBhPBZTBAI5gHaWh/2
    //    a=fingerprint:sha-256 BF:E8:53:72:30:E2:C9:B0:83:EB:F4:1B:04:6C:EF:73:14:38:83:18:09:69:0E:4D:67:F3:D8:88:27:26:F4:5C
    //    a=setup:actpass
    //    a=mid:audio
    //    a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=sendrecv
    //    a=rtcp-mux
    //    a=rtpmap:111 opus/48000/2
    //    a=fmtp:111 minptime=10; useinbandfec=1
    //    a=rtpmap:103 ISAC/16000
    //    a=rtpmap:9 G722/8000
    //    a=rtpmap:102 ILBC/8000
    //    a=rtpmap:0 PCMU/8000
    //    a=rtpmap:8 PCMA/8000
    //    a=rtpmap:106 CN/32000
    //    a=rtpmap:105 CN/16000
    //    a=rtpmap:13 CN/8000
    //    a=rtpmap:127 red/8000
    //    a=rtpmap:126 telephone-event/8000
    //    a=maxptime:60
    //    a=ssrc:313247115 cname:mH9QZ3fiEOprI/Zw
    //    a=ssrc:313247115 msid:ARDAMS ARDAMSa0
    //    a=ssrc:313247115 mslabel:ARDAMS
    //    a=ssrc:313247115 label:ARDAMSa0
    //    m=video 9 UDP/TLS/RTP/SAVPF 100 101 116 117 96
    //    c=IN IP4 0.0.0.0
    //    a=rtcp:9 IN IP4 0.0.0.0
    //    a=candidate:4192287035 1 udp 2122260223 192.168.3.4 58573 typ host generation 0
    //    a=candidate:4192287035 2 udp 2122260222 192.168.3.4 62976 typ host generation 0
    //    a=candidate:3076703691 1 tcp 1518280447 192.168.3.4 50271 typ host tcptype passive generation 0
    //    a=candidate:3076703691 2 tcp 1518280446 192.168.3.4 50272 typ host tcptype passive generation 0
    //    a=ice-ufrag:4NW63yhwV6tpY1Tz
    //    a=ice-pwd:cNX2yQTBhPBZTBAI5gHaWh/2
    //    a=fingerprint:sha-256 BF:E8:53:72:30:E2:C9:B0:83:EB:F4:1B:04:6C:EF:73:14:38:83:18:09:69:0E:4D:67:F3:D8:88:27:26:F4:5C
    //    a=setup:actpass
    //    a=mid:video
    //    a=extmap:2 urn:ietf:params:rtp-hdrext:toffset
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=extmap:4 urn:3gpp:video-orientation
    //    a=recvonly
    //    a=rtcp-mux
    //    a=rtpmap:100 VP8/90000
    //    a=rtcp-fb:100 ccm fir
    //    a=rtcp-fb:100 nack
    //    a=rtcp-fb:100 nack pli
    //    a=rtcp-fb:100 goog-remb
    //    a=rtcp-fb:100 transport-cc
    //    a=rtpmap:101 VP9/90000
    //    a=rtcp-fb:101 ccm fir
    //    a=rtcp-fb:101 nack
    //    a=rtcp-fb:101 nack pli
    //    a=rtcp-fb:101 goog-remb
    //    a=rtcp-fb:101 transport-cc
    //    a=rtpmap:116 red/90000
    //    a=rtpmap:117 ulpfec/90000
    //    a=rtpmap:96 rtx/90000
    //    a=fmtp:96 apt=100
}



// remove candidate lines from the given sdp and return them as elements of an NSArray
-(NSDictionary*)incomingFilterCandidatesFromSdp:(NSMutableString*)sdp
{
    NSMutableArray * audioCandidates = [[NSMutableArray alloc] init];
    NSMutableArray * videoCandidates = [[NSMutableArray alloc] init];
    
    NSString *searchedString = sdp;
    NSRange searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"m=audio|m=video|a=(candidate.*)\\r\\n";
    NSError  *error = nil;
    
    NSString * collectionState = @"none";
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    NSArray* matches = [regex matchesInString:searchedString options:0 range: searchedRange];
    for (NSTextCheckingResult* match in matches) {
        //NSString* matchText = [searchedString substringWithRange:[match range]];
        NSString * stringMatch = [searchedString substringWithRange:[match range]];
        if ([stringMatch isEqualToString:@"m=audio"]) {
            // enter audio collection state
            collectionState = @"audio";
            audiocount++;
            printf("audio count:%d\n",audiocount);
            continue;
        }
        if ([stringMatch isEqualToString:@"m=video"]) {
            // enter audio video collection state
            // collectionState = @"audio";
            collectionState=@"audio";
            printf("videocount:%d and audiocount:%d\n",videocount,audiocount);
            videocount++;
            audiocount++;
            continue;
        }
        
        if ([collectionState isEqualToString:@"audio"]) {
            [audioCandidates addObject:[searchedString substringWithRange:[match rangeAtIndex:1]]];
        }
        if ([collectionState isEqualToString:@"video"]) {
            [videoCandidates addObject:[searchedString substringWithRange:[match rangeAtIndex:1]]];
        }
    }
    
    NSString *removePattern = @"a=(candidate.*)\\r\\n";
    NSRegularExpression* removeRegex = [NSRegularExpression regularExpressionWithPattern:removePattern options:0 error:&error];
    // remove the candidates (we want a candidateless SDP)
    [removeRegex replaceMatchesInString:sdp options:0 range:NSMakeRange(0, [sdp length]) withTemplate:@""];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:audioCandidates, @"audio",
            videoCandidates, @"video", nil];
}


//restore in the future
- (NSString *)fix488:(NSString *)input
{
    NSString *processor;
    
    processor = [input stringByReplacingOccurrencesOfString:@"TCP/TLS/" withString:@"RTP/SAVPF"];
    
    NSLog(@"printing value of fix488 processer:%@\n",processor);
    
    return processor;
}
-(NSString*)fix489:(NSString*)input
{
     if([input length]>0)
      {
      NSString *processor;
      
      [input stringByReplacingOccurrencesOfString:@"sendonly" withString:@"recvonly"];
      processor = [input stringByReplacingOccurrencesOfString:@"recvonly" withString:@"sendrecv"];
      
      printf("fix 489");
      return processor;
          
      }else
      {
          return input;
      }
}

-(NSString*)fix490:(NSString*)input
{
    NSString *processor;
    
    processor = [input stringByReplacingOccurrencesOfString:@"typ host" withString:@"typ relay"];
    
    printf("fix 489");
    return processor;
}

- (NSString *)killVideoLines:(NSString *)input
{
    NSString *processor;
    
    NSRange range = [input rangeOfString:@"m=video"];
    
    range.length = [input length] - range.location;
    
    processor = [input stringByReplacingCharactersInRange:range withString:@""];
    
    return processor;
}

char * deblank(char *str)
{
    char *out = str, *put = str;
    
    for(; *str != '\0'; ++str)
    {
        if(*str != ' ')
            *put++ = *str;
    }
    *put = '\0';
    
    return out;
}

+(NSString*)killvidaudparam:(NSString*)input
{
    NSString *processor;
    
    
    processor = [input stringByReplacingOccurrencesOfString:@"m=video 9 TCP/TLS/RTP/SAVPF 100 116 117 96" withString:@"m=video 9 TCP/TLS/RTP/SAVPF 100"];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"m=audio 9 TCP/TLS/RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126" withString:@"m=audio 9 TCP/TLS/RTP/SAVPF 111"];
    
    
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:103 ISAC/16000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:9 G722/8000" withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:102 ILBC/8000"  withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:0 PCMU/8000" withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:8 PCMA/8000" withString:@" "];
    
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:106 CN/32000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:105 CN/16000" withString:@" "];
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:13 CN/8000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:127 red/8000" withString:@" "];
    
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtpmap:126 telephone-event/8000" withString:@" "];
    
    
    NSLog(@"printing sdp after modification:%@\n",processor);
    
    return processor;
    
    
    //          return processor;
    
    
    
    
}


+ (NSDictionary *)getIPAddresses
{
    
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    //char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
    struct ifaddrs *interfaces;
    if (!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        //  memcpy((__bridge void *)(addrBuf1),addrBuf,sizeof(addrBuf));
                        //   addrBuf1=[[[NSMutableString alloc] appendString:addrBuf1]];
                        
                        
                        
                        
                        
                        type = IP_ADDR_IPv4;
                        
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        //  memcpy((__bridge void *)(addrBuf2),addrBuf,sizeof(addrBuf));
                        
                        
                        
                        // addrBuf2=[str mutableCopy];
                        // NSLog(@"printing address ipv6:%@\n",addrBuf2);
                        type = IP_ADDR_IPv6;
                        
                    }
                    
                }
                if(type) {
                    
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}






// primary address is the one we want to be used based on charging. For example if both wifi & cellular network
// are available we want to use wifi
+ (NSString *)getPrimaryIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_interfaces = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_interfaces = interfaces;
        while(temp_interfaces != NULL) {
            //murli implementing for IPV6 as well 2-10-16
            if(temp_interfaces->ifa_addr->sa_family == AF_INET   )
            {
                // Check if interface is en0 which is the wifi connection on the iPhone mainly EA0/iPV4 contans the Primary /ip Address
                
                if([[NSString stringWithUTF8String:temp_interfaces->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_interfaces->ifa_addr)->sin_addr)];
                    strIPAddtessFamily =IP_ADDR_IPv4;
                    break;
                    
                }
            }
            
            
            else
            {
                NSDictionary *addresses = (NSDictionary*)[AppClient getIPAddresses];
                
                NSLog(@"addresses: %@\n",addresses);
                
                // prefer wifi over cellular. TODO: need to add ipv6 logic if we want to support it
                NSArray * preference;
                // preference= @[WIFI@"/"IP_ADDR_IPv4,WIFI@"/"IP_ADDR_IPv6, ETH_OVER_THUNDERBOLT@"/"IP_ADDR_IPv4, ETH_OVER_THUNDERBOLT@"/"IP_ADDR_IPv6,IOS_CELLULAR@"/"IP_ADDR_IPv4];
                
                preference= @[WIFI@"/"IP_ADDR_IPv4,WIFI@"/"IP_ADDR_IPv6,IOS_CELLULAR@"/"IP_ADDR_IPv4];
                NSLog(@"IP Preference: %@",preference);
                NSString * key ;
                for (key in preference) {
                    if ([addresses objectForKey:key]) {
                        NSLog(@"address: %@",addresses);
                        strIPAddtessFamily =IP_ADDR_IPv6;
                        break;
                        //return [addresses objectForKey:key];
                    }
                }
                if(addresses)
                    return [addresses objectForKey:key];
                else
                    return @"";
                
            }
            
            
            temp_interfaces = temp_interfaces->ifa_next;
        }
    }
    // Free memory
    NSLog(@"geting Vectone Sim ip  addresses:%@\n",address);
    freeifaddrs(interfaces);
    return address;
    
}



- (NSString *)fixCandMlinesWithProperIPandPort:(NSString *)input
{
    NSString *processor;
    
    NSRange startRange;
    startRange.location = 0;
    
    if([input containsString:@"a=candidate"])
    {
    NSRange endRange = [input rangeOfString:@"a=candidate"];
    startRange.length = endRange.location;
    if(startRange.length > 0)
    processor = [input stringByReplacingCharactersInRange:startRange withString:@""];
    }
    
    NSRange spaceRange;
    
    for (int i = 0; i < 4; i++)
    {
        spaceRange = [processor rangeOfString:@" "];
        startRange.location = 0;
        startRange.length = spaceRange.location + 1; // +1 ---> delete the space itself as well
        if(startRange.length > 0)
        processor = [processor stringByReplacingCharactersInRange:startRange withString:@""];
    }
    
    spaceRange = [processor rangeOfString:@" "];
    NSString *IPaddress = [processor substringToIndex:spaceRange.location];
    
    startRange.location = 0;
    startRange.length = spaceRange.location + 1; // +1 ---> delete the space itself as well
    if(startRange.length > 0)
    processor = [processor stringByReplacingCharactersInRange:startRange withString:@""];
    
    spaceRange = [processor rangeOfString:@" "];
    NSString *PortNumber = [processor substringToIndex:spaceRange.location];
    
    
    
    processor = [input stringByReplacingOccurrencesOfString:@"0.0.0.0" withString:IPaddress];
    
    NSString *replacement = [NSString stringWithFormat:@"m=audio %@", PortNumber];
    processor = [processor stringByReplacingOccurrencesOfString:@"m=audio 9" withString:replacement];
    
    
    replacement = [NSString stringWithFormat:@"a=rtcp:%@", PortNumber];
    processor = [processor stringByReplacingOccurrencesOfString:@"a=rtcp:9" withString:replacement];
    
    
    return processor;
    
    //    v=0
    //    o=- 8883056594029298043 2 IN IP4 127.0.0.1
    //    s=-
    //    t=0 0
    //    a=group:BUNDLE audio video
    //    a=msid-semantic: WMS ARDAMS
    //    m=audio 58748 RTP/SAVPF 111 103 9 102 0 8 106 105 13 127 126
    //    c=IN IP4 192.168.3.4
    //    a=rtcp:58748 IN IP4 192.168.3.4
    //    a=candidate:4192287035 1 udp 2122260223 192.168.3.4 58748 typ host generation 0
    //    a=candidate:4192287035 2 udp 2122260222 192.168.3.4 60953 typ host generation 0
    //    a=candidate:3076703691 1 tcp 1518280447 192.168.3.4 52467 typ host tcptype passive generation 0
    //    a=candidate:3076703691 2 tcp 1518280446 192.168.3.4 52468 typ host tcptype passive generation 0
    //    a=ice-ufrag:6ISVj+ay8ddGC6fi
    //    a=ice-pwd:hl9tn7wZhxM7Fh8GOh1u3rt0
    //    a=fingerprint:sha-256 ED:76:DA:16:CE:C1:9A:AC:57:96:51:F1:CA:31:B4:5A:89:F8:65:56:1F:EB:36:FA:10:50:38:4D:63:27:58:13
    //    a=setup:actpass
    //    a=mid:audio
    //    a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level
    //    a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time
    //    a=sendrecv
    //    a=rtcp-mux
    //    a=rtpmap:111 opus/48000/2
    //    a=fmtp:111 minptime=10; useinbandfec=1
    //    a=rtpmap:103 ISAC/16000
    //    a=rtpmap:9 G722/8000
    //    a=rtpmap:102 ILBC/8000
    //    a=rtpmap:0 PCMU/8000
    //    a=rtpmap:8 PCMA/8000
    //    a=rtpmap:106 CN/32000
    //    a=rtpmap:105 CN/16000
    //    a=rtpmap:13 CN/8000
    //    a=rtpmap:127 red/8000
    //    a=rtpmap:126 telephone-event/8000
    //    a=maxptime:60
    //    a=ssrc:93419890 cname:uu3+D2nw8Ig3EjdW
    //    a=ssrc:93419890 msid:ARDAMS ARDAMSa0
    //    a=ssrc:93419890 mslabel:ARDAMS
    //    a=ssrc:93419890 label:ARDAMSa0
}

- (void)workaroundTruncation:(NSMutableString*)sdp
{
    NSString *pattern = @"cnam$";
    NSError  *error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
    [regex replaceMatchesInString:sdp options:0 range:NSMakeRange(0, [sdp length]) withTemplate:@"$0e:r5NeEmW7rYyFBr5w"];
}

#pragma mark - ARDSignalingChannelDelegate
//
//- (void)channel:(id<ARDSignalingChannel>)channel
//    didReceiveMessage:(ARDSignalingMessage *)message {
//  switch (message.type) {
//    case kARDSignalingMessageTypeOffer:
//    case kARDSignalingMessageTypeAnswer:
//      // Offers and answers must be processed before any other message, so we
//      // place them at the front of the queue.
//      _hasReceivedSdp = YES;
//      [_messageQueue insertObject:message atIndex:0];
//      break;
//    case kARDSignalingMessageTypeCandidate:
//      [_messageQueue addObject:message];
//      break;
//    case kARDSignalingMessageTypeBye:
//      // Disconnects can be processed immediately.
//      [self processSignalingMessage:message];
//      return;
//  }
//  [self drainMessageQueueIfReady];
//}
//
//- (void)channel:(id<ARDSignalingChannel>)channel
//    didChangeState:(ARDSignalingChannelState)state {
//  switch (state) {
//    case kARDSignalingChannelStateOpen:
//      break;
//    case kARDSignalingChannelStateRegistered:
//      break;
//    case kARDSignalingChannelStateClosed:
//    case kARDSignalingChannelStateError:
//      // TODO(tkchin): reconnection scenarios. Right now we just disconnect
//      // completely if the websocket connection fails.
//      [self disconnect];
//      break;
//  }
//}

#pragma mark - Collider methods

//- (void)registerWithColliderIfReady {
//  if (!self.hasJoinedRoomServerRoom) {
//    return;
//  }
//  // Open WebSocket connection.
//  if (!_channel) {
//    _channel =
//        [[ARDWebSocketChannel alloc] initWithURL:_websocketURL
//                                         restURL:_websocketRestURL
//                                        delegate:self];
//    if (_isLoopback) {
//      _loopbackChannel =
//          [[ARDLoopbackWebSocketChannel alloc] initWithURL:_websocketURL
//                                                   restURL:_websocketRestURL];
//    }
//  }
//  [_channel registerForRoomId:_roomId clientId:_clientId];
//  if (_isLoopback) {
//    [_loopbackChannel registerForRoomId:_roomId clientId:@"LOOPBACK_CLIENT_ID"];
//  }
//}

#pragma mark - Errors

//+ (NSError *)errorForJoinResultType:(ARDJoinResultType)resultType {
//  NSError *error = nil;
//  switch (resultType) {
//    case kARDJoinResultTypeSuccess:
//      break;
//    case kARDJoinResultTypeUnknown: {
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorUnknown
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Unknown error.",
//      }];
//      break;
//    }
//    case kARDJoinResultTypeFull: {
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorRoomFull
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Room is full.",
//      }];
//      break;
//    }
//  }
//  return error;
//}
//
//+ (NSError *)errorForMessageResultType:(ARDMessageResultType)resultType {
//  NSError *error = nil;
//  switch (resultType) {
//    case kARDMessageResultTypeSuccess:
//      break;
//    case kARDMessageResultTypeUnknown:
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorUnknown
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Unknown error.",
//      }];
//      break;
//    case kARDMessageResultTypeInvalidClient:
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorInvalidClient
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Invalid client.",
//      }];
//      break;
//    case kARDMessageResultTypeInvalidRoom:
//      error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
//                                         code:kARDAppClientErrorInvalidRoom
//                                     userInfo:@{
//        NSLocalizedDescriptionKey: @"Invalid room.",
//      }];
//      break;
//  }
//  return error;
//}

#pragma mark - RENEGOTIATION REINVITE

-(BOOL)createReInviteOffer
{
    if (_peerConnection != NULL) {
        printf("test createReInviteOffer \n");
        _candidatesGathered = NO;
       // self.isInitiator = NO;
//        _isInitiator = YES;
        self.state = kARDAppClientReConnecting;
        NSDictionary *mandatoryConstraints = [[NSDictionary alloc] initWithObjectsAndKeys:@"true",@"IceRestart", nil];
        RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
        _peerConnection.delegate = self;
        // Create AV media stream and add it to the peer connection.
        
//        [self createLocalMediaStream:_video];
//        [_peerConnection addStream:_localMediaStream];
        
        printf("creating offer");
        __weak AppClient *weakSelf = self;
        
        [_peerConnection offerForConstraints:constraints
                           completionHandler:^(RTCSessionDescription *sdp,
                                               NSError *error) {
                               AppClient *strongSelf = weakSelf;
                               [strongSelf peerConnection:strongSelf.peerConnection
                              didCreateSessionDescription:sdp
                                                    error:error];
                           }];
        
    }
    
    return YES;
}

-(void) createReInviteAnswer:(BOOL) isVideo
{
    
    if (_peerConnection != NULL) {
        
//        _isIncomingCall = YES;
    //    _isInitiator = NO;
        _isAudioOnly = isVideo;
        self.state = kARDAppClientReConnecting;
        // [ _iceCandidates removeAllObjects];
        if(self.queuedRemoteCandidates)
        {
            [_peerConnection removeIceCandidates:self.queuedRemoteCandidates];
            [self.queuedRemoteCandidates removeAllObjects];
            self.queuedRemoteCandidates = nil;
        }
        
        self.queuedRemoteCandidates = [NSMutableArray array];
        //_iceCandidates = [NSMutableArray array];
//        _candidatesGathered = YES;
        
        //  self.state = kARDAppClientReConnecting;
        
        
        
        _peerConnection.delegate = self;
        
        if(!isVideo)
            self.state = kARDAppClientAudioSessionCreated;
        printf("creating peerConnection");
        printf("1111printing video value of BOOL value:%d",isVideo);
        
//        if([DataSingleton sharedInstance].isAutoReInvite)
//        {
//            [DataSingleton sharedInstance].incomingPayload = [[DataSingleton sharedInstance].incomingPayload stringByReplacingOccurrencesOfString:@"setup:actpass" withString:@"setup:passive"];
//        }
//
      
        
        NSMutableString *answerSDP = [NSMutableString stringWithString:self.incomingPayload];
        RTCSessionDescription *incomingDescription;
        self.sdp = nil;
        RTCSdpType type;
        type = RTCSdpTypeOffer;
        //this means offer
        incomingDescription = [[RTCSessionDescription alloc] initWithType:type sdp:answerSDP];
        
        // test changes for new webrtc
        __weak AppClient *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(),^{
            [_peerConnection setRemoteDescription:incomingDescription
                                completionHandler:^(NSError *error) {
                                    AppClient *strongSelf = weakSelf;
                                    [strongSelf peerConnection:strongSelf.peerConnection
                             didSetSessionDescriptionWithError1:error];
                                }];
            
            
            printf("setRemoteDescription happening here");
            
            NSDictionary *candidates = [self incomingFilterCandidatesFromSdp:answerSDP];
            RTCIceCandidate *iceCandidate;
            
            
            for (NSString * key in candidates)
            {
                for (NSString * candidate in [candidates objectForKey:key]) {
                    // remember that we have set 'key' to be either 'audio' or 'video' inside incomingFilterCandidatesFromSdp
                    iceCandidate =[[RTCIceCandidate alloc] initWithSdp:candidate sdpMLineIndex:0 sdpMid:key];
                    if (self.queuedRemoteCandidates)
                    {
                        [self.queuedRemoteCandidates addObject:iceCandidate];
                    }
                    
                    [_peerConnection addIceCandidate:iceCandidate];
                    
                }
            }
            
            
            self.state =  kARDAppClientStateConnected;

            
        });
        printf("Offer is created ");
        
    }
}

-(BOOL) reConnectWhenNetworkChanged
{

    self.isTurnComplete = YES;
    _peerConnection.delegate = self;
    
    [_peerConnection removeIceCandidates:_iceCandidates];
    
    [_iceCandidates removeAllObjects];
    _iceCandidates = [NSMutableArray array];
    return ([self createReInviteOffer]);
    
}


@end



