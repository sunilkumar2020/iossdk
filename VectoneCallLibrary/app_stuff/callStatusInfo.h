//
//  callStatusInfo.h
//  ChillitalkWebRTC
//
//  Created by mundio on 19/02/20.
//  Copyright © 2020 mundio. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface callStatusInfo : NSObject

@property (nonatomic, strong)  NSString *from;
@property (nonatomic, strong)  NSString *to;
@property (nonatomic, strong)  NSString *call_id;
@property (nonatomic, strong)  NSString *iu_id;
@property  int status;

@end

NS_ASSUME_NONNULL_END
